package exceptions;

public class MaxNumOfBooksReachedException extends Exception {

    private int maxNumber;

    public MaxNumOfBooksReachedException(int maxNumber) {
        this.maxNumber = maxNumber;
    }

    public String getValue() {
        return String.valueOf(maxNumber);
    }
}
