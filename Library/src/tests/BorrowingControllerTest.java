package tests;

import controllers.BorrowingController;
import model.enums.Genre;
import model.enums.MemberType;
import model.library.Book;
import model.library.BorrowedCopy;
import model.library.Borrowing;
import model.users.Member;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BorrowingControllerTest {

    static BorrowingController bc;
    static Member m;
    static Book b;
    static BorrowedCopy brc;
    static Borrowing br;

    @BeforeAll
    static void before(){
        bc = new BorrowingController();
        m = new Member("7418529634","Test name", "Test surname",
                new Date(21,7,4),new Date(121,7,4),new Date(212,7,4), MemberType.HONORABLE);
        m.setId(0);
        b = new Book("Test title",2021,0,"Test publisher",0, Genre.AUTOBIOGRAPHY,false,"test tags");
        brc = new BorrowedCopy();
        br = new Borrowing(m.getId(),0,new Date(121,7,4));
        List<BorrowedCopy> list = new ArrayList<BorrowedCopy>();
        list.add(brc);
        br.setBorrowedCopies(list);
        List<Borrowing> l = new ArrayList<Borrowing>();
        l.add(br);
        m.setBorrowings(l);
    }


    @Test
    void getCardNum() {
        String s = bc.getCardNum(m);
        assertEquals("0",s);
    }

    @Test
    void getBorrowedCopies() {
        assertNotNull(bc.getBorrowedCopies(m));
    }

    @Test
    void getCurrentlyBorrowedCopies() {
        assertNotNull(bc.getCurrentlyBorrowedCopies(m));
    }
}