package tests;

import controllers.BorrowingHistoryController;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class BorrowingHistoryControllerTest {

    static BorrowingHistoryController bhc;

    @BeforeAll
    static void before(){
        bhc = new BorrowingHistoryController();
    }

    @Test
    void getColumnCount() {
        assertEquals(6,bhc.getColumnCount());
    }

    @Test
    void getColumnName() {
        assertEquals("Naslov",bhc.getColumnName(0));
    }

    @Test
    void getColumnClass() {
        assertEquals(Date.class,bhc.getColumnClass(4));
    }

}