package repos;

import model.enums.BookCopyState;
import model.library.Book;
import model.library.BookCopy;
import model.library.BorrowedCopy;
import model.library.Borrowing;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class BookRepo extends ListProperty<Book> {
    Session session;

    public BookRepo(Session session) {
        values = new ArrayList<Book>();
        this.session = session;
    }

    public int getNumOfFreeCopies(Book book){
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        session.beginTransaction();
        int bookId = book.getId();
        Query<BookCopy> query = session.createQuery("from BookCopy bc where bc.bookId = :bookId and bc.state = :state",BookCopy.class);
        query.setParameter("bookId",bookId);
        query.setParameter("state", BookCopyState.valueOf("FREE"));
        List<BookCopy> list = query.getResultList();
        int i = list.size();
        session.getTransaction().commit();
        return i;
    }

    public int getNumOfLoanedBooks(int memberId){
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        session.beginTransaction();
        Query<Borrowing> query = session.createQuery("from Borrowing b where b.memberId = :memberId",Borrowing.class);
        query.setParameter("memberId",memberId);
        ArrayList<Borrowing> borrowings = (ArrayList<Borrowing>) query.getResultList();
        int loaned = 0;
        for (Borrowing b : borrowings){
            List<BorrowedCopy> borrowedCopies = (List<BorrowedCopy>) b.getBorrowedCopies();
            for (BorrowedCopy bc : borrowedCopies){
                if (bc.getReturnDate()==null){
                    loaned+=1;
                }
            }
        }
        session.getTransaction().commit();
        return loaned;
        //return 0;
    }

    public ArrayList<Integer> getMostRead(){
        ArrayList<Integer> mostRead = new ArrayList<Integer>();
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        session.beginTransaction();

        Query query = session.createQuery("SELECT b.id, count(*) from Book b " +
                "inner join BookCopy bc on b.id = bc.bookId " +
                "inner join BorrowedCopy brc on brc.copyId = bc.copyId " +
                "group by b.id order by count(*) DESC");
        List<Object> list = query.getResultList();
        for (int i = 0; i<5;i++){
            Object[] fields = (Object[]) list.get(i);
            mostRead.add((Integer)fields[0]);
        }
        session.getTransaction().commit();

        return mostRead;
    }

    public Book getBook(int id){
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        session.beginTransaction();
        Book book = session.get(Book.class,id);
        session.getTransaction().commit();
        return book;
    }

    public int getBorrowCount(Book b){
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        session.beginTransaction();

        int count = 0;
        Query query = session.createQuery("SELECT count(*) " +
                "from Book b " +
                "inner join BookCopy bc on b.id = bc.bookId " +
                "inner join BorrowedCopy brc on brc.copyId = bc.copyId " +
                "where b.id = :id " +
                "group by b.id");
        query.setParameter("id",b.getId());
        List<Object> list = query.getResultList();
        count += (Long)list.get(0);

        session.getTransaction().commit();
        return count;
    }


    public ListProperty<Book> getBooks() {
        ListProperty<Book> books = null;
        if (values.size() > 0) {
            return new ListProperty<>(values);
        }
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query<Book> query = session.createQuery("from Book", Book.class);
            values = query.getResultList();
            tx.commit();
            return new ListProperty<>(values);
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
            return books;
        }
    }

    public List<String> getAllTitles() {
        List<String> titles = new ArrayList<>();
        for (Book book : getBooks().get())
            titles.add(book.getTitle().toLowerCase());
        return titles;
    }

    public List<String> getAllAuthorsNames() {
        List<String> authors = new ArrayList<>();
        for (Book book : getBooks().get())
            for (String author : book.getAuthorsNames())
                if (!authors.contains(author.toLowerCase())) authors.add(author.toLowerCase());
        return authors;
    }

    public List<String> getAllTags() {
        List<String> tags = new ArrayList<>();
        for (Book book : getBooks().get())
            for (String tag : book.getTags())
                if (!tags.contains(tag.toLowerCase())) tags.add(tag.toLowerCase());
        return tags;
    }

    public boolean canBeReserved(int bookID) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query<BookCopy> query = session.createQuery("from BookCopy b where b.bookId = :book and b.state = :state", BookCopy.class);
            query.setParameter("book", bookID);
            query.setParameter("state", 0);
            query.getResultList();
            tx.commit();
            return true;
        } catch (Exception e) {
            tx.rollback();
            return false;
        }
    }

    public void updateCopy(BookCopy copy) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(copy);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }
}