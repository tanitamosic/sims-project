package repos;

import model.library.PriceListing;
import model.users.UserAccount;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.Date;

public class PriceRepo extends ListProperty<PriceListing> {
    Session session;
    public PriceRepo(Session session) {
        values = new ArrayList<PriceListing>();
        this.session = session;
    }

    public PriceListing getCurrentPrice() {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query<PriceListing> query = session.createQuery("from PriceListing pl where :date BETWEEN startDate AND endDate", PriceListing.class);
            query.setParameter("date", new Date());
            PriceListing pl = query.getSingleResult();
            tx.commit();
            return pl;
        } catch (Exception e) {
            tx.rollback();
            return null;
        }
    }

    public void changePrice(PriceListing pl, PriceListing newPrice) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.merge(pl);
            session.save(newPrice);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }
}
