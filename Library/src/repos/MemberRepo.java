package repos;

import model.library.Book;
import model.users.Member;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

public class MemberRepo extends ListProperty<Member> {
    private Session session;

    public MemberRepo() {
        values = new ArrayList<Member>();
    }

    public MemberRepo(Session session) {
        values = new ArrayList<Member>();
        this.session = session;
    }

    public Member getByAccountId(int accountId) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        Member m = null;
        try {
            tx = session.beginTransaction();
            Query<Member> mem = session.createQuery("from Member m where m.accountId=:id", Member.class);
            mem.setParameter("id", accountId);
            m = mem.uniqueResult();
            tx.commit();
            return m;
        } catch (Exception e) {
            tx.rollback();
            return m;
        }
    }

    public void updateMember(Member member) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.merge(member);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }

    public Member getByMemberID(int id) {
        Member m = null;
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            m = session.get(Member.class, id);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
        return m;
    }

    public ListProperty<Member> getMembers() {
        ListProperty<Member> members = null;
        if (values.size() > 0) {
            return new ListProperty<>(values);
        }
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query<Member> query = session.createQuery("from Member", Member.class);
            values = query.getResultList();
            tx.commit();
            return new ListProperty<>(values);
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
            return members;
        }
    }


    public void saveMember(Member m) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(m);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }
}
