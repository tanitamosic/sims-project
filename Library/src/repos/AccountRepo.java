package repos;

import model.users.UserAccount;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;

public class AccountRepo extends ListProperty<UserAccount> {
    Session session;
    public AccountRepo() {
        values = new ArrayList<UserAccount>();
    }

    public AccountRepo(Session session) {
        this.session = session;
    }

    public boolean checkCredentials(String un, String pass){
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        if (!session.getTransaction().isActive()) session.beginTransaction();
        Query<UserAccount> query = session.createQuery("from UserAccount u where u.username = :username and u.password = :password", UserAccount.class);
        query.setParameter("username", un);
        query.setParameter("password", pass);
        UserAccount ua = query.getSingleResult();
        session.getTransaction().commit();
        if (ua != null) {
            return true;
        }
        return false;
    }


    public UserAccount getUser(String username){
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query<UserAccount> query = session.createQuery("from UserAccount u where u.username = :username", UserAccount.class);
            query.setParameter("username", username);
            UserAccount ua = query.getSingleResult();
            tx.commit();
            return ua;
        } catch (Exception e) {
            tx.rollback();
            return null;
        }
    }

    public void updateAccount(UserAccount userAccount) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.merge(userAccount);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }


    public void saveAccount(UserAccount ua) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(ua);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }
}