package repos;
import java.util.Collections;
import java.util.List;

public class ListProperty<T> {
    protected List<T> values = null;

    public ListProperty() {
    }

    public ListProperty(List<T> values) {
        this.values = values;
    }

    public List<T> get() {
        if (values == null) return null;
        return Collections.unmodifiableList(values);
    }

    public void add(T t) {
        if (t == null) throw new NullPointerException();
        if (values == null) throw  new IllegalArgumentException();
        values.add(t);
    }

    public void remove(T t) {
        if (t == null) throw new NullPointerException();
        if (values == null) throw  new IllegalArgumentException();
        if (!values.contains(t)) throw  new IllegalArgumentException();
        values.remove(t);
    }

    public int size() {
        if (values == null) return 0;
        return values.size();
    }

    public T at(int index) {
        return values.get(index);
    }
}
