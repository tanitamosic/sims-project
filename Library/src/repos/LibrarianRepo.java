package repos;

import model.enums.LibrarianRole;
import model.users.Librarian;
import model.users.Member;
import model.users.User;
import model.users.UserAccount;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;

public class LibrarianRepo extends ListProperty<Librarian> {
    private Session session;
    public LibrarianRepo() {
        values = new ArrayList<Librarian>();
    }

    public LibrarianRepo(Session session) {
        this.session = session;
    }


    public Librarian getByAccountId(int accountId) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Librarian l = null;
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query<Librarian> mem = session.createQuery("from Librarian m where m.accountId=:id", Librarian.class);
            mem.setParameter("id", accountId);
            l = mem.uniqueResult();
            tx.commit();
            return l;
        } catch (Exception e) {
            tx.rollback();
            return l;
        }
    }

    public void saveRoles(Librarian l) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            for (LibrarianRole role : l.getRoles()) {
                Query query = session.createSQLQuery("INSERT INTO LIBRARIAN_ROLE (LIBRARIANID, ROLE) VALUES (:LID, :ROLEE)");
                query.setParameter("LID", l.getLibrarianId());
                query.setParameter("ROLEE", role.ordinal());
                query.executeUpdate();
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }

    public void saveLibrarian(Librarian l) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(l);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }

}
