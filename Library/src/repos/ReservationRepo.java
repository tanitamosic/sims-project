package repos;

import model.enums.ReservationStatus;
import model.library.Reservation;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;

public class ReservationRepo extends ListProperty<Reservation> {
    Session session;
    public ReservationRepo(Session session) {
        this.session = session;
    }

    public ArrayList<Reservation> getPendingReservations(){

        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        session.beginTransaction();
        Query<Reservation> query = session.createQuery("from Reservation r where r.status = :status", Reservation.class);
        query.setParameter("status", ReservationStatus.PENDING);
        ArrayList<Reservation> list = (ArrayList<Reservation>) query.getResultList();
        session.getTransaction().commit();
        return list;
    }

    public void addReservation(Reservation reservation) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(reservation);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }

    public void updateReservation(Reservation r){
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.merge(r);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }
}
