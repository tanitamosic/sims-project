package repos;

import model.library.BorrowedCopy;
import model.library.Borrowing;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;

public class BorrowingRepo extends ListProperty<Borrowing> {
    Session session;
    public BorrowingRepo(Session session) {
        values = new ArrayList<Borrowing>();
        this.session = session;
    }

    public void updateBorrowedCopy(BorrowedCopy borrowedCopy) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.merge(borrowedCopy);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }

    public void addBorrowing(Borrowing b) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(b);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }

    public void addBorrowedCopy(BorrowedCopy bc) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(bc);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }

}
