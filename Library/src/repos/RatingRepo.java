package repos;

import model.library.BorrowedCopy;
import model.library.Rating;
import model.states.Borrowed;
import model.users.Member;
import model.users.UserAccount;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class RatingRepo extends ListProperty<Rating> {
    Session session;

    public RatingRepo(Session session) {
        values = new ArrayList<Rating>();
        this.session = session;
    }

    public void addRating(Rating rating) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            System.out.println(rating);
            session.save(rating);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }

    public boolean existsRating(Member member, BorrowedCopy borrowedCopy) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query<Rating> query = session.createQuery("from Rating r where r.memberID = :member and r.bookID = :book", Rating.class);
            query.setParameter("member", member.getId());
            query.setParameter("book", borrowedCopy.getCopy().getBookId());
            List<Rating> list = query.getResultList();
            tx.commit();
            if (list.size() > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
            return false;
        }
    }

    public void deleteRating(Rating rating) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(rating);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }

    public ArrayList<Rating> getPendingRatings() {
        ArrayList<Rating> pending = new ArrayList<>();
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            Query<Rating> query = session.createQuery("from Rating r where r.approved = :state", Rating.class);
            query.setParameter("state", false);
            List<Rating> ratings = query.getResultList();
            tx.commit();
            pending.addAll(ratings);
        } catch (Exception e) {
            tx.rollback();
        }
        return pending;

    }

    public void updateRating(Rating rating) {
        if (!session.isOpen()) {
            session = session.getSessionFactory().openSession();
        }
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.merge(rating);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
        }
    }
}
