package model.users;

import exceptions.InvalidMemberTypeException;
import model.enums.MemberType;
import model.library.Borrowing;
import model.library.Rating;
import model.library.Reservation;

import javax.persistence.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "MEMBER")
public class Member extends User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MEMBER")
    @SequenceGenerator(name = "SEQ_MEMBER", sequenceName = "MEMBER_SEQ", allocationSize = 1)
    @Column(name = "MEMBERID", updatable = false, nullable = false)
    private int id;

    @Column(name = "ACCOUNTID")
    private int accountId;
    private Date enrollmentDate;
    private Date expirationDate;

    @Column(name = "TYPE")
    private MemberType memberType;

    @Basic(fetch = FetchType.LAZY)
    @OneToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "rating",
            joinColumns = @JoinColumn(name = "memberId"),
            inverseJoinColumns = @JoinColumn(name = "ratingId")
    )
    private List<Rating> ratings;

    @Basic(fetch = FetchType.LAZY)
    @OneToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "reservation",
            joinColumns = @JoinColumn(name = "memberId"),
            inverseJoinColumns = @JoinColumn(name = "reservationId")
    )
    private List<Reservation> reservations;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinTable(name = "borrowing",
            joinColumns = @JoinColumn(name = "memberId"),
            inverseJoinColumns = @JoinColumn(name = "borrowingId")
    )
    private List<Borrowing> borrowings;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "accountId", insertable = false, updatable = false)
    private UserAccount user_acc;

    public Member() {
    }

    public Member(String JMBG, String name, String surname, Date birthDate, Date enrollmentDate, Date expirationDate, MemberType memberType) {
        super(JMBG, name, surname, birthDate);
        this.enrollmentDate = enrollmentDate;
        this.expirationDate = expirationDate;
        this.memberType = memberType;
    }

    public Member(int id, int accountId, Date enrollmentDate, Date expirationDate, MemberType memberType, List<Rating> ratings, List<Reservation> reservations, List<Borrowing> borrowings) {
        this.id = id;
        this.accountId = accountId;
        this.enrollmentDate = enrollmentDate;
        this.expirationDate = expirationDate;
        this.memberType = memberType;
        this.ratings = ratings;
        this.reservations = reservations;
        this.borrowings = borrowings;
    }

    public Member(String JMBG, String name, String surname, Date birthDate, int accountId, Date enrollmentDate, Date expirationDate, MemberType memberType) {
        super(JMBG, name, surname, birthDate);
        this.accountId = accountId;
        this.enrollmentDate = enrollmentDate;
        this.expirationDate = expirationDate;
        this.memberType = memberType;
    }

    public Member(String JMBG, String name, String surname, Date birthDate, int id, int accountId, Date enrollmentDate, Date expirationDate, MemberType memberType, List<Rating> ratings, List<Reservation> reservations, List<Borrowing> borrowings) {
        super(JMBG, name, surname, birthDate);
        this.id = id;
        this.accountId = accountId;
        this.enrollmentDate = enrollmentDate;
        this.expirationDate = expirationDate;
        this.memberType = memberType;
        this.ratings = ratings;
        this.reservations = reservations;
        this.borrowings = borrowings;
    }

    public Member(String JMBG, String name, String surname, Date birthDate, int id, int accountId, Date enrollmentDate, Date expirationDate, MemberType memberType) {
        super(JMBG, name, surname, birthDate);
        this.id = id;
        this.accountId = accountId;
        this.enrollmentDate = enrollmentDate;
        this.expirationDate = expirationDate;
        this.memberType = memberType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public Date getEnrollmentDate() {
        return enrollmentDate;
    }

    public void setEnrollmentDate(Date enrollmentDate) {
        this.enrollmentDate = enrollmentDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public MemberType getMemberType() {
        return memberType;
    }

    public void setMemberType(MemberType memberType) throws InvalidMemberTypeException {
        if (memberType == null) throw new InvalidMemberTypeException();
        this.memberType = memberType;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public List<Borrowing> getBorrowings() {
        return borrowings;
    }

    public void setBorrowings(List<Borrowing> borrowings) {
        this.borrowings = borrowings;
    }

    public UserAccount getUser_acc() {
        return user_acc;
    }

    public void setUser_acc(UserAccount user_acc) {
        this.user_acc = user_acc;
    }

    @Override
    public String toString() {
        return "Member{" +
                "id=" + id +
                ", accountId=" + accountId +
                ", enrollmentDate=" + enrollmentDate +
                ", expirationDate=" + expirationDate +
                ", memberType=" + memberType +
                ", ratings=" + ratings +
                ", reservations=" + reservations +
                ", borrowings=" + borrowings +
                ", acount=" + user_acc.getAccountId() +
                '}';
    }

    public Object toSearchCell(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return this.name;
            case 1:
                return this.surname;
            case 2:
                return this.id;
            case 3:
                return this.typeTypeToString();
            case 4:
                return dateToString(this.birthDate);
            default:
                return " ";
        }
    }

    public String typeTypeToString() {
        switch (getMemberType()) {
            case HONORABLE:
                return "Pocasni clan";
            case PRESCHOOLER:
                return "Predskolac";
            case STUDENT:
                return "Ucenik / Student";
            case PENSIONER:
                return "Penzioner";
            default:
                return "Zaposleni";
        }
    }

    public void setProperty(int col, Object value) throws ParseException, InvalidMemberTypeException {
        switch (col) {
            case 0:
                this.setName((String) value);
                break;
            case 1:
                this.setSurname((String) value);
                break;
            case 3:
                this.setMemberType(MemberType.stringToType((String) value));
                break;
            case 4:
                this.setBirthDate(stringToDate((String) value));
                break;
            default:

        }
    }

    private Date stringToDate(String value) throws ParseException {
        return new SimpleDateFormat("dd-MM-yyyy").parse(value);
    }

    private String dateToString(Date date) {
        return new SimpleDateFormat("dd-MM-yyyy").format(date);
    }

}
