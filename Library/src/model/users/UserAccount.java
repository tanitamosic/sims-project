package model.users;

import javax.persistence.*;

@Entity
@Table(name = "ACCOUNT")
public class UserAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ACC")
    @SequenceGenerator(name = "SEQ_ACC", sequenceName = "ACCOUNT_SEQ", allocationSize = 1)
    @Column(name = "ACCOUNTID", updatable = false, nullable = false)
    private int accountId;
    private String username;
    private String password;
    private boolean isActive;

    @Column(name = "ACCOUNTTYPE")
    private String typeofacc;

    @Transient
    private User user;

    public UserAccount() {
    }

    public UserAccount(int accountId, String username, String password, boolean isActive, String typeofacc) {
        this.accountId = accountId;
        this.username = username;
        this.password = password;
        this.isActive = isActive;
        this.typeofacc = typeofacc;
    }

    public UserAccount(String username, String password, boolean isActive, String typeofacc) {
        this.username = username;
        this.password = password;
        this.isActive = isActive;
        this.typeofacc = typeofacc;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public String getTypeofacc() {
        return typeofacc;
    }

    public void setTypeofacc(String typeofacc) {
        this.typeofacc = typeofacc;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "UserAccount{" +
                "accountId=" + accountId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", isActive=" + isActive +
                ", typeofacc='" + typeofacc + '\'' +
                ", user=" + user +
                '}';
    }
}
