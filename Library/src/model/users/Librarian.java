package model.users;

import model.enums.LibrarianRole;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "LIBRARIAN")
@SecondaryTable(name = "LIBRARIAN_ROLE")
public class Librarian extends User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_LIBRARIAN")
    @SequenceGenerator(name = "SEQ_LIBRARIAN", sequenceName = "LIBRARIAN_SEQ", allocationSize = 1)
    @Column(name = "LIBRARIANID", updatable = false, nullable = false)
    private int librarianId;

    @Column(name = "ACCOUNTID")
    private int accountId;

    @Transient
    private List<LibrarianRole> roles;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "accountId", insertable = false, updatable = true)
    private UserAccount user_acc;

    public Librarian() {
    }

    public Librarian(String JMBG, String name, String surname, Date birthDate, int accountId) {
        super(JMBG, name, surname, birthDate);
        this.accountId = accountId;
    }

    public Librarian(String JMBG, String name, String surname, Date birthDate) {
        super(JMBG, name, surname, birthDate);
    }

    public Librarian(int librarianId, int accountId) {
        this.librarianId = librarianId;
        this.accountId = accountId;
    }

    public Librarian(String JMBG, String name, String surname, Date birthDate, int librarianId, int accountId) {
        super(JMBG, name, surname, birthDate);
        this.librarianId = librarianId;
        this.accountId = accountId;
    }

    public int getLibrarianId() {
        return librarianId;
    }

    public void setLibrarianId(int librarianId) {
        this.librarianId = librarianId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public List<LibrarianRole> getRoles() {
        return roles;
    }

    public void setRoles(List<LibrarianRole> roles) {
        this.roles = roles;
    }

    public UserAccount getUser_acc() {
        return user_acc;
    }

    public void setUser_acc(UserAccount user_acc) {
        this.user_acc = user_acc;
    }

    @Override
    public String toString() {
        return "Librarian{" +
                "librarianId=" + librarianId +
                ", accountId=" + accountId +
                ", roles=" + roles +
                ", JMBG='" + JMBG + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + birthDate +
                '}';
    }
}
