package model.users;

import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
public abstract class User {
    protected String JMBG;
    protected String name;
    protected String surname;
    protected Date birthDate;

    public User() {
    }

    public User(String JMBG, String name, String surname, Date birthDate) {
        this.JMBG = JMBG;
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public String getJMBG() {
        return JMBG;
    }

    public void setJMBG(String JMBG) {
        this.JMBG = JMBG;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}


