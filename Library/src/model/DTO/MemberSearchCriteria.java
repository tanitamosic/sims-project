package model.DTO;

public class MemberSearchCriteria {
    private String name;
    private String surname;
    private String id;
    private int type;


    public MemberSearchCriteria(String n, String s, String memId, int t) {
        this.name = n;
        this.surname = s;
        this.id = memId;
        this.type = t;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getId() {
        return id;
    }

    public int getType() {
        return type;
    }
}
