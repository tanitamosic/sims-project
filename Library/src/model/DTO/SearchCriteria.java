package model.DTO;

public class SearchCriteria {

    private String title;
    private String author;
    private String tags;
    private String genre;

    public SearchCriteria(String title, String author, String tags, String genre) {
        this.title = title;
        this.author = author;
        this.tags = tags;
        this.genre = genre;
    }

    public SearchCriteria() {
        this.title = "";
        this.author = "";
        this.tags = "";
        this.genre = "-";
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getTags() {
        return tags;
    }

    public String getGenre() {
        return genre;
    }
}
