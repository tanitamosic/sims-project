package model.DTO;

import java.util.Date;

public class DateInterval {

    private Date startDate;
    private Date endDate;

    public DateInterval(Date startDate, Date endDate) {
        if (startDate == null) startDate = new Date(Long.MIN_VALUE);
        this.startDate = startDate;
        if (endDate == null) endDate = new Date(Long.MAX_VALUE);
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

}
