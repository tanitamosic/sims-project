package model.enums;

public enum Section {
    MISCELLANEOUS, CHILDREN
}
