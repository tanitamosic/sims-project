package model.enums;

public enum MemberType {
    HONORABLE, PRESCHOOLER, STUDENT, REGULAR, PENSIONER;

    private static String[] names = {"Pocasni", "Predskolac", "Student / Ucenik", "Zaposleni", "Penzioner"};

    @Override
    public String toString()
    {
        return names[this.ordinal()];
    }

    public static String[] getNames()
    {
        return names;
    }

    public static MemberType stringToType(String s)
    {
        for (MemberType mt : MemberType.values())
        {
            if (mt.toString().equalsIgnoreCase(s)) return mt;
        }
        return null;
    }


}






