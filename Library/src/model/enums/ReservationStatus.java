package model.enums;

public enum ReservationStatus {
    PENDING, APPROVED, MISSED, FULFILLED, DENIED
}
