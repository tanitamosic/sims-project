package model.enums;

public enum AuthorshipType {
    PREFACE_WRITER("prolog"), AUTHOR("pisac"), REVIEWER("lektor"),
    TRANSLATOR("prevodilac"), ILLUSTRATOR("ilustrator"), EDITOR("urednik");

    public final String label;

    private AuthorshipType(String label) {
        this.label = label;
    }
}
