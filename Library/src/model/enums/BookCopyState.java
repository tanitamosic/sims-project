package model.enums;

public enum BookCopyState {
    FREE,
    BORROWED,
    RESERVED,
    IN_REPAIR
}
