package model.enums;

public enum LibrarianRole {
    ADMINISTRATOR,
    FRONT_DESK,
    CATALOGUER
}
