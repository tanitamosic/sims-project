package model.enums;

public enum Genre {
    HISTORY,
    CLASSICAL_LITERATURE,
    POETRY,
    THRILLER,
    CHILDREN,
    SCIENTIFIC_LITERATURE,
    FANTASY,
    SCIENCE_FICTION,
    DRAMA,
    AUTOBIOGRAPHY,
    COMEDY;

    private static String[] names = {"Istorijski", "Klasicni", "Poezija", "Triler", "Decji", "Naucni", "Fantasticni",
            "Naucna fantastika", "Drama", "Autobiografski", "Komedija"};


    @Override
    public String toString() {
        return names[this.ordinal()];
    }

    public static String[] getNames() {
        return names;
    }
}
