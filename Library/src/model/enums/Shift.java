package model.enums;

public enum Shift {
    MORNING, AFTERNOON
}
