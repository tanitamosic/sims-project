package model.library;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PLACE")
public class Place {
    @Id
    private int pttNumber;
    private String name;

    public Place() {
    }

    public Place(int pttNumber, String name) {
        this.pttNumber = pttNumber;
        this.name = name;
    }

    public int getPttNumber() {
        return pttNumber;
    }

    public void setPttNumber(int pttNumber) {
        this.pttNumber = pttNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Place{" +
                "pttNumber=" + pttNumber +
                ", name='" + name + '\'' +
                '}';
    }
}
