package model.library;

import model.enums.Shift;
import model.users.Member;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "PAYMENT")
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PAY")
    @SequenceGenerator(name = "SEQ_PAY", sequenceName = "PAYMENT_SEQ", allocationSize = 1)
    @Column(name = "PAYMENTID", updatable = false, nullable = false)
    private int paymentId;

    @Column(name = "PAYMENTDATE")
    private Date date;

    @Column(name = "MEMBERID")
    private int memberID;

    private double amount;
    private Shift shift;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", insertable = false, updatable = true)
    private Member member;

    public Payment() {
    }

    public Payment(Date date, int memberID, double amount, Shift shift) {
        this.date = date;
        this.memberID = memberID;
        this.amount = amount;
        this.shift = shift;
    }

    public Payment(Date date, int memberID, double amount, Shift shift, Member member) {
        this.date = date;
        this.memberID = memberID;
        this.amount = amount;
        this.shift = shift;
        this.member = member;
    }

    public Payment(int paymentId, int memberId, Date date, double amount, Shift shift, Member member) {
        this.paymentId = paymentId;
        this.memberID = memberId;
        this.date = date;
        this.amount = amount;
        this.shift = shift;
        this.member = member;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public int getMemberID() {
        return memberID;
    }

    public void setMemberID(int memberID) {
        this.memberID = memberID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Shift getShift() {
        return shift;
    }

    public void setShift(Shift shift) {
        this.shift = shift;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "paymentId=" + paymentId +
                ", date=" + date +
                ", amount=" + amount +
                ", shift=" + shift +
                ", member=" + member.getId() +
                '}';
    }
}
