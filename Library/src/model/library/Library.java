package model.library;
import repos.*;

import javax.persistence.*;

@Entity
@Table(name = "LIBRARY")
public class Library {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_LIB")
    @SequenceGenerator(name = "SEQ_LIB", sequenceName = "LIBRARY_SEQ", allocationSize = 1)
    @Column(name = "LIBRARYID", updatable = false, nullable = false)
    private int libraryId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "placeId")
    private Place headOfficePlace;

    @Column(name = "ADDRESS")
    private String headOfficeAddress;

    @Transient
    private BrancheRepo brancheRepo;

    @Transient
    private AccountRepo accountRepo;

    @Transient
    private LibrarianRepo librarianRepo;

    @Transient
    private MemberRepo memberRepo;

    @Transient
    private PaymentRepo paymentRepo;

    @Transient
    private BookRepo bookRepo;

    @Transient
    private PriceRepo priceRepo;

    @Transient
    private BorrowingRepo borrowingRepo;

    public Library() {
    }

    public Library(int libraryId, Place headOfficePlace, String headOfficeAddress) {
        this.libraryId = libraryId;
        this.headOfficePlace = headOfficePlace;
        this.headOfficeAddress = headOfficeAddress;
    }

    public Library(Place headOfficePlace, String headOfficeAddress) {
        this.headOfficePlace = headOfficePlace;
        this.headOfficeAddress = headOfficeAddress;
    }

    public int getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(int libraryId) {
        this.libraryId = libraryId;
    }

    public Place getHeadOfficePlace() {
        return headOfficePlace;
    }

    public void setHeadOfficePlace(Place headOfficePlace) {
        this.headOfficePlace = headOfficePlace;
    }

    public String getHeadOfficeAddress() {
        return headOfficeAddress;
    }

    public void setHeadOfficeAddress(String headOfficeAddress) {
        this.headOfficeAddress = headOfficeAddress;
    }

    public BrancheRepo getBrancheRepo() {
        return brancheRepo;
    }

    public void setBrancheRepo(BrancheRepo brancheRepo) {
        this.brancheRepo = brancheRepo;
    }

    public AccountRepo getAccountRepo() {
        return accountRepo;
    }

    public void setAccountRepo(AccountRepo accountRepo) {
        this.accountRepo = accountRepo;
    }

    public LibrarianRepo getLibrarianRepo() {
        return librarianRepo;
    }

    public void setLibrarianRepo(LibrarianRepo librarianRepo) {
        this.librarianRepo = librarianRepo;
    }

    public MemberRepo getMemberRepo() {
        return memberRepo;
    }

    public void setMemberRepo(MemberRepo memberRepo) {
        this.memberRepo = memberRepo;
    }

    public PaymentRepo getPaymentRepo() {
        return paymentRepo;
    }

    public void setPaymentRepo(PaymentRepo paymentRepo) {
        this.paymentRepo = paymentRepo;
    }

    public BookRepo getBookRepo() {
        return bookRepo;
    }

    public void setBookRepo(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }

    public PriceRepo getPriceRepo() {
        return priceRepo;
    }

    public void setPriceRepo(PriceRepo priceRepo) {
        this.priceRepo = priceRepo;
    }

    public BorrowingRepo getBorrowingRepo() {
        return borrowingRepo;
    }

    public void setBorrowingRepo(BorrowingRepo borrowingRepo) {
        this.borrowingRepo = borrowingRepo;
    }

    @Override
    public String toString() {
        return "Library{" +
                "libraryId=" + libraryId +
                ", headOfficePlace=" + headOfficePlace +
                ", headOfficeAddress='" + headOfficeAddress + '\'' +
                ", branches=" + brancheRepo +
                ", accounts=" + accountRepo +
                ", librarians=" + librarianRepo +
                ", members=" + memberRepo +
                ", payments=" + paymentRepo +
                ", books=" + bookRepo +
                ", prices=" + priceRepo +
                ", borrowings=" + borrowingRepo +
                '}';
    }
}
