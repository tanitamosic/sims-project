package model.library;

import model.users.Member;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "BORROWING")
public class Borrowing {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_BORR")
    @SequenceGenerator(name = "SEQ_BORR", sequenceName = "BORROWING_SEQ", allocationSize = 1)
    @Column(name = "BORROWINGID", updatable = false, nullable = false)
    private int borrowingId;

    @Column(name = "MEMBERID")
    private int memberId;

    @Column(name = "NUMOFPICTUREBOOKS")
    private int numberOfPictureBooks = 0;
    private Date borrowingDate;

    @Basic(fetch = FetchType.LAZY)
    @OneToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "BORROWEDCOPY",
            joinColumns =  @JoinColumn(name = "borrowingID"),
            inverseJoinColumns = @JoinColumn(name = "bcopyId")
    )
    private List<BorrowedCopy> borrowedCopies;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", insertable = false, updatable = false)
    private Member member;

    public Borrowing() {
    }

    public Borrowing(int memberId, int numberOfPictureBooks, Date borrowingDate) {
        this.memberId = memberId;
        this.numberOfPictureBooks = numberOfPictureBooks;
        this.borrowingDate = borrowingDate;
    }

    public Borrowing(int borrowingId, int memberId, int numberOfPictureBooks, Date borrowingDate, List<BorrowedCopy> borrowedCopies, Member member) {
        this.borrowingId = borrowingId;
        this.memberId = memberId;
        this.numberOfPictureBooks = numberOfPictureBooks;
        this.borrowingDate = borrowingDate;
        this.borrowedCopies = borrowedCopies;
        this.member = member;
    }

    public int getBorrowingId() {
        return borrowingId;
    }

    public void setBorrowingId(int borrowingId) {
        this.borrowingId = borrowingId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public int getNumberOfPictureBooks() {
        return numberOfPictureBooks;
    }

    public void setNumberOfPictureBooks(int numberOfPictureBooks) {
        this.numberOfPictureBooks = numberOfPictureBooks;
    }

    public Date getBorrowingDate() {
        return borrowingDate;
    }

    public void setBorrowingDate(Date borrowingDate) {
        this.borrowingDate = borrowingDate;
    }

    public List<BorrowedCopy> getBorrowedCopies() {
        return borrowedCopies;
    }

    public void setBorrowedCopies(List<BorrowedCopy> borrowedCopies) {
        this.borrowedCopies = borrowedCopies;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    @Override
    public String toString() {
        return "Borrowing{" +
                "borrowingId=" + borrowingId +
                ", memberId=" + memberId +
                ", numberOfPictureBooks=" + numberOfPictureBooks +
                ", borrowingDate=" + borrowingDate +
                ", borrowedCopies=" + borrowedCopies +
                ", member=" + member.getId() +
                '}';
    }
}
