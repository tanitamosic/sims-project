package model.library;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "PRICELIST")
public class PriceListing {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PL")
    @SequenceGenerator(name = "SEQ_PL", sequenceName = "PRICE_SEQ", allocationSize = 1)
    @Column(name = "PLID", updatable = false, nullable = false)
    private int paymentId;
    private Date startDate;
    private Date endDate;
    private double basePrice;

    public PriceListing() {
    }

    public PriceListing(Date startDate, Date endDate, double basePrice) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.basePrice = basePrice;
    }

    public PriceListing(int paymentId, Date startDate, Date endDate, double basePrice) {
        this.paymentId = paymentId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.basePrice = basePrice;
    }

    public int getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    @Override
    public String toString() {
        return "PriceListing{" +
                "paymentId=" + paymentId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", basePrice=" + basePrice +
                '}';
    }
}
