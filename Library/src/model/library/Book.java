package model.library;

import model.enums.Genre;

import javax.persistence.*;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "BOOK")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_BOOK")
    @SequenceGenerator(name = "SEQ_BOOK", sequenceName = "BOOK_SEQ", allocationSize = 1)
    @Column(name = "BOOKID", updatable = false, nullable = false)
    private int id;
    private String title;
    private int year;
    @Column(name = "DIMENSIONS")
    private int dimensions;
    @Transient
    private Dimensions dimension;
    private String publisher;

    @Column(name = "shelf")
    private int shelfId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "shelf", insertable = false, updatable = false)
    private Shelf shelfF;

    private Genre genre;
    private boolean carryOut = false;

    @Column(name = "TAGS")
    private String tags;

    @Basic(fetch = FetchType.LAZY)
    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "BOOK_AUTHOR",
            joinColumns =  @JoinColumn(name = "bookID"),
            inverseJoinColumns = @JoinColumn(name = "authorID")
    )
    private List<Author> authors;

    @Basic(fetch = FetchType.LAZY)
    @OneToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "BOOKCOPY",
            joinColumns =  @JoinColumn(name = "bookID"),
            inverseJoinColumns = @JoinColumn(name = "copyID")
    )
    private List<BookCopy> copies;

    @Basic(fetch = FetchType.LAZY)
    @OneToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "rating",
            joinColumns =  @JoinColumn(name = "bookID"),
            inverseJoinColumns = @JoinColumn(name = "ratingId")
    )
    private List<Rating> ratings;

    public Book() {
    }

    public Book(String title, int year, int dimensions, String publisher, int shelfId, Genre genre, boolean carryOut, String tags) {
        this.title = title;
        this.year = year;
        this.dimensions = dimensions;
        this.publisher = publisher;
        this.shelfId = shelfId;
        this.genre = genre;
        this.carryOut = carryOut;
        this.tags = tags;
    }

    public Book(String title, int year, int dimensions, String publisher, int shelfId, Genre genre, boolean carryOut, String tags, List<Author> authors) {
        this.title = title;
        this.year = year;
        this.dimensions = dimensions;
        this.publisher = publisher;
        this.shelfId = shelfId;
        this.genre = genre;
        this.carryOut = carryOut;
        this.tags = tags;
        this.authors = authors;
    }

    public Book(int id, String title, int year, int dimensions, Dimensions dimension, String publisher, Shelf shelfF, Genre genre, boolean carryOut, String tags, List<Author> authors, List<BookCopy> copies, List<Rating> ratings) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.dimensions = dimensions;
        this.dimension = dimension;
        this.publisher = publisher;
        this.shelfF = shelfF;
        this.genre = genre;
        this.carryOut = carryOut;
        this.tags = tags;
        this.authors = authors;
        this.copies = copies;
        this.ratings = ratings;
    }

    public Book(int id, String title, int year, int dimensions, String publisher, int shelfF, Genre genre, boolean carryOut, String tags) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.dimensions = dimensions;
        this.publisher = publisher;
        this.shelfId = shelfF;
        this.genre = genre;
        this.carryOut = carryOut;
        this.tags = tags;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getShelfId() {
        return shelfId;
    }

    public void setShelfId(int shelfId) {
        this.shelfId = shelfId;
    }

    public int getDimensions() {
        return dimensions;
    }

    public void setDimensions(int dimensions) {
        this.dimensions = dimensions;
    }

    public Dimensions getDimension() {
        return dimension;
    }

    public void setDimension(Dimensions dimension) {
        this.dimension = dimension;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Shelf getShelfF() {
        return shelfF;
    }

    public void setShelfF(Shelf shelfF) {
        this.shelfF = shelfF;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public boolean isCarryOut() {
        return carryOut;
    }

    public void setCarryOut(boolean carryOut) {
        this.carryOut = carryOut;
    }



    public List<String> getTags() {
        return Arrays.asList(tags.split(";"));
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public List<BookCopy> getCopies() {
        return copies;
    }

    public void setCopies(List<BookCopy> copies) {
        this.copies = copies;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", year=" + year +
                ", dimensions=" + dimensions +
                ", dimension=" + dimension +
                ", publisher='" + publisher + '\'' +
                ", shelfId=" + shelfId +
                ", tags=" + tags +
                ", genre=" + genre +
                ", carryOut=" + carryOut +
                ", authors=" + authors +
                ", copies=" + copies +
                ", ratings=" + ratings +
                '}';
    }

    public String[] getAuthorsNames() {
        String[] authorsNames = new String[authors.size()];
        for (int i = 0; i < authors.size(); i++) {
            authorsNames[i] = authors.get(i).getName() + " " + authors.get(i).getSurname();
        }
        return authorsNames;
    }

    public double getAverageRatingScore() {
        if (ratings.size() == 0) return 0;
        double sum = 0;
        for (Rating r : ratings) sum += r.getNumOfStars();
        return sum / ratings.size();
    }
}
