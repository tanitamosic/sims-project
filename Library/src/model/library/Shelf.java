package model.library;

import model.enums.Section;

import javax.persistence.*;

@Entity
@Table(name = "SHELF")
public class Shelf {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_SHELF")
    @SequenceGenerator(name = "SEQ_SHELF", sequenceName = "SHELF_SEQ", allocationSize = 1)
    @Column(name = "SHELFID", updatable = false, nullable = false)
    private int shelfid;
    private Section section;
    private int position;

    public Shelf() {
    }

    public Shelf(int shelfid, Section section, int position) {
        this.shelfid = shelfid;
        this.section = section;
        this.position = position;
    }

    public Shelf(Section section, int position) {
        this.section = section;
        this.position = position;
    }

    public int getShelfid() {
        return shelfid;
    }

    public void setShelfid(int shelfid) {
        this.shelfid = shelfid;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Shelf{" +
                "shelfid=" + shelfid +
                ", section=" + section +
                ", position=" + position +
                '}';
    }
}
