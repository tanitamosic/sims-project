package model.library;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "LIBRARY")
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_LIB")
    @SequenceGenerator(name = "SEQ_LIB", sequenceName = "LIBRARY_SEQ", allocationSize = 1)
    @Column(name = "LIBRARYID", updatable = false, nullable = false)
    private int libraryId;

    @Column(name = "PLACEID")
    private int placeId;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "placeId")
    private Place place;

    private String address;

    @Basic(fetch = FetchType.LAZY)
    @OneToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "BOOK",
            joinColumns = @JoinColumn(name = "bookID"),
            inverseJoinColumns = @JoinColumn(name = "libraryID")
    )
    private List<Book> books;

    public Branch() {
    }

    public Branch(int libraryId, int placeId, Place place, String address, List<Book> books) {
        this.libraryId = libraryId;
        this.placeId = placeId;
        this.place = place;
        this.address = address;
        this.books = books;
    }

    public int getLibraryId() {
        return libraryId;
    }

    public void setLibraryId(int libraryId) {
        this.libraryId = libraryId;
    }

    public int getPlaceId() {
        return placeId;
    }

    public void setPlaceId(int placeId) {
        this.placeId = placeId;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return "Branch{" +
                "libraryId=" + libraryId +
                ", placeId=" + placeId +
                ", place=" + place +
                ", address='" + address + '\'' +
                ", books=" + books +
                '}';
    }
}
