package model.library;

import model.enums.AuthorshipType;

import javax.persistence.*;

@Entity
@Table(name = "AUTHOR")
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_AUTH")
    @SequenceGenerator(name = "SEQ_AUTH", sequenceName = "AUTHOR_SEQ", allocationSize = 1)
    @Column(name = "AUTHORID", updatable = false, nullable = false)
    private int authorId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "AUTHORSHIPTYPE")
    private AuthorshipType authorshipType;

    public Author() {
    }

    public Author(int authorId, String name, String surname, AuthorshipType authorshipType) {
        this.authorId = authorId;
        this.name = name;
        this.surname = surname;
        this.authorshipType = authorshipType;
    }

    public Author(String name, String surname, AuthorshipType authorshipType) {
        this.name = name;
        this.surname = surname;
        this.authorshipType = authorshipType;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public AuthorshipType getAuthorshipType() {
        return authorshipType;
    }

    public void setAuthorshipType(AuthorshipType authorshipType) {
        this.authorshipType = authorshipType;
    }

    @Override
    public String toString() {
        return "Author{" +
                "authorId=" + authorId +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", authorshipType=" + authorshipType +
                '}';
    }
}
