package model.library;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "BORROWEDCOPY")
public class BorrowedCopy {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_BCOPY")
    @SequenceGenerator(name = "SEQ_BCOPY", sequenceName = "BORROWEDCOPY_SEQ", allocationSize = 1)
    @Column(name = "BCOPYID", updatable = false, nullable = false)
    private int bcopyID;

    @Column(name = "BORROWINGID")
    private int borrowingid;

    @Column(name = "COPYID")
    private int copyId;

    private Date returnDate;
    private boolean prolonged = false;
    private Date returnDeadline;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "borrowingId", insertable = false, updatable = false)
    private Borrowing borrowing;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "copyId", insertable = false, updatable = false)
    private BookCopy copy;

    public BorrowedCopy() {
    }

    public BorrowedCopy(int borrowingid, int copyId, Date returnDate, boolean prolonged, Date returnDeadline) {
        this.borrowingid = borrowingid;
        this.copyId = copyId;
        this.returnDate = returnDate;
        this.prolonged = prolonged;
        this.returnDeadline = returnDeadline;
    }

    public BorrowedCopy(int bcopyID, int borrowingid, int copyId, Date returnDate, boolean prolonged, Date returnDeadline) {
        this.bcopyID = bcopyID;
        this.borrowingid = borrowingid;
        this.copyId = copyId;
        this.returnDate = returnDate;
        this.prolonged = prolonged;
        this.returnDeadline = returnDeadline;
    }

    public int getBcopyID() {
        return bcopyID;
    }

    public void setBcopyID(int bcopyID) {
        this.bcopyID = bcopyID;
    }

    public int getBorrowingid() {
        return borrowingid;
    }

    public void setBorrowingid(int borrowingid) {
        this.borrowingid = borrowingid;
    }

    public int getCopyId() {
        return copyId;
    }

    public void setCopyId(int copyId) {
        this.copyId = copyId;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public boolean isProlonged() {
        return prolonged;
    }

    public void setProlonged(boolean prolonged) {
        this.prolonged = prolonged;
    }

    public Date getReturnDeadline() {
        return returnDeadline;
    }

    public void setReturnDeadline(Date returnDeadline) {
        this.returnDeadline = returnDeadline;
    }

    public Borrowing getBorrowing() {
        return borrowing;
    }

    public void setBorrowing(Borrowing borrowing) {
        this.borrowing = borrowing;
    }

    public BookCopy getCopy() {
        return copy;
    }

    public void setCopy(BookCopy copy) {
        this.copy = copy;
    }

    @Override
    public String toString() {
        return "BorrowedCopy{" +
                "bcopyID=" + bcopyID +
                ", borrowingid=" + borrowingid +
                ", copyId=" + copyId +
                ", returnDate=" + returnDate +
                ", prolonged=" + prolonged +
                ", returnDeadline=" + returnDeadline +
                ", borrowing=" + borrowing.getBorrowingId() +
                '}';
    }
}
