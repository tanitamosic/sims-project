package model.library;

import model.users.Member;

import javax.persistence.*;

@Entity
@Table(name = "RATING")
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_RATING")
    @SequenceGenerator(name = "SEQ_RATING", sequenceName = "RATING_SEQ", allocationSize = 1)
    @Column(name = "RATINGID", updatable = false, nullable = false)
    private int ratingID;

    @Column(name = "MEMBERID")
    private int memberID;

    @Column(name = "BOOKID")
    private int bookID;

    private int numOfStars = 1;

    @Column(name = "TEXT")
    private String comment;

    @Column(name = "APPROVED")
    private boolean approved;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", insertable = false, updatable = false)
    private Member member;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bookID", insertable = false, updatable = false)
    private Book book;

    public Rating() {
    }

    public Rating(int memberID, int bookID, int numOfStars, String comment) {
        this.memberID = memberID;
        this.bookID = bookID;
        this.numOfStars = numOfStars;
        this.comment = comment;
    }

    public Rating(int memberID, int bookID, int numOfStars, String comment, boolean approved) {
        this.memberID = memberID;
        this.bookID = bookID;
        this.numOfStars = numOfStars;
        this.comment = comment;
        this.approved = approved;
    }

    public Rating(int ratingID, int memberID, int bookID, int numOfStars, String comment) {
        this.ratingID = ratingID;
        this.memberID = memberID;
        this.bookID = bookID;
        this.numOfStars = numOfStars;
        this.comment = comment;
    }

    public int getRatingID() {
        return ratingID;
    }

    public void setRatingID(int ratingID) {
        this.ratingID = ratingID;
    }

    public int getMemberID() {
        return memberID;
    }

    public void setMemberID(int memberID) {
        this.memberID = memberID;
    }

    public int getBookID() {
        return bookID;
    }

    public void setBookID(int bookID) {
        this.bookID = bookID;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public int getNumOfStars() {
        return numOfStars;
    }

    public void setNumOfStars(int numOfStars) {
        this.numOfStars = numOfStars;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public boolean isApproved() {
        return approved;
    }

    @Override
    public String toString() {
        return "Rating{" +
                "ratingID=" + ratingID +
                ", memberID=" + memberID +
                ", bookID=" + bookID +
                ", numOfStars=" + numOfStars +
                ", appr=" + approved +
                '}';
    }
}
