package model.library;

import javax.persistence.*;

@Entity
@Table(name = "DIMENSION")
public class Dimensions {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_DIM")
    @SequenceGenerator(name = "SEQ_DIM", sequenceName = "DIMENSION_SEQ", allocationSize = 1)
    @Column(name = "DIMENSIONID", updatable = false, nullable = false)
    private int dimensionId;
    private float width;
    private float height;
    private float depth;

    public Dimensions() {
    }

    public Dimensions(int dimensionId, float width, float height, float depth) {
        this.dimensionId = dimensionId;
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public Dimensions(float width, float height, float depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public int getDimensionId() {
        return dimensionId;
    }

    public void setDimensionId(int dimensionId) {
        this.dimensionId = dimensionId;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getDepth() {
        return depth;
    }

    public void setDepth(float depth) {
        this.depth = depth;
    }

    @Override
    public String toString() {
        return "Dimensions{" +
                "dimensionId=" + dimensionId +
                ", width=" + width +
                ", height=" + height +
                ", depth=" + depth +
                '}';
    }
}
