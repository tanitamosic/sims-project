package model.library;

import model.enums.BookCopyState;
import model.states.State;

import javax.persistence.*;

@Entity
@Table(name = "BOOKCOPY")
public class BookCopy {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_COPY")
    @SequenceGenerator(name = "SEQ_COPY", sequenceName = "BOOKCOPY_SEQ", allocationSize = 1)
    @Column(name = "COPYID", updatable = false, nullable = false)
    private int copyId;

    @Column(name = "BOOKID")
    private int bookId;
    private String code;

    @Column(name = "STATE")
    private BookCopyState state;

    @Transient
    private State currentState;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bookId", insertable = false, updatable = false)
    private Book book;

    public BookCopy() {
    }

    public BookCopy(int bookId, String code, BookCopyState state) {
        this.bookId = bookId;
        this.code = code;
        this.state = state;
    }

    public BookCopy(int copyId, int bookId, String code, State currentState) {
        this.copyId = copyId;
        this.bookId = bookId;
        this.code = code;
        this.currentState = currentState;
    }

    public BookCopy(int copyId, int bookId, String code, BookCopyState currentState) {
        this.copyId = copyId;
        this.bookId = bookId;
        this.code = code;
        this.state = currentState;
    }

    public void changeState(State newState) {
        currentState.exit();
        currentState = newState;
        currentState.entry();
    }

    public void approvedForReservation() {}
    public void reservationMissed() {}
    public void returned() {}
    public void repaired() {}
    public void userPicksUpBook() {}

    public int getCopyId() {
        return copyId;
    }

    public void setCopyId(int copyId) {
        this.copyId = copyId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public BookCopyState getState() {
        return state;
    }

    public void setState(BookCopyState state) {
        this.state = state;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isAvailable() {
        return state == BookCopyState.FREE;
    }

    public State getCurrentState() {
        return currentState;
    }

    public void setCurrentState(State currentState) {
        this.currentState = currentState;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public String toString() {
        return "BookCopy{" +
                "copyId=" + copyId +
                ", code='" + code + '\'' +
                ", isAvailable=" + isAvailable() +
                ", currentState=" + currentState +
                ", book=" + book.getId() +
                '}';
    }
}
