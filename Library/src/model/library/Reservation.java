package model.library;

import model.enums.ReservationStatus;
import model.users.Member;

import javax.persistence.*;

@Entity
@Table(name = "RESERVATION")
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_RESERVATION")
    @SequenceGenerator(name = "SEQ_RESERVATION", sequenceName = "RESERVATION_SEQ", allocationSize = 1)
    @Column(name = "RESERVATIONID", updatable = false, nullable = false)
    private int reservationID;

    @Column(name = "BOOKID")
    private int bookID;

    @Column(name = "MEMBERID")
    private int memberID;

    private ReservationStatus status;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", insertable = false, updatable = false)
    private Member member;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bookId", insertable = false, updatable = false)
    private Book book;

    public Reservation() {
    }

    public Reservation(int bookID, int memberID, ReservationStatus status) {
        this.bookID = bookID;
        this.memberID = memberID;
        this.status = status;
    }

    public Reservation(int reservationID, int bookID, int memberID, ReservationStatus status) {
        this.reservationID = reservationID;
        this.bookID = bookID;
        this.memberID = memberID;
        this.status = status;
    }

    public int getReservationID() {
        return reservationID;
    }

    public void setReservationID(int reservationID) {
        this.reservationID = reservationID;
    }

    public int getBookID() {
        return bookID;
    }

    public void setBookID(int bookID) {
        this.bookID = bookID;
    }

    public int getMemberID() {
        return memberID;
    }

    public void setMemberID(int memberID) {
        this.memberID = memberID;
    }

    public ReservationStatus getStatus() {
        return status;
    }

    public void setStatus(ReservationStatus status) {
        this.status = status;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

}
