package model.states;

import model.library.BookCopy;

public abstract class State {
    private BookCopy context;

    public void entry() {}
    public void exit() {}
    public void approvedForReservation() {}
    public void reservationMissed() {}
    public void returned() {}
    public void repaired() {}
    public void userPicksUpBook() {}
}
