package model.states;

public class Free extends State {
    @Override
    public void entry() {

    }

    @Override
    public void exit() {

    }

    @Override
    public void approvedForReservation() {

    }

    @Override
    public void userPicksUpBook() {

    }
}
