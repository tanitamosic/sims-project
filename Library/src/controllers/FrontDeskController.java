package controllers;

import model.enums.ReservationStatus;
import model.library.Book;
import model.library.Reservation;
import model.users.Member;
import repos.BookRepo;
import repos.ReservationRepo;

import java.util.ArrayList;

public class FrontDeskController {
    ReservationRepo reservationRepo;
    BookRepo bookRepo;

    public FrontDeskController(ReservationRepo reservationRepo, BookRepo bookRepo){
        this.reservationRepo = reservationRepo;
        this.bookRepo = bookRepo;
    }

    public ArrayList<Reservation> getPendingReservations(){
        return reservationRepo.getPendingReservations();
    }

    public int getNumOfFreeCopies(Book book){
        return bookRepo.getNumOfFreeCopies(book);
    }

    public int getNumOfLoanedBooks(Member member){
        return bookRepo.getNumOfLoanedBooks(member.getId());
    }

    public void approveReservation(Reservation r){
        r.setStatus(ReservationStatus.APPROVED);
        reservationRepo.updateReservation(r);
    }

    public void declineReservation(Reservation r){
        r.setStatus(ReservationStatus.DENIED);
        reservationRepo.updateReservation(r);
    }
}
