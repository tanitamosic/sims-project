package controllers;

import application.Constants;
import exceptions.BorrowingAlreadyProlongedException;
import model.DTO.DateInterval;
import model.library.Book;
import model.library.BorrowedCopy;
import model.library.Borrowing;
import model.library.Reservation;
import model.users.Member;
import repos.BorrowingRepo;
import repos.ReservationRepo;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BorrowingController {

    private BorrowingRepo borrowingRepo;
    private Constants constants;

    public BorrowingController(BorrowingRepo borrowingRepo, Constants constants) {
        this.borrowingRepo = borrowingRepo;
        this.constants = constants;
    }

    public BorrowingController() {
        constants = new Constants();
    }

    public String getCardNum(Member user) {
        int id = user.getId();
        return Integer.toString(id);
    }

    public List<BorrowedCopy> getBorrowedCopies(Member member) {
        List<BorrowedCopy> borrowedCopies = new ArrayList<>();
        for (Borrowing borrowing : member.getBorrowings())
            borrowedCopies.addAll(borrowing.getBorrowedCopies());
        return borrowedCopies;
    }

    public List<BorrowedCopy> getBorrowedCopies(Member member, DateInterval interval) {
        List<BorrowedCopy> borrowedCopies = new ArrayList<>();
        for (Borrowing borrowing : member.getBorrowings())
            if (borrowing.getBorrowingDate().after(interval.getStartDate()) && borrowing.getBorrowingDate().before(interval.getEndDate()))
                borrowedCopies.addAll(borrowing.getBorrowedCopies());
        return borrowedCopies;
    }

    public List<BorrowedCopy> getCurrentlyBorrowedCopies(Member member) {
        List<BorrowedCopy> borrowedCopies = new ArrayList<>();
        for (Borrowing borrowing : member.getBorrowings())
            for (BorrowedCopy borrowedCopy : borrowing.getBorrowedCopies())
                if (borrowedCopy.getReturnDate() == null)
                    borrowedCopies.add(borrowedCopy);
        return borrowedCopies;
    }

    public boolean prolongBorrowedCopy(BorrowedCopy borrowedCopy, Member member){
        if (borrowedCopy.isProlonged()) return false;
        LocalDate currentDeadline = toLocalDate(borrowedCopy.getReturnDeadline());
        LocalDate prolongedDeadline = currentDeadline.plusDays(getNumOfProlongationDays(member));
        borrowedCopy.setReturnDeadline(toDate(prolongedDeadline));
        borrowedCopy.setProlonged(true);
        borrowingRepo.updateBorrowedCopy(borrowedCopy);
        return true;
    }

    private LocalDate toLocalDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    private Date toDate(LocalDate localDate) {
        return java.util.Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    private int getNumOfProlongationDays(Member member) {
        return constants.getNumOfProlongationDaysByType(member.getMemberType());
    }
}
