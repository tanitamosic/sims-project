package controllers;

import model.library.Rating;

import javax.swing.*;

public class CommentsTableController {

    private AdminController adminController;

    private String[] columnNames = {"Tekst komentara", "", ""};
    public static final int COMMENT_INDEX = 0;
    public static final int APPROVE_INDEX = 1;
    public static final int REJECT_INDEX = 2;

    public CommentsTableController(AdminController adminController) {
        this.adminController = adminController;
    }

    public int getRowCount() {
        return adminController.getRatingRepo().getPendingRatings().size();
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public String getColumnName(int column) {
        return columnNames[column];
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case COMMENT_INDEX:
                return String[].class;
            case APPROVE_INDEX:
            case REJECT_INDEX:
                return JButton.class;
            default:
                return Object.class;
        }
    }

    public Rating getRating(int rowIndex) {
        return adminController.getRatingRepo().getPendingRatings().get(rowIndex);
    }

    public void approveComment(Rating rating) {
        adminController.approveComment(rating);
    }

    public void rejectRating(Rating rating) {
        adminController.rejectRating(rating);
    }
}
