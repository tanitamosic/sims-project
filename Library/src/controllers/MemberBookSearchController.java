package controllers;

import exceptions.NoAvailableBookCopiesException;
import model.library.Book;
import model.users.Member;

import javax.swing.*;

public class MemberBookSearchController {

    private MemberController memberController;
    private LibraryController libraryController;

    // additional columns for reservation button and reviews button
    private String[] columnNames = {"Naslov", "Autori", "Izdavač", "Izdanje", "", ""};

    public static final int TITLE_INDEX = 0;
    public static final int AUTHORS_INDEX = 1;
    public static final int PUBLISHER_INDEX = 2;
    public static final int YEAR_INDEX = 3;
    public static final int RESERVE_BUTTON_INDEX = 4;
    public static final int REVIEWS_BUTTON_INDEX = 5;

    public MemberBookSearchController(MemberController memberController, LibraryController libraryController) {
        this.memberController = memberController;
        this.libraryController = libraryController;
    }

    public int getRowCount() {
        return libraryController.getSearchCandidates().size();
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public String getColumnName(int column) {
        return columnNames[column];
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case TITLE_INDEX:
            case PUBLISHER_INDEX:
                return String.class;
            case AUTHORS_INDEX:
                return String[].class;
            case YEAR_INDEX:
                return Integer.class;
            case RESERVE_BUTTON_INDEX:
            case REVIEWS_BUTTON_INDEX:
                return JButton.class;
            default:
                return Object.class;
        }
    }

    public Book getBook(int rowIndex) {
        return libraryController.getSearchCandidates().get(rowIndex);
    }

    public void reserveBook(Book book, Member member) throws NoAvailableBookCopiesException {
        if (!libraryController.canBeReserved(book)) throw new NoAvailableBookCopiesException();
        memberController.reserveBook(book, member);
    }

}
