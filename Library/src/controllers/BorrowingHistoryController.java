package controllers;

import model.DTO.DateInterval;
import model.library.BorrowedCopy;
import model.users.Member;

import javax.swing.*;
import java.util.Date;

public class BorrowingHistoryController {

    private MemberController memberController;
    private DateInterval dateInterval;

    // additional column for rating button
    private String[] columnNames = {"Naslov", "Autori", "Datum pozajmljivanja", "Datum vraćanja", "Rok za vraćanje", ""};
    public static final int TITLE_INDEX = 0;
    public static final int AUTHORS_INDEX = 1;
    public static final int BORROWING_DATE_INDEX = 2;
    public static final int RETURN_DATE_INDEX = 3;
    public static final int DEADLINE_INDEX = 4;
    public static final int RATING_BUTTON_INDEX = 5;

    public BorrowingHistoryController(MemberController memberController) {
        this.memberController = memberController;
    }

    public BorrowingHistoryController(){}

    public void setDateInterval(DateInterval dateInterval) {
        this.dateInterval = dateInterval;
    }

    public int getRowCount(Member member) {
        if (dateInterval == null)
            return memberController.getBorrowedCopies(member).size();
        return memberController.getBorrowedCopies(member, dateInterval).size();
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public BorrowedCopy getBorrowedCopy(int rowIndex, Member member) {
        if (dateInterval == null) return memberController.getBorrowedCopies(member).get(rowIndex);
        return memberController.getBorrowedCopies(member, dateInterval).get(rowIndex);
    }

    public String getColumnName(int column) {
        return columnNames[column];
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case TITLE_INDEX:
                return String.class;
            case AUTHORS_INDEX:
                return String[].class;
            case BORROWING_DATE_INDEX:
            case RETURN_DATE_INDEX:
            case DEADLINE_INDEX:
                return Date.class;
            case RATING_BUTTON_INDEX:
                return JButton.class;
            default:
                return Object.class;
        }
    }

    public boolean existsRating(Member member, BorrowedCopy borrowedCopy) {
        return memberController.existsRating(member, borrowedCopy);
    }

    public MemberController getMemberController() {
        return memberController;
    }
}
