package controllers;

import model.library.Book;

import javax.swing.*;

public class BorrowingBookSearchController {

    private LibraryController libraryController;
    private AdminController adminController;

    private final String[] columnNames = {"Naslov", "Autori", "Izdavač", "Izdanje", "Zanr / Polica", "Pozicija", "Broj slobodnih primjeraka", ""};
    public static final int TITLE_INDEX = 0;
    public static final int AUTHORS_INDEX = 1;
    public static final int PUBLISHER_INDEX = 2;
    public static final int YEAR_INDEX = 3;
    public static final int GENRE_INDEX = 4;
    public static final int POSITION_INDEX = 5;
    public static final int NUM_OF_FREE_INDEX = 6;
    public static final int BORROW_BUTTON_INDEX = 7;

    public BorrowingBookSearchController(LibraryController libraryController, AdminController adminController) {
        this.libraryController = libraryController;
        this.adminController = adminController;
    }

    public void eraseBorrowCandidates() {
        adminController.eraseBorrowCandidates();
    }

    public int getRowCount() {
        return libraryController.getSearchCandidates().size();
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public Book getBook(int rowIndex) {
        return libraryController.getSearchCandidates().get(rowIndex);
    }

    public int getNumOfFreeCopies(Book book) {
        return adminController.getBookRepo().getNumOfFreeCopies(book);
    }

    public boolean hasFreeCopies(Book book) {
        return adminController.getBookRepo().getNumOfFreeCopies(book) > 0;
    }

    public void addBorrowedCandidate(Book book) {
        adminController.addBorrowCandidate(book);
    }

    public String getColumnName(int column) {
        return columnNames[column];
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case TITLE_INDEX:
            case PUBLISHER_INDEX:
            case GENRE_INDEX:
                return String.class;
            case AUTHORS_INDEX:
                return String[].class;
            case YEAR_INDEX:
            case POSITION_INDEX:
            case NUM_OF_FREE_INDEX:
                return Integer.class;
            case BORROW_BUTTON_INDEX:
                return JButton.class;
            default:
                return Object.class;
        }
    }
}
