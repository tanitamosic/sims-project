package controllers;

import exceptions.RatingNotSubmittedException;
import model.library.BorrowedCopy;
import model.library.Rating;
import model.users.Member;
import repos.RatingRepo;

public class RatingController {

    private RatingRepo ratingRepo;

    public RatingController(RatingRepo ratingRepo) {
        this.ratingRepo = ratingRepo;
    }

    public void submitRating(Member member, int bookID, int numStars, String comment) throws RatingNotSubmittedException {
        if (numStars == 0) throw new RatingNotSubmittedException();
        Rating rating = new Rating(member.getId(), bookID, numStars, comment);
        ratingRepo.addRating(rating);
    }

    public boolean existsRating(Member member, BorrowedCopy borrowedCopy) {
        return ratingRepo.existsRating(member, borrowedCopy);
    }
}
