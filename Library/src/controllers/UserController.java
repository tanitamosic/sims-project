package controllers;

import exceptions.IncorrectPasswordException;
import exceptions.MismatchingConfirmedPasswordException;
import exceptions.WeakPasswordException;
import model.users.User;
import model.users.UserAccount;
import org.hibernate.HibernateError;
import repos.AccountRepo;
import repos.LibrarianRepo;
import repos.MemberRepo;


public class UserController {
    private AccountRepo accountRepo;
    private MemberRepo memberRepo;
    private LibrarianRepo librarianRepo;

    public UserController(AccountRepo accountRepo, MemberRepo memberRepo, LibrarianRepo librarianRepo) {
        this.accountRepo = accountRepo;
        this.memberRepo = memberRepo;
        this.librarianRepo = librarianRepo;
    }

    public boolean tryLogin(String un, String pass) {
        try {
            return accountRepo.checkCredentials(un, pass);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public User login(String un) {
        UserAccount acc = accountRepo.getUser(un);
        User user;
        switch (acc.getTypeofacc()) {
            case "Member":
                user = memberRepo.getByAccountId(acc.getAccountId());
                break;
            case "Librarian":
                user = librarianRepo.getByAccountId(acc.getAccountId());
                break;
            default:
                user = new User() {
                };
        }
        return user;
    }

    public void changePassword(String oldPassword, String newPassword, String newPasswordConfirmed, UserAccount userAccount)
            throws IncorrectPasswordException, MismatchingConfirmedPasswordException, WeakPasswordException {
        validate(oldPassword, newPassword, newPasswordConfirmed, userAccount);
        userAccount.setPassword(newPassword);

        accountRepo.updateAccount(userAccount);
    }

    private void validate(String oldPassword, String newPassword, String newPasswordConfirmed, UserAccount userAccount)
            throws IncorrectPasswordException, MismatchingConfirmedPasswordException, WeakPasswordException {
        try {
            if (!accountRepo.checkCredentials(userAccount.getUsername(), oldPassword))
                throw new IncorrectPasswordException();
        } catch (Exception ex) {
            throw new IncorrectPasswordException();
        }
        if (!newPassword.equals(newPasswordConfirmed))
            throw new MismatchingConfirmedPasswordException();
        if (!isStrongPassword(newPassword))
            throw new WeakPasswordException();
    }

    private boolean isStrongPassword(String password) {
        final int MIN_PASSWORD_LENGTH = 6;
        if (password.length() < MIN_PASSWORD_LENGTH) return false;
        if (!containsDigit(password)) return false;
        if (!containsUpperCase(password)) return false;
        return containsLowerCase(password);
    }

    private boolean containsDigit(String password) {
        for (int i = 0; i < password.length(); i++)
            if (Character.isDigit(password.charAt(i))) return true;
        return false;
    }

    private boolean containsUpperCase(String password) {
        for (int i = 0; i < password.length(); i++)
            if (Character.isUpperCase(password.charAt(i))) return true;
        return false;
    }

    private boolean containsLowerCase(String password) {
        for (int i = 0; i < password.length(); i++)
            if (Character.isLowerCase(password.charAt(i))) return true;
        return false;
    }
}
