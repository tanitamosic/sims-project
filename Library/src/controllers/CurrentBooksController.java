package controllers;

import exceptions.BorrowingAlreadyProlongedException;
import model.library.BorrowedCopy;
import model.users.Member;

import javax.swing.*;
import java.util.Date;

public class CurrentBooksController {

    private MemberController memberController;

    // additional column for prolongation button
    private String[] columnNames = {"Naslov", "Autori", "Datum pozajmljivanja", "Rok za vraćanje", ""};
    public static final int TITLE_INDEX = 0;
    public static final int AUTHORS_INDEX = 1;
    public static final int BORROWING_DATE_INDEX = 2;
    public static final int RETURN_DEADLINE_INDEX = 3;
    public static final int PROLONGATION_BUTTON_INDEX = 4;

    public CurrentBooksController(MemberController memberController) {
        this.memberController = memberController;
    }

    public int getRowCount(Member member) {
        return memberController.getCurrentlyBorrowedCopies(member).size();
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public BorrowedCopy getBorrowedCopy(int rowIndex, Member member) {
        return memberController.getCurrentlyBorrowedCopies(member).get(rowIndex);
    }

    public String getColumnName(int column) {
        return columnNames[column];
    }

    public boolean prolongBorrowedCopy(BorrowedCopy borrowedCopy, Member member){
        return memberController.prolongBorrowedCopy(borrowedCopy, member);
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case TITLE_INDEX:
                return String.class;
            case AUTHORS_INDEX:
                return String[].class;
            case BORROWING_DATE_INDEX:
            case RETURN_DEADLINE_INDEX:
                return Date.class;
            case PROLONGATION_BUTTON_INDEX:
                return JButton.class;
            default:
                return Object.class;
        }
    }
}
