package controllers;

import model.DTO.SearchCriteria;
import model.enums.Genre;
import model.library.Book;
import model.DTO.SearchCriteria;
import observer.Observer;
import observer.Publisher;
import observer.UpdateEvent;
import repos.BookRepo;
import repos.ListProperty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LibraryController implements Publisher {

    private BookRepo bookRepo;
    private List<Book> searchCandidates;
    private List<Observer> observers;
    private SearchCriteria criteria;

    public LibraryController(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
        this.searchCandidates = bookRepo.getBooks().get();
    }

    public List<Book> getSearchCandidates() {
        search();
        return searchCandidates;
    }

    public void setSearchCriteria(SearchCriteria criteria) {
        this.criteria = criteria;
    }

    public void search() {
        if (criteria == null || (criteria.getTitle().equals("") && criteria.getAuthor().equals("") && criteria.getTags().equals("") && criteria.getGenre().equals("-"))) {
            searchCandidates = bookRepo.getBooks().get();
            return;
        }
        searchCandidates = new ArrayList<>();
        List<Book> allBooks = bookRepo.getBooks().get();
        List<String> titles = new ArrayList<>() {{add(criteria.getTitle().toLowerCase());}};
        List<String> authors = new ArrayList<>() {{add(criteria.getAuthor().toLowerCase());}};
        List<String> tags = new ArrayList<>();
        String[] tagsArr = criteria.getTags().split(" ");
        for (String t : tagsArr)
            tags.add(t.toLowerCase());
        List<String> genres = new ArrayList<>() {{add(criteria.getGenre());}};

        if (criteria.getTitle().equals("")) titles = bookRepo.getAllTitles();
        if (criteria.getAuthor().equals("")) authors = bookRepo.getAllAuthorsNames();
        if (criteria.getTags().equals("")) tags = bookRepo.getAllTags();
        if (criteria.getGenre().equals("-")) genres = Arrays.asList(Genre.getNames());

        for (Book b : allBooks) {
            out:
            if (titles.contains(b.getTitle().toLowerCase()) && genres.contains(b.getGenre().toString())) {
                for (String tag : b.getTags()) {
                    if (tags.contains(tag.toLowerCase())) {
                        for (String author : b.getAuthorsNames()) {
                            if (authors.contains(author.toLowerCase())) {
                                searchCandidates.add(b);
                                break out;
                            }
                        }
                    }
                }
            }
        }
        notifyObservers();
    }

    public BookRepo getBookRepo() {
        return bookRepo;
    }

    public boolean canBeReserved(Book book) {
        return bookRepo.getNumOfFreeCopies(book) > 0;
    }

    @Override
    public void addObserver(Observer observer) {
        if (null == observers)
            observers = new ArrayList<>();
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        if (null == observers)
            return;
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : this.observers) {
            observer.updatePerformed(new UpdateEvent());
        }
    }
}
