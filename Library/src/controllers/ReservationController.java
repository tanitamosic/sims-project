package controllers;

import model.enums.ReservationStatus;
import model.library.Book;
import model.library.Reservation;
import model.users.Member;
import repos.ReservationRepo;

public class ReservationController {

    private ReservationRepo reservationRepo;


    public ReservationController(ReservationRepo reservationRepo) {
        this.reservationRepo = reservationRepo;
    }

    public void reserveBook(Book book, Member member) {
        Reservation reservation = new Reservation(book.getId(), member.getId(), ReservationStatus.PENDING);
        reservationRepo.addReservation(reservation);
    }
}
