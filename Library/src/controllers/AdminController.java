package controllers;

import application.Constants;
import exceptions.*;
import model.DTO.MemberSearchCriteria;
import model.enums.BookCopyState;
import model.enums.LibrarianRole;
import model.enums.MemberType;
import model.library.*;
import model.users.Librarian;
import model.users.Member;
import model.users.UserAccount;
import observer.Observer;
import observer.Publisher;
import observer.UpdateEvent;
import repos.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AdminController implements Publisher {

    private MemberRepo memberRepo;
    private LibrarianRepo librarianRepo;
    private PriceRepo priceRepo;
    private BorrowingRepo borrowingRepo;
    private MemberSearchCriteria memberSearchCriteria;
    private ArrayList<BookCopy> borrowCandidates;
    private List<Member> searchResult;
    private Constants constants;
    private BookRepo bookRepo;
    private RatingRepo ratingRepo;
    private AccountRepo accountRepo;
    private List<Observer> observers;

    public RatingRepo getRatingRepo() {
        return ratingRepo;
    }


    public void eraseBorrowCandidates() {
        this.borrowCandidates = new ArrayList<>();
    }

    public AdminController(MemberRepo mr, LibrarianRepo lr, PriceRepo pr, BorrowingRepo br, RatingRepo rr, Constants con,
                           BookRepo bookr, AccountRepo ar) {
        this.borrowingRepo = br;
        this.memberRepo = mr;
        this.librarianRepo = lr;
        this.priceRepo = pr;
        this.ratingRepo = rr;
        this.bookRepo = bookr;
        this.constants = con;
        this.accountRepo = ar;
        this.borrowCandidates = new ArrayList<>();
        this.searchResult = new ArrayList<>();
    }

    public List<Member> getSearchResult() {
        //searchMembers();
        return searchResult;
    }

    public BookRepo getBookRepo() {
        return bookRepo;
    }

    public Constants getConstants() {
        return constants;
    }

    public ArrayList<Book> getMostRead(){
        ArrayList<Book> mostRead = new ArrayList<Book>();
        ArrayList<Integer> mostReadIds = bookRepo.getMostRead();
        for (Integer id : mostReadIds){
            mostRead.add(bookRepo.getBook(id));
        }
        return mostRead;
    }

    public int getBorrowCount(Book b){
        return bookRepo.getBorrowCount(b);
    }


    public void addMember(String name, String surname, Date date, String jmbg, int userType, String username, String password, String password2)
            throws MismatchingConfirmedPasswordException, WeakPasswordException, FieldMissingException {
        validate(password, password2);
        checkInput(name, surname, jmbg, username);
        UserAccount ua = new UserAccount(username, password, true, "Member");
        accountRepo.saveAccount(ua);
        MemberType mt = MemberType.values()[userType];
        LocalDate currentDate = LocalDate.now();
        LocalDate endDate = currentDate.plusYears(1);
        Member m = new Member(jmbg, name, surname, date, ua.getAccountId(), toDate(currentDate), toDate(endDate), mt);
        memberRepo.saveMember(m);
    }

    private MemberType getUserTypeByIndex(int userType) {
        switch (userType) {
            case 0:
                return MemberType.HONORABLE;
            case 1:
                return MemberType.PRESCHOOLER;
            case 2:
                return MemberType.STUDENT;
            case 4:
                return MemberType.PENSIONER;
            default:
                return MemberType.REGULAR;
        }
    }

    private Date getExpirationDate() {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 365);
        return cal.getTime();

    }

    public void addLibrarian(String name, String surname, Date date, String jmbg, List<LibrarianRole> roles, String username, String password, String password2)
            throws MismatchingConfirmedPasswordException, WeakPasswordException, FieldMissingException {
        validate(password, password2);
        checkInput(name, surname, jmbg, username);
        UserAccount ua = new UserAccount(username, password, true, "Librarian");
        accountRepo.saveAccount(ua);
        Librarian l = new Librarian(jmbg, name, surname, date, ua.getAccountId());
        librarianRepo.saveLibrarian(l);
        l.setRoles(roles);
        librarianRepo.saveRoles(l);
    }

    private void checkInput(String name, String surname, String jmbg, String username) throws FieldMissingException {
        if (name.equals("") || surname.equals("") || jmbg.equals("") || username.equals(""))
            throw new FieldMissingException();
    }


    private void validate(String password1, String password2)
            throws MismatchingConfirmedPasswordException, WeakPasswordException {
        if (!password1.equals(password2))
            throw new MismatchingConfirmedPasswordException();
        if (!isStrongPassword(password1))
            throw new WeakPasswordException();
    }

    private boolean isStrongPassword(String password) {
        final int MIN_PASSWORD_LENGTH = 6;
        if (password.length() < MIN_PASSWORD_LENGTH) return false;
        if (!containsDigit(password)) return false;
        if (!containsUpperCase(password)) return false;
        return containsLowerCase(password);
    }

    private boolean containsDigit(String password) {
        for (int i = 0; i < password.length(); i++)
            if (Character.isDigit(password.charAt(i))) return true;
        return false;
    }

    private boolean containsUpperCase(String password) {
        for (int i = 0; i < password.length(); i++)
            if (Character.isUpperCase(password.charAt(i))) return true;
        return false;
    }

    private boolean containsLowerCase(String password) {
        for (int i = 0; i < password.length(); i++)
            if (Character.isLowerCase(password.charAt(i))) return true;
        return false;
    }

    private void checkField(String newBasePrice) throws FieldMissingException {
        if (newBasePrice.length() == 0) {
            throw new FieldMissingException();
        }

    }

    public void setMemberSearchCriteria(MemberSearchCriteria msc) {
        this.memberSearchCriteria = msc;
    }

    public void searchMembers() {
        List<Member> allMembers = memberRepo.getMembers().get();
        searchResult = new ArrayList<>();
        for (Member m : allMembers) {
            if (m.getName().equalsIgnoreCase(memberSearchCriteria.getName()) || memberSearchCriteria.getName().equals("")) {
                if (m.getSurname().toLowerCase().equals(memberSearchCriteria.getSurname().toLowerCase()) || memberSearchCriteria.getSurname().equals("")) {
                    if (String.valueOf(m.getId()).equals(memberSearchCriteria.getId()) || memberSearchCriteria.getId().equals("")) {
                        if (m.getMemberType().ordinal() == memberSearchCriteria.getType() || memberSearchCriteria.getType() == 5) {
                            searchResult.add(m);
                        }
                    }
                }
            }
        }
    }

    public MemberRepo getMemberRepo() {
        return memberRepo;
    }

    public void approveComment(Rating rating) {
        rating.setApproved(true);
        ratingRepo.updateRating(rating);
        notifyObservers();
    }

    public void rejectRating(Rating rating) {
        ratingRepo.deleteRating(rating);
        notifyObservers();
    }

    public void addBorrowCandidate(Book book) {
        for (BookCopy bc : book.getCopies()) {
            if (bc.isAvailable()) {
                borrowCandidates.add(bc);
                break;
            }
        }
        notifyObservers();
    }

    public void makeBorrowing(String id, String num) throws FieldMissingException, MaxNumOfBooksReachedException {
        validateBorrowing(id, num);
        Date date = new Date();
        Borrowing b = new Borrowing(Integer.parseInt(id), Integer.parseInt(num), date);
        borrowingRepo.addBorrowing(b);
        List<BorrowedCopy> bCopies = new ArrayList<>();
        for (BookCopy copy : borrowCandidates) {
            BorrowedCopy bc = new BorrowedCopy(b.getBorrowingId(), copy.getCopyId(), null, false, calculateReturnDeadline(id));
            borrowingRepo.addBorrowedCopy(bc);
            bCopies.add(bc);
            copy.setState(BookCopyState.BORROWED);
            bookRepo.updateCopy(copy);
        }
        b.setBorrowedCopies(bCopies);
        eraseBorrowCandidates();
    }

    private void validateBorrowing(String id, String num) throws FieldMissingException, MaxNumOfBooksReachedException {
        try {
            int id_ = Integer.parseInt(id);
            int num_ = Integer.parseInt(num);
        } catch (Exception ex) {
            throw new FieldMissingException();
        }
        if (maxNumOfBookReached(id, num))
            throw new MaxNumOfBooksReachedException(getBookLimit(id));
    }

    private boolean maxNumOfBookReached(String id, String num) {
        return borrowCandidates.size() > getBookLimit(id);
    }

    private int getBookLimit(String id) {
        int id_ = Integer.parseInt(id);
        Member m = memberRepo.getByMemberID(id_);
        return constants.getBookLimit(m.getMemberType());
    }

    private Date calculateReturnDeadline(String id) {
        Member m = memberRepo.getByMemberID(Integer.parseInt(id));
        int days = getDaysLimitByType(m.getMemberType());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    private int getDaysLimitByType(MemberType memberType) {
        switch (memberType) {
            case HONORABLE:
                return constants.getDayLimitHonorable();
            case PRESCHOOLER:
            case STUDENT:
                return constants.getDayLimitStudent();
            case REGULAR:
                return constants.getDayLimitRegular();
            case PENSIONER:
                return constants.getDayLimitPension();
            default:
                return 0;
        }
    }

    public List<BookCopy> getBorrowCandidates() {
        return borrowCandidates;
    }

    public String getLastBookTitle() {
        return getBorrowCandidates().get(getBorrowCandidates().size() - 1).getBook().getTitle();
    }

    @Override
    public void addObserver(Observer observer) {
        if (null == observers)
            observers = new ArrayList<>();
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        if (null == observers)
            return;
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : this.observers) {
            observer.updatePerformed(new UpdateEvent());
        }
    }

    public List<String> getBorrowedTitles() {
        List<String> titles = new ArrayList<>();
        for (BookCopy copy : getBorrowCandidates())
            titles.add(copy.getBook().getTitle());
        return titles;
    }

    private Date toDate(LocalDate localDate) {
        return java.util.Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

}
