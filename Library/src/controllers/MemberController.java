package controllers;

import application.Constants;
import exceptions.BorrowingAlreadyProlongedException;
import exceptions.RatingNotSubmittedException;
import model.DTO.DateInterval;
import model.enums.MemberType;
import model.library.Book;
import model.library.BorrowedCopy;
import model.users.Member;
import repos.MemberRepo;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

public class MemberController {

    private ReservationController reservationController;
    private BorrowingController borrowingController;
    private RatingController ratingController;
    private MemberRepo memberRepo;
    private Constants constants;

    private static final String DATE_FORMAT = "dd/MM/yyyy";

    public MemberController(ReservationController reservationController, BorrowingController borrowingController,
                            RatingController ratingController, MemberRepo memberRepo, Constants constants) {
        this.reservationController = reservationController;
        this.borrowingController = borrowingController;
        this.ratingController = ratingController;
        this.memberRepo = memberRepo;
        this.constants = constants;
    }

    public void payMembership(Member member) {
        if (member.getExpirationDate().before(toDate(LocalDate.now()))) {
            LocalDate newExpirationDate = LocalDate.now().plusYears(1);
            member.setExpirationDate(toDate(newExpirationDate));
        } else {
            int year = member.getExpirationDate().getYear() + 1;
            member.getExpirationDate().setYear(year);
        }
        memberRepo.updateMember(member);
    }

    public String DateToString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        return sdf.format(date);
    }

    public void reserveBook(Book book, Member member) {
        reservationController.reserveBook(book, member);
    }

    public List<BorrowedCopy> getBorrowedCopies(Member member) {
        return borrowingController.getBorrowedCopies(member);
    }

    public List<BorrowedCopy> getBorrowedCopies(Member member, DateInterval interval) {
        return borrowingController.getBorrowedCopies(member, interval);
    }

    public List<BorrowedCopy> getCurrentlyBorrowedCopies(Member member) {
        return borrowingController.getCurrentlyBorrowedCopies(member);
    }

    public boolean prolongBorrowedCopy(BorrowedCopy borrowedCopy, Member member){
        return borrowingController.prolongBorrowedCopy(borrowedCopy, member);
    }

    public void submitRating(Member member, int bookID, int numStars, String comment) throws RatingNotSubmittedException {
        ratingController.submitRating(member, bookID, numStars, comment);
    }

    public boolean existsRating(Member member, BorrowedCopy borrowedCopy) {
        return ratingController.existsRating(member, borrowedCopy);
    }

    private Date toDate(LocalDate localDate) {
        return java.util.Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    public double getMembershipCost(MemberType memberType) {
        return constants.getMembershipCostByType(memberType);
    }

    private int getNumOfProlongationDays(Member member) {
        return constants.getNumOfProlongationDaysByType(member.getMemberType());
    }
}
