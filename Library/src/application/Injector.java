package application;

import services.LibrarianServices;
import services.MemberServices;
import view.LoginFrame;
import view.UserGUI;
import view.librarian.LibrarianGUI;
import view.member.MemberMenu;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import repos.*;
import controllers.*;

public class Injector {

    public AccountRepo accountRepo;
    public BookRepo bookRepo;
    public BrancheRepo brancheRepo;
    public LibrarianRepo librarianRepo;
    public MemberRepo memberRepo;
    public PaymentRepo allPaymentRepo;
    public PriceRepo priceRepo;
    public ReservationRepo reservationRepo;
    public BorrowingRepo borrowingRepo;
    public RatingRepo ratingRepo;

    public Constants constants;

    public AdminController adminController;
    public CataloguerController cataloguerController;
    public FrontDeskController frontDeskController;
    public MemberController memberController;
    public SystemController systemController;
    public UserController userController;
    public ReservationController reservationController;
    public BorrowingController borrowingController;
    public RatingController ratingController;

    public LibrarianServices librarianServices;
    public MemberServices memberServices;

    public LibrarianGUI libGUI;
    public LoginFrame login;
    public MemberMenu memberMenu;
    public UserGUI userGUI;

    private Session session;
    private SessionFactory sf;
    private LibraryController libraryController;
    private SessionFactory sessionFactory;

    Injector() {
        initSession();
        initCollections();
        initControllers();
        initServices();
        initGUI();
        startGUI();
    }

    private void initSession() {
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml").buildSessionFactory();

        session = factory.getCurrentSession();

    }

    private void startGUI() {
        login.setVisible(true);
    }

    private void initGUI() {
        libGUI = new LibrarianGUI(librarianServices);
        memberMenu = new MemberMenu(memberServices);
        login = new LoginFrame(librarianServices,memberServices, userController);
    }

    private void initControllers() {
        adminController = new AdminController(memberRepo, librarianRepo, priceRepo, borrowingRepo, ratingRepo, constants, bookRepo, accountRepo);
        cataloguerController = new CataloguerController();
        ratingController = new RatingController(ratingRepo);
        reservationController = new ReservationController(reservationRepo);
        borrowingController = new BorrowingController(borrowingRepo, constants);
        memberController = new MemberController(reservationController, borrowingController, ratingController, memberRepo, constants);
        frontDeskController = new FrontDeskController(reservationRepo, bookRepo);
        systemController = new SystemController();
        userController = new UserController(accountRepo, memberRepo, librarianRepo);
        libraryController = new LibraryController(bookRepo);
    }

    private void initServices() {
        librarianServices = new LibrarianServices(userController, adminController, cataloguerController, frontDeskController, libraryController);
        memberServices = new MemberServices(userController, memberController, libraryController);
    }

    private void initCollections() {
        accountRepo = new AccountRepo(session);
        bookRepo = new BookRepo(session);
        brancheRepo = new BrancheRepo();
        librarianRepo = new LibrarianRepo(session);
        memberRepo = new MemberRepo(session);
        allPaymentRepo = new PaymentRepo();
        priceRepo = new PriceRepo(session);
        reservationRepo = new ReservationRepo(session);
        borrowingRepo = new BorrowingRepo(session);
        ratingRepo = new RatingRepo(session);
        reservationRepo = new ReservationRepo(session);
        constants = new Constants("constants.txt");
    }
}
