package application;

import model.enums.MemberType;
import model.users.Member;

import java.io.*;

public class Constants {

    private int basePrice;
    private int discountStudent;
    private int discountPension;
    private int dayLimitRegular;
    private int dayLimitStudent;
    private int dayLimitPension;
    private int dayLimitHonorable;
    private int bookLimitStudent;
    private int bookLimitPension;
    private int bookLimitRegular;
    private int bookLimitHonorable;

    public Constants(String path){
        loadConstants(path);
    }

    public Constants(){
        loadConstants("constants.txt");
    }

    public void saveConstants(){
        try {
            FileWriter writer = new FileWriter("constants.txt");
            String content = "";
            content += "basePrice " + basePrice + "\n";
            content += "discountStudent " + discountStudent + "\n";
            content += "discountPension " + discountPension + "\n";
            content += "dayLimitRegular " + dayLimitRegular + "\n";
            content += "dayLimitStudent " + dayLimitStudent + "\n";
            content += "dayLimitPension " + dayLimitPension + "\n";
            content += "dayLimitHonorable " + dayLimitHonorable + "\n";
            content += "bookLimitRegular " + bookLimitRegular + "\n";
            content += "bookLimitStudent " + bookLimitStudent + "\n";
            content += "bookLimitPension " + bookLimitPension + "\n";
            content += "bookLimitHonorable " + bookLimitHonorable;
            writer.write(content);
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    private void loadConstants(String path) {
        try {
            FileInputStream fstream = new FileInputStream(path);
            DataInputStream datastream = new DataInputStream(fstream);
            BufferedReader buff = new BufferedReader(new InputStreamReader(datastream));
            String line;
            String[] pieces;

            while((line=buff.readLine())!= null){
                pieces = line.split(" ");
                switch(pieces[0]){
                    case "basePrice":
                        basePrice = Integer.parseInt(pieces[1]);
                        break;
                    case "discountStudent":
                        discountStudent =  Integer.parseInt(pieces[1]);
                        break;
                    case "discountPension":
                        discountPension =  Integer.parseInt(pieces[1]);
                        break;
                    case "dayLimitRegular":
                        dayLimitRegular =  Integer.parseInt(pieces[1]);
                        break;
                    case "dayLimitStudent":
                        dayLimitStudent =  Integer.parseInt(pieces[1]);
                        break;
                    case "dayLimitPension":
                        dayLimitPension =  Integer.parseInt(pieces[1]);
                        break;
                    case "dayLimitHonorable":
                        dayLimitHonorable =  Integer.parseInt(pieces[1]);
                        break;
                    case "bookLimitRegular":
                        bookLimitRegular =  Integer.parseInt(pieces[1]);
                        break;
                    case "bookLimitStudent":
                        bookLimitStudent =  Integer.parseInt(pieces[1]);
                        break;
                    case "bookLimitPension":
                        bookLimitPension =  Integer.parseInt(pieces[1]);
                        break;
                    case "bookLimitHonorable":
                        bookLimitHonorable =  Integer.parseInt(pieces[1]);
                        break;
                }
            }

            buff.close();
            datastream.close();
            fstream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public int getDayLimitRegular() {
        return dayLimitRegular;
    }

    public void setDayLimitRegular(int dayLimitRegular) {
        this.dayLimitRegular = dayLimitRegular;
    }

    public int getDayLimitStudent() {
        return dayLimitStudent;
    }

    public void setDayLimitStudent(int dayLimitStudent) {
        this.dayLimitStudent = dayLimitStudent;
    }

    public int getDayLimitPension() {
        return dayLimitPension;
    }

    public void setDayLimitPension(int dayLimitPension) {
        this.dayLimitPension = dayLimitPension;
    }

    public int getDayLimitHonorable() {
        return dayLimitHonorable;
    }

    public void setDayLimitHonorable(int dayLimitHonorable) {
        this.dayLimitHonorable = dayLimitHonorable;
    }

    public int getBookLimitStudent() {
        return bookLimitStudent;
    }

    public void setBookLimitStudent(int bookLimitStudent) {
        this.bookLimitStudent = bookLimitStudent;
    }

    public int getBookLimitPension() {
        return bookLimitPension;
    }

    public void setBookLimitPension(int bookLimitPension) {
        this.bookLimitPension = bookLimitPension;
    }

    public int getBookLimitRegular() {
        return bookLimitRegular;
    }

    public void setBookLimitRegular(int bookLimitRegular) {
        this.bookLimitRegular = bookLimitRegular;
    }

    public int getBookLimitHonorable() {
        return bookLimitHonorable;
    }

    public void setBookLimitHonorable(int bookLimitHonorable) {
        this.bookLimitHonorable = bookLimitHonorable;
    }

    public int getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(int basePrice) {
        this.basePrice = basePrice;
    }

    public int getDiscountStudent() {
        return discountStudent;
    }

    public void setDiscountStudent(int discountStudent) {
        this.discountStudent = discountStudent;
    }

    public int getDiscountPension() {
        return discountPension;
    }

    public void setDiscountPension(int discountPension) {
        this.discountPension = discountPension;
    }

    public double getMembershipCostByType(MemberType memberType) {
        switch (memberType) {
            case PENSIONER:
                return basePrice * discountPension/100;
            case STUDENT:
            case PRESCHOOLER:
                return basePrice * discountStudent/100;
            default:
                return basePrice;
        }
    }

    public int getNumOfProlongationDaysByType(MemberType memberType) {
        switch (memberType) {
            case HONORABLE:
                return dayLimitHonorable;
            case STUDENT:
            case PRESCHOOLER:
                return dayLimitStudent;
            case PENSIONER:
                return dayLimitPension;
            default:
                return dayLimitRegular;
        }
    }


    public int getBookLimit(MemberType memberType) {
        switch (memberType)
        {
            case HONORABLE:
                return getBookLimitHonorable();
            case PRESCHOOLER:
            case STUDENT:
                return getBookLimitStudent();
            case PENSIONER:
                return getBookLimitPension();
            default:
                return getBookLimitRegular();
        }
    }
}
