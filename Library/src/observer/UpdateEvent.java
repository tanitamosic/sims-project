package observer;

public class UpdateEvent {

    protected String action;

    public UpdateEvent() {}

    public UpdateEvent(String action) {
        this.action = action;
    }
}
