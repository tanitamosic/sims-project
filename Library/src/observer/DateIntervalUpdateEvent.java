package observer;

import model.DTO.DateInterval;

public class DateIntervalUpdateEvent extends UpdateEvent {

    private DateInterval dateInterval;

    public DateIntervalUpdateEvent(DateInterval dateInterval) {
        this.dateInterval = dateInterval;
    }

    public DateInterval getDateInterval() {
        return dateInterval;
    }
}
