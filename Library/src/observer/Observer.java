package observer;

public interface Observer {

    public void updatePerformed(UpdateEvent event);
    
}
