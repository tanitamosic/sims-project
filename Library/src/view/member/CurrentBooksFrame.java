package view.member;

import view.components.JTableButtonMouseListener;
import view.tables.CurrentBooksTable;
import model.users.Member;
import controllers.MemberController;

import javax.swing.*;
import java.awt.*;

public class CurrentBooksFrame extends JFrame {

    private MemberController memberController;
    private Member member;

    public CurrentBooksFrame(MemberController memberController, Member member) {
        this.memberController = memberController;
        this.member = member;

        initFrame();
        initPanes();
    }

    private void initFrame() {
        setTitle("Knjige na čitanju");
        setExtendedState(MAXIMIZED_BOTH);
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(new Dimension(size.width / 2, size.height / 2));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
    }

    private void initPanes() {
        JTable currentBooksTable = new CurrentBooksTable(memberController, member);
        JScrollPane scrollPane = new JScrollPane(currentBooksTable);
        add(scrollPane, BorderLayout.CENTER);
        currentBooksTable.addMouseListener(new JTableButtonMouseListener(currentBooksTable));
    }
}
