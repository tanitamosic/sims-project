package view.member;

import services.MemberServices;
import view.ChangePasswordDialog;
import view.LoginFrame;
import view.UserGUI;
import view.components.*;
import model.users.Member;
import net.miginfocom.swing.MigLayout;
import controllers.LibraryController;
import controllers.MemberController;
import controllers.UserController;
import view.components.colors.MenuBackgroundColor;

import javax.swing.*;
import java.awt.*;

public class MemberMenu extends UserGUI {

    private JPanel centerPane;
    private JPanel northPane;
    private JButton btnSearch;
    private JButton btnPayMembership;
    private JButton btnBorrowingHistory;
    private JButton btnCurrentlyBorrowed;
    private JButton btnChangePassword;
    private JButton btnLogOut;
    private JLabel lblName;
    private JLabel lblUserID;
    private UserController userController;
    private MemberController memberController;
    private LibraryController libraryController;
    private Member member;
    private LoginFrame lg;

    public MemberMenu(MemberServices ms) {
        this.userController = ms.userController;
        this.memberController = ms.memberController;
        this.libraryController = ms.libraryController;
    }

    public void open(Member member, LoginFrame loginFrame) {
        this.member = member;
        this.lg = loginFrame;
        initPanes();
        initComponents();
        addComponents();
        addButtonListeners();
        setVisible(true);
    }

    private void initPanes() {
        centerPane = new JPanel(new MigLayout("", "25%[50%]", "17%[7%]5%[7%]5%[7%]5%[7%]5%[7%]"));
        add(centerPane, BorderLayout.CENTER);
        centerPane.setBackground(new MenuBackgroundColor());
        northPane = new JPanel(new MigLayout("", "1%[grow][]1%", "15%[][]"));
        add(northPane, BorderLayout.NORTH);
        northPane.setBackground(new MenuBackgroundColor());
    }

    private void initComponents() {
        initLabels();
        initButtons();
    }

    private void initLabels() {
        lblName = new BoldLabel(member.getName() + " " + member.getSurname());
        lblUserID = new BoldLabel("Broj članske karte: " + member.getId());
    }

    private void initButtons() {
        btnSearch = new MenuButton("Pretraži knjige");
        btnPayMembership = new MenuButton("Plati članarinu");
        btnBorrowingHistory = new MenuButton("Istorija pozajmljivanja");
        btnCurrentlyBorrowed = new MenuButton("Knjige na čitanju");
        btnChangePassword = new MenuButton("Promeni lozinku");
        btnLogOut = new ImageButton(LOGOUT_ICON_PATH, 35, 35);
        btnLogOut.setBorder(new RoundedBorder(5));
        btnLogOut.setToolTipText("Odjava");
    }

    private void addComponents() {
        centerPane.add(btnSearch, "cell 0 0, grow");
        centerPane.add(btnPayMembership, "cell 0 1, grow");
        centerPane.add(btnBorrowingHistory, "cell 0 2, grow");
        centerPane.add(btnCurrentlyBorrowed, "cell 0 3, grow");
        centerPane.add(btnChangePassword, "cell 0 4, grow");
        northPane.add(lblName, "cell 0 0");
        northPane.add(btnLogOut, "cell 1 0, alignx right");
        northPane.add(lblUserID, "cell 0 1");
    }

    private void addButtonListeners() {
        btnSearch.addActionListener(e -> new MemberBookSearchFrame(memberController, libraryController, member).setVisible(true));
        btnPayMembership.addActionListener(e -> new PayMembershipDialog(memberController, member).setVisible(true));
        btnBorrowingHistory.addActionListener(e -> new BorrowingHistoryFrame(memberController, member).setVisible(true));
        btnChangePassword.addActionListener(e -> new ChangePasswordDialog(userController, member.getUser_acc()).setVisible(true));
        btnCurrentlyBorrowed.addActionListener(e -> new CurrentBooksFrame(memberController, member).setVisible(true));
        btnLogOut.addActionListener(e -> logout());
    }

    private void logout() {
        lg.open();
        removeAll();
        dispose();
    }
}
