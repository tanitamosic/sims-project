package view.member;

import view.DateIntervalPane;
import view.components.JTableButtonMouseListener;
import view.components.colors.HeaderBackgroundColor;
import view.tables.BorrowingHistoryTable;
import model.users.Member;
import observer.DateIntervalUpdateEvent;
import observer.Observer;
import observer.UpdateEvent;
import controllers.MemberController;

import javax.swing.*;
import java.awt.*;

public class BorrowingHistoryFrame extends JFrame implements Observer {

    private MemberController memberController;
    private Member member;
    private BorrowingHistoryTable borrowingHistoryTable;

    public BorrowingHistoryFrame(MemberController memberController, Member member) {
        this.memberController = memberController;
        this.member = member;

        initFrame();
        initPanes();
    }

    private void initFrame() {
        setTitle("Istorija pozajmljivanja");
        setExtendedState(MAXIMIZED_BOTH);
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(new Dimension(size.width / 2, size.height / 2));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
    }

    private void initPanes() {
        borrowingHistoryTable = new BorrowingHistoryTable(memberController, member);
        borrowingHistoryTable.addMouseListener(new JTableButtonMouseListener(borrowingHistoryTable));
        JScrollPane scrollPane = new JScrollPane(borrowingHistoryTable);
        add(scrollPane, BorderLayout.CENTER);
        DateIntervalPane northPane = new DateIntervalPane();
        add(northPane, BorderLayout.NORTH);
        northPane.addObserver(this);
        northPane.setBackground(new HeaderBackgroundColor());
    }

    @Override
    public void updatePerformed(UpdateEvent event) {
        DateIntervalUpdateEvent updateEvent = (DateIntervalUpdateEvent) event;
        borrowingHistoryTable.updateData(updateEvent.getDateInterval());
    }
}
