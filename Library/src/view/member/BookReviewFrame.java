package view.member;

import controllers.MemberController;
import exceptions.RatingNotSubmittedException;
import model.users.Member;
import net.miginfocom.swing.MigLayout;
import view.components.BoldLabel;
import view.components.RegularButton;
import view.components.RegularLabel;
import view.components.StarRater;

import javax.swing.*;
import java.awt.*;

public class BookReviewFrame extends JFrame {

    private JPanel northPane;
    private JPanel centerPane;
    private JPanel southPane;
    private JScrollPane scrollPane;
    private JLabel lblTitle;
    private JLabel lblAuthor;
    private JLabel lblScore;
    private JLabel lblComment;
    private StarRater starRater;
    private JTextArea taComment;
    private JButton btnConfirm;
    private MemberController ratingController;
    private Member member;
    private int bookID;

    public BookReviewFrame(MemberController memberController, Member member, int bookID) {
        this.ratingController = memberController;
        this.member = member;
        this.bookID = bookID;

        initFrame();
        initPanes();
        initComponents();
        addComponents();
        addListeners();
    }

    void initFrame() {
        setTitle("Ocena knjige");
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(size.width / 2, size.height / 2);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setLayout(new BorderLayout());
    }

    void initPanes() {
        northPane = new JPanel(new MigLayout("", "[grow]", "20%[][]"));
        add(northPane, BorderLayout.NORTH);
        northPane.setBackground(new Color(250, 240, 255));
        centerPane = new JPanel(new MigLayout("", "[grow]", "12%[]5%[]10%[]5%[]"));
        add(centerPane, BorderLayout.CENTER);
        centerPane.setBackground(new Color(250, 240, 255));
        southPane = new JPanel(new MigLayout("", "[grow]2sp", "[]3sp"));
        add(southPane, BorderLayout.SOUTH);
        southPane.setBackground(new Color(250, 240, 255));
    }

    void initComponents() {
        lblTitle = new BoldLabel("Na Drini ćuprija");
        lblAuthor = new BoldLabel("Ivo Andrić");
        lblScore = new RegularLabel("Ocena");
        lblComment = new RegularLabel("Komentar");
        taComment = new JTextArea(5, 30);
        scrollPane = new JScrollPane(taComment);
        btnConfirm = new RegularButton("Potvrdi");
    }

    void addComponents() {
        northPane.add(lblTitle, "cell 0 0, alignx center");
        northPane.add(lblAuthor, "cell 0 1, alignx center");
        centerPane.add(lblScore, "cell 0 0, alignx center");
        starRater = new StarRater();
        centerPane.add(starRater, "cell 0 1, alignx center");
        centerPane.add(lblComment, "cell 0 2, alignx center");
        centerPane.add(scrollPane, "cell 0 3, alignx center");
        southPane.add(btnConfirm, "cell 0 0, alignx right");
    }

    private void addListeners() {
        btnConfirm.addActionListener(e -> {
            int numStars = starRater.getSelection();
            String comment = taComment.getText();
            try {
                ratingController.submitRating(member, bookID, numStars, comment);
                JOptionPane.showMessageDialog(null, "Vaša ocena je zabeležena");
                dispose();
            } catch (RatingNotSubmittedException ex) {
                JOptionPane.showMessageDialog(null, "Niste ocenili knjigu", "Greška", JOptionPane.ERROR_MESSAGE);
            }
        });
    }
}
