package view.member;

import controllers.MemberController;
import model.users.Member;
import net.miginfocom.swing.MigLayout;
import view.components.BoldLabel;
import view.components.RegularButton;
import view.components.colors.DefaultBackgroundColor;

import javax.swing.*;
import java.awt.*;

public class PayMembershipDialog extends JDialog {

    private JLabel lblDeadline;
    private JLabel lblAmount;
    private JButton btnConfirm;
    private MemberController memberController;
    private Member member;
    private JPanel centerPane;

    private String deadline;
    private String membershipCost;

    public PayMembershipDialog(MemberController memberController, Member member) {
        this.memberController = memberController;
        this.member = member;
        this.deadline = memberController.DateToString(member.getExpirationDate());
        this.membershipCost = String.valueOf(memberController.getMembershipCost(member.getMemberType()));

        initDialogue();
        initComponents();
        addComponents();
        addListeners();
    }

    private void initDialogue() {
        setTitle("Plaćanje članarine");
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(size.width / 2, size.height / 2);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        centerPane = new JPanel(new MigLayout("", "20%[50%]", "20%[]12%[]20%[12%]"));
        centerPane.setBackground(new DefaultBackgroundColor());
        setLayout(new BorderLayout());
        add(centerPane, BorderLayout.CENTER);
    }

    private void initComponents() {
        lblDeadline = new BoldLabel("Aktuelna članarina važi do " + deadline);
        lblAmount = new BoldLabel("Iznos članarine: " + membershipCost + " RSD");
        btnConfirm = new RegularButton("Potvrdi uplatu");
    }

    private void addComponents() {
        centerPane.add(lblDeadline, "cell 0 0");
        centerPane.add(lblAmount, "cell 0 1");
        centerPane.add(btnConfirm, "cell 0 2, alignx center, growy");
    }

    private void addListeners() {
        btnConfirm.addActionListener(e -> {
            memberController.payMembership(member);
            JOptionPane.showMessageDialog(null, "Članarina je uspešno uplaćena");
            dispose();
        });
    }
}
