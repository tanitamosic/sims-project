package view.member;

import model.DTO.SearchCriteria;
import model.users.Member;
import controllers.LibraryController;
import controllers.MemberController;
import view.SearchCriteriaPane;
import view.components.JTableButtonMouseListener;
import view.components.colors.HeaderBackgroundColor;
import view.tables.MemberBookSearchTable;

import javax.swing.*;
import java.awt.*;

public class MemberBookSearchFrame extends JFrame {

    private MemberController memberController;
    private LibraryController libraryController;
    private Member member;

    public MemberBookSearchFrame(MemberController memberController, LibraryController libraryController, Member member) {
        this.memberController = memberController;
        this.libraryController = libraryController;
        this.member = member;
        this.libraryController.setSearchCriteria(new SearchCriteria());

        initFrame();
        initPanes();
    }

    private void initFrame() {
        setTitle("Pretraga knjiga");
        setExtendedState(MAXIMIZED_BOTH);
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(new Dimension(size.width / 2, size.height / 2));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
    }

    private void initPanes() {
        JPanel northPane = new SearchCriteriaPane(libraryController);
        add(northPane, BorderLayout.NORTH);
        northPane.setBackground(new HeaderBackgroundColor());
        JTable bookSearchTable = new MemberBookSearchTable(memberController, libraryController, member);
        JScrollPane scrollPane = new JScrollPane(bookSearchTable);
        add(scrollPane, BorderLayout.CENTER);
        bookSearchTable.addMouseListener(new JTableButtonMouseListener(bookSearchTable));
    }
}
