package view.librarian;

import com.toedter.calendar.JDateChooser;
import exceptions.FieldMissingException;
import exceptions.MismatchingConfirmedPasswordException;
import exceptions.WeakPasswordException;
import model.enums.LibrarianRole;
import net.miginfocom.swing.MigLayout;
import controllers.AdminController;
import view.components.BoldLabel;
import view.components.RegularButton;
import view.components.RegularLabel;
import view.components.colors.DefaultBackgroundColor;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddNewLibrarianFrame extends JFrame {

    private AdminController adminController;

    private JPanel contentPane;
    private JTextField textFieldName;
    private JTextField textFieldSurname;
    private JTextField textFieldJMBG;
    private JPanel northPane;
    private JPanel southPane;
    private JPanel centralPane;
    private JLabel lblTitle;
    private JButton btnOK;
    private JLabel lblName;
    private JLabel lblSurname;
    private JLabel lblDate;
    private JLabel lblJMBG;
    private JLabel lblType;
    private JLabel lblUsername;
    private JLabel lblPassword;
    private JTextField textFieldUsername;
    private JDateChooser birthDate;
    private JPasswordField textFieldPassword2;
    private JLabel lblPassword2;
    private JTextField textFieldPassword;
    private JCheckBox chckbxAdmin;
    private JCheckBox chckbxCataloque;
    private JCheckBox chckbxFrontDesk;


    public AddNewLibrarianFrame(AdminController as) {
        this.adminController = as;
        initFrame();
        initPanes();
        initComponents();
        addComponents();
        addButtonListeners();
    }

    private void initFrame() {
        setTitle("Registracija novog bibliotekara");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(size.width / 2, size.height / 3 * 2);
        setLocationRelativeTo(null);
        setResizable(false);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
    }

    private void initPanes() {
        northPane = new JPanel();
        contentPane.add(northPane, BorderLayout.NORTH);
        northPane.setLayout(new MigLayout("", "1%[grow]1%", "15%[]7%[]"));
        northPane.setBackground(new DefaultBackgroundColor());

        southPane = new JPanel();
        contentPane.add(southPane, BorderLayout.SOUTH);
        southPane.setLayout(new MigLayout("", "[30%]3%[30%]5%[]1sp", "[]15%[10%][10%]2sp"));
        southPane.setBackground(new DefaultBackgroundColor());

        centralPane = new JPanel();
        contentPane.add(centralPane, BorderLayout.CENTER);
        centralPane.setLayout(new MigLayout("", "5%[30%]2%[30%]2%[]2%[][]", "10%[8%]3%[8%]3%[8%]3%[8%]3%[8%]3%[8%]3%[]3%[]3%[]3%[]3%[]3%[]3%[]3%[][][]"));
        centralPane.setBackground(new DefaultBackgroundColor());
    }

    private void initComponents() {
        lblTitle = new BoldLabel("Unesite podatke o novom korisniku");
        btnOK = new RegularButton("Završi sa unosom");
    }

    private void addComponents() {
        northPane.add(lblTitle, "cell 0 1,alignx center,aligny top");
        contentPane.add(southPane, BorderLayout.SOUTH);
        southPane.add(btnOK, "cell 13 1,alignx right,aligny top");
        contentPane.add(centralPane, BorderLayout.CENTER);
        lblName = new RegularLabel("Ime");
        centralPane.add(lblName, "cell 0 0,alignx trailing");
        textFieldName = new JTextField();
        textFieldName.setColumns(10);
        centralPane.add(textFieldName, "cell 1 0,grow");
        lblSurname = new RegularLabel("Prezime");
        centralPane.add(lblSurname, "cell 0 1,alignx trailing,aligny center");
        textFieldSurname = new JTextField();
        textFieldSurname.setColumns(10);
        centralPane.add(textFieldSurname, "cell 1 1,grow");
        lblDate = new RegularLabel("Datum rodenja");
        centralPane.add(lblDate, "cell 0 2,alignx trailing,aligny center");
        birthDate = new JDateChooser();
        centralPane.add(birthDate, "cell 1 2,grow");
        lblJMBG = new RegularLabel("JMBG");
        centralPane.add(lblJMBG, "cell 0 3,alignx trailing");
        textFieldJMBG = new JTextField();
        textFieldJMBG.setColumns(10);
        centralPane.add(textFieldJMBG, "cell 1 3,grow");
        lblType = new RegularLabel("Ovlašćenja");
        centralPane.add(lblType, "cell 0 4,alignx trailing");

        chckbxAdmin = new JCheckBox("Administrator");
        centralPane.add(chckbxAdmin, "cell 1 4");
        chckbxAdmin.setBackground(new DefaultBackgroundColor());

        chckbxCataloque = new JCheckBox("Katalogizator");
        centralPane.add(chckbxCataloque, "cell 1 5");
        chckbxCataloque.setBackground(new DefaultBackgroundColor());

        chckbxFrontDesk = new JCheckBox("Pozajmo odjeljenje");
        centralPane.add(chckbxFrontDesk, "cell 1 6");
        lblUsername = new RegularLabel("Korisničko ime");
        chckbxFrontDesk.setBackground(new DefaultBackgroundColor());

        centralPane.add(lblUsername, "cell 0 7,alignx trailing");
        textFieldUsername = new JTextField();
        centralPane.add(textFieldUsername, "cell 1 7,growx");
        textFieldUsername.setColumns(10);
        lblPassword = new RegularLabel("Lozinka");
        centralPane.add(lblPassword, "cell 0 8,alignx trailing,aligny center");

        textFieldPassword = new JPasswordField();
        centralPane.add(textFieldPassword, "cell 1 8,growx");
        textFieldPassword.setColumns(10);
        textFieldPassword2 = new JPasswordField();
        centralPane.add(textFieldPassword2, "cell 1 9,growx");
        textFieldPassword2.setColumns(10);

        lblPassword2 = new RegularLabel("Potvrda lozinke");
        centralPane.add(lblPassword2, "cell 0 9,alignx trailing");
    }

    private void addButtonListeners() {
        btnOK.addActionListener(e -> {
            String name = String.valueOf(textFieldName.getText());
            String surname = String.valueOf(textFieldSurname.getText());
            String JMBG = String.valueOf(textFieldJMBG.getText());
            String username = String.valueOf(textFieldUsername.getText());
            List<LibrarianRole> roles = getRoles(chckbxAdmin, chckbxCataloque, chckbxFrontDesk);
            String password = String.valueOf(textFieldPassword.getText());
            String password2 = String.valueOf(textFieldPassword2.getText());
            Date date = birthDate.getDate();

            try {
                adminController.addLibrarian(name, surname, date, JMBG, roles, username, password, password2);
                this.setVisible(false);
                new AddedLibrarianFrameMessage().setVisible(true);
            } catch (FieldMissingException e1) {
                JOptionPane.showMessageDialog(null, "Nisu unesena sva polja", "Greška", JOptionPane.ERROR_MESSAGE);

            } catch (WeakPasswordException e2) {
                emptyPasswordFields();
                JOptionPane.showMessageDialog(null, "Unesena lozinka nije dovoljno snažna", "Greška", JOptionPane.ERROR_MESSAGE);
            } catch (MismatchingConfirmedPasswordException e3) {
                emptyPasswordFields();
                JOptionPane.showMessageDialog(null, "Lozinka nije ispravno potvrđena", "Greška", JOptionPane.ERROR_MESSAGE);
            } catch (Exception e4) {
                emptyPasswordFields();
                JOptionPane.showMessageDialog(null, "Dogodila se greška", "Greška", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    private void emptyPasswordFields() {
        textFieldPassword.setText("");
        textFieldPassword2.setText("");
    }

    private List<LibrarianRole> getRoles(JCheckBox chckbxAdmin, JCheckBox chckbxCataloque, JCheckBox chckbxFrontDesk) {
        List<LibrarianRole> retVal = new ArrayList<>();
        if (chckbxAdmin.isSelected()) retVal.add(LibrarianRole.ADMINISTRATOR);
        if (chckbxCataloque.isSelected()) retVal.add(LibrarianRole.CATALOGUER);
        if (chckbxFrontDesk.isSelected()) retVal.add(LibrarianRole.FRONT_DESK);
        return retVal;
    }


}
