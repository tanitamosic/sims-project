package view.librarian;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class CreateBorrowingFrameMessage extends JFrame {

    private JPanel contentPane;
    private JLabel lblMessage;
    private JButton btnOK;

    public CreateBorrowingFrameMessage(String id, String num) {
        initFrame();
        addComponents();
        //addListener();
    }

    private void addComponents() {
        lblMessage = new JLabel("Uspjesno ste kreirali pozajmicu");
        contentPane.add(lblMessage, "cell 0 0,alignx center");

        btnOK = new JButton("OK");
        contentPane.add(btnOK, "cell 0 1,alignx center");
    }

    private void initFrame() {
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(size.width / 4, size.height / 4);
        setLocationRelativeTo(null);
        setResizable(false);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new MigLayout("", "30%[]", "20%[]50%[]"));
    }
}
