package view.librarian;

import view.components.JTableButtonMouseListener;
import view.components.RegularButton;
import view.components.colors.DefaultBackgroundColor;
import view.tables.CommentsTable;
import net.miginfocom.swing.MigLayout;
import controllers.AdminController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;

public class ModerateCommentsFrame extends JFrame {

    private JScrollPane scrollPane;
    private ArrayList<String> comments = new ArrayList<String>();
    private JPanel contentPane;
    private JPanel southPanel;
    private JPanel centralPanel;
    private JButton btnAccept;

    private AdminController adminController;

    public ModerateCommentsFrame(AdminController as) {
        this.adminController = as;
        initFrame();
        initPanes();
        addComponents();
        addButtonListeners();
    }

    private void addButtonListeners() {
        btnAccept.addActionListener(e -> new ModerateCommentsDialog(this).setVisible(true));
    }

    private void addComponents() {
        btnAccept = new RegularButton("Potvrdi");
        southPanel.add(btnAccept, "cell 0 0, alignx right");

        JTable commentsTable = new CommentsTable(adminController);
        commentsTable.getColumnModel().getColumn(0).setPreferredWidth(800);
        JScrollPane scrollPane = new JScrollPane(commentsTable);

        commentsTable.addMouseListener(new JTableButtonMouseListener(commentsTable));

        add(scrollPane, BorderLayout.CENTER);

    }

    private void initPanes() {
        southPanel = new JPanel();
        contentPane.add(southPanel, BorderLayout.SOUTH);
        southPanel.setLayout(new MigLayout("", "[grow]2sp", "[]"));
        southPanel.setBackground(new DefaultBackgroundColor());

        centralPanel = new JPanel();
        contentPane.add(centralPanel, BorderLayout.CENTER);
        centralPanel.setLayout(new MigLayout("", "[]", "[]"));
        centralPanel.setBackground(new DefaultBackgroundColor());
    }

    private void initFrame() {
        setTitle("Komentari");
        setExtendedState(MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(new Dimension(size.width / 2, size.height / 2));
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
    }
}
