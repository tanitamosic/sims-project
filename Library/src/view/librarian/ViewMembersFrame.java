package view.librarian;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

import model.DTO.MemberSearchCriteria;
import model.enums.MemberType;
import net.miginfocom.swing.MigLayout;
import controllers.AdminController;

import view.components.RegularButton;
import view.components.RegularLabel;
import view.components.colors.DefaultBackgroundColor;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class ViewMembersFrame extends JFrame {

    private JPanel contentPane;
    private JTextField tfName;
    private JTextField tfSurname;
    private JTextField tfNum;
    private JButton btnSearch;
    private JLabel lblName;
    private JLabel lblSurname;
    private JLabel lblNum;
    private JLabel lblType;
    private JComboBox<String> cbType;
    private JPanel northPanel;
    private JPanel southPanel;
    private JPanel centralPanel;
    private AdminController adminController;


    public ViewMembersFrame(AdminController as) {
        this.adminController = as;
        initFrame();
        initPanes();
        initComponents();
        addComponents();
        addButtonListeners();
    }

    private void initFrame() {
        setTitle("Pregled članova");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(size.width / 5 * 2, size.height / 2);
        setLocationRelativeTo(null);
        setResizable(false);
        contentPane = new JPanel(new BorderLayout());
        contentPane.setBackground(new DefaultBackgroundColor());
        setContentPane(contentPane);
    }

    private void initPanes() {
        northPanel = new JPanel(new MigLayout("", "[grow]", "[][]"));
        northPanel.setBackground(new DefaultBackgroundColor());
        contentPane.add(northPanel, BorderLayout.NORTH);

        southPanel = new JPanel(new MigLayout("", "10%[30%]3%[30%]2sp", "15%[30%][30%]3sp"));
        southPanel.setBackground(new DefaultBackgroundColor());
        contentPane.add(southPanel, BorderLayout.SOUTH);

        centralPanel = new JPanel(new MigLayout("", "5%[30%]2%[40%]", "20%[7%]8%[7%]8%[7%]8%[7%]"));
        centralPanel.setBackground(new DefaultBackgroundColor());
        contentPane.add(centralPanel, BorderLayout.CENTER);
    }

    private void initComponents() {
        btnSearch = new RegularButton("Traži");

        lblName = new RegularLabel("Ime");
        tfName = new JTextField(40);

        lblSurname = new RegularLabel("Prezime");
        tfSurname = new JTextField(40);

        lblNum = new RegularLabel("Broj članske karte");
        tfNum = new JTextField(40);

        lblType = new RegularLabel("Tip korisnika");
        cbType = new JComboBox<>();
        cbType.setModel(new DefaultComboBoxModel<>(MemberType.getNames()));
        cbType.setSelectedIndex(0);
    }

    private void addComponents() {
        southPanel.add(btnSearch, "cell 13 0,alignx right,aligny top");

        centralPanel.add(lblName, "cell 0 0,alignx trailing");
        centralPanel.add(tfName, "cell 1 0,grow");

        centralPanel.add(lblSurname, "cell 0 1,alignx trailing");
        centralPanel.add(tfSurname, "cell 1 1,grow");

        centralPanel.add(lblNum, "cell 0 2,alignx trailing");
        centralPanel.add(tfNum, "cell 1 2,grow");

        centralPanel.add(lblType, "cell 0 3,alignx trailing");
        centralPanel.add(cbType, "cell 1 3,grow");
    }

    private void addButtonListeners() {

        btnSearch.addActionListener(e -> {
            String name = tfName.getText();
            String surname = tfSurname.getText();
            String id = tfNum.getText();
            int type = cbType.getSelectedIndex();
            adminController.setMemberSearchCriteria(new MemberSearchCriteria(name, surname, id, type));
            adminController.searchMembers();
            new ViewFilteredMembersFrame(adminController).setVisible(true);
        });

    }
}

