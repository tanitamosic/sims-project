package view.librarian;

import exceptions.*;
import view.SearchCriteriaPane;
import view.components.JTableButtonMouseListener;
import view.components.RegularButton;
import view.components.colors.DefaultBackgroundColor;
import view.components.colors.HeaderBackgroundColor;
import view.tables.BorrowingBookSearchTable;
import model.DTO.SearchCriteria;
import net.miginfocom.swing.MigLayout;
import observer.Observer;
import observer.UpdateEvent;
import controllers.AdminController;
import controllers.LibraryController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;


public class BorrowingFrame extends JFrame implements Observer {

    private JPanel contentPane;
    private JTextField textFieldUserId;
    private JTextField textFieldNumOfPictureBooks;
    private JPanel eastPanel;
    private JLabel lblAddedBooks;
    private JPanel centralPanel;
    private JPanel southPanel;
    private JButton btnFinish;
    private JLabel lblPictureBooks;
    private JSeparator separator;
    private SearchCriteriaPane searchBookPane;
    private JLabel lblUserID;
    private Dimension size;
    private JPanel northPanel;
    private List<String> titles;

    private LibraryController libraryController;
    private AdminController adminController;

    public BorrowingFrame(AdminController as, LibraryController ls) {
        this.adminController = as;
        this.libraryController = ls;
        this.libraryController.setSearchCriteria(new SearchCriteria());
        this.titles = adminController.getBorrowedTitles();
        adminController.addObserver(this);
        initFrame();
        initPanes();
        initComponents();
        addComponents();
        addButtonListeners();

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                adminController.eraseBorrowCandidates();
                updateEastPanel();
            }
        });
    }

    private void initFrame() {
        setTitle("Nova pozajmica");
        setExtendedState(MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        size = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(new Dimension(size.width / 2, size.height / 2));
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
    }

    private void initPanes() {
        searchBookPane = new SearchCriteriaPane(libraryController);
        searchBookPane.setBackground(new DefaultBackgroundColor());
        JTable borrowingBookSearchTable = new BorrowingBookSearchTable(adminController, libraryController);

        borrowingBookSearchTable.addMouseListener(new JTableButtonMouseListener(borrowingBookSearchTable));

        JScrollPane scrollPane = new JScrollPane(borrowingBookSearchTable);
        add(scrollPane, BorderLayout.CENTER);
        northPanel = new JPanel();
        contentPane.add(northPanel, BorderLayout.NORTH);
        northPanel.setLayout(new MigLayout("", "[][grow][grow]", "[][grow][][]"));
        northPanel.setBackground(new DefaultBackgroundColor());

        southPanel = new JPanel();
        contentPane.add(southPanel, BorderLayout.SOUTH);
        southPanel.setLayout(new MigLayout("", "[grow]3sp", "[]"));
        southPanel.setBackground(new DefaultBackgroundColor());

        eastPanel = new JPanel();
        contentPane.add(eastPanel, BorderLayout.EAST);
        eastPanel.setLayout(new MigLayout("", "[]", "[]"));
        eastPanel.setBackground(new DefaultBackgroundColor());
    }

    private void initComponents() {
        lblUserID = new JLabel("Broj članske karte");
        separator = new JSeparator();
        separator.setPreferredSize(new Dimension(size.width, 1));
        separator.setBackground(Color.BLACK);
        textFieldUserId = new JTextField();
        textFieldUserId.setColumns(10);
        lblPictureBooks = new JLabel("Broj slikovnica:");
        textFieldNumOfPictureBooks = new JTextField("0");
        textFieldNumOfPictureBooks.setColumns(10);
        btnFinish = new RegularButton("Završi");
        lblAddedBooks = new JLabel("Dodate knjige");
    }

    private void addComponents() {
        northPanel.add(lblUserID, "cell 0 0,alignx left");
        northPanel.add(searchBookPane, "cell 0 1,grow");
        northPanel.add(separator, "cell 0 2");
        northPanel.add(textFieldUserId, "cell 0 0,alignx left");
        northPanel.add(lblPictureBooks, "cell 0 0");
        northPanel.add(textFieldNumOfPictureBooks, "cell 0 0,alignx left");

        southPanel.add(btnFinish, "cell 0 0, alignx right");
        eastPanel.add(lblAddedBooks, "wrap");
        for (String title : titles)
            eastPanel.add(new JLabel(title), "wrap");

    }

    private void addButtonListeners() {
        btnFinish.addActionListener(e -> {
            String id = textFieldUserId.getText();
            String num = textFieldNumOfPictureBooks.getText();
            try {
                adminController.makeBorrowing(id, num);
                JOptionPane.showMessageDialog(null, "Uspešno ste kreirali pozajmicu!");
                dispose();
            } catch (FieldMissingException e1) {
                JOptionPane.showMessageDialog(null, "Nije unesen ID korisnika", "Greška", JOptionPane.ERROR_MESSAGE);
            } catch (MaxNumOfBooksReachedException e2) {
                JOptionPane.showMessageDialog(null, "Maksimalan broj knjiga koje mozete podici je " + e2.getValue(), "Greška", JOptionPane.ERROR_MESSAGE);
            } catch (Exception e4) {
                JOptionPane.showMessageDialog(null, "Dogodila se greška", "Greška", JOptionPane.ERROR_MESSAGE);
            } finally {
                adminController.eraseBorrowCandidates();
                updateEastPanel();
            }
        });
    }

    @Override
    public void updatePerformed(UpdateEvent event) {
        updateEastPanel();
    }

    private void updateEastPanel() {
        titles = adminController.getBorrowedTitles();
        eastPanel.removeAll();
        eastPanel.revalidate();
        eastPanel.repaint();
        addComponents();
    }
}

