package view.librarian;

import view.components.RegularButton;
import controllers.LibraryController;

import javax.swing.*;

public class LibrarianBookSearchItem {

    private int id;
    private String title;
    private String publisher;
    private String section;
    private int position;
    private String tags;
    private boolean carryOut = true;
    private String[] authors;
    private int numOfFreeCopies;
    private LibraryController libraryController;

    public LibrarianBookSearchItem(int id, String title, String publisher, String section, int position, String tags,
                                   boolean carryOut, String[] authors, int numOfFreeCopies, LibraryController ls) {
        super();
        this.id = id;
        this.title = title;
        this.authors = authors;
        this.publisher = publisher;
        this.section = section;
        this.position = position;
        this.tags = tags;
        this.carryOut = carryOut;
        this.numOfFreeCopies = numOfFreeCopies;
        this.libraryController = ls;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public boolean isCarryOut() {
        return carryOut;
    }

    public void setCarryOut(boolean carryOut) {
        this.carryOut = carryOut;
    }

    public String [] getAuthors() {
        return authors;
    }

    public void setAuthors(String[] authors) {
        this.authors = authors;
    }

    public int getNumOfFreeCopies() {
        return numOfFreeCopies;
    }

    public void setNumOfFreeCopies(int numOfFreeCopies) {
        this.numOfFreeCopies = numOfFreeCopies;
    }

    public Object toCell(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return this.title;
            case 1:
                return this.authors;
            case 2:
                return this.publisher;
            case 3:
                return this.section;
            case 4:
                return this.position;
            case 5:
                return this.tags;
            case 6:
                return this.carryOut;
            case 7:
                return this.numOfFreeCopies;
            case 8:
                return makeBorrowButton(this.id);
            default:
                return " ";
        }
    }

    private JButton makeBorrowButton(int id2) {
        JButton borrowButton = new RegularButton("Zaduzi");
        //borrowButton.addActionListener(e -> new BorrowingMemberIDFrame.setVisible(true));
        return borrowButton;
    }


}
