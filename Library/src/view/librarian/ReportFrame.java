package view.librarian;

import controllers.AdminController;
import model.library.Author;
import model.library.Book;
import view.DateIntervalPane;
import view.ReviewsFrame;
import view.components.BoldLabel;
import view.components.RegularButton;
import view.components.colors.DefaultBackgroundColor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReportFrame extends JFrame {
    private JPanel northPanel;
    private DateIntervalPane centerPanel;
    private JPanel southPanel;
    private JLabel titleLabel;
    private JButton confirmBtn;
    private Date startDate;
    private Date endDate;
    private ArrayList<JPanel> panels;
    private JPanel contentPane;
    private JPanel scrollPanel;
    private JScrollPane scroller;
    private AdminController adminServices;

    public ReportFrame(AdminController as) {
        adminServices = as;
        initFrame();
        initPanes();
        initComponents();
        addComponents();
    }

    private void initFrame() {
        setTitle("Izveštaj o čitanosti");
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Dimension(size.width / 2, size.height / 2 + 100));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
    }

    private void initPanes() {
        northPanel = new JPanel();
        northPanel.setLayout(new FlowLayout());
        northPanel.setBackground(Color.WHITE);

        centerPanel = new DateIntervalPane();
        centerPanel.setBackground(new DefaultBackgroundColor());

        southPanel = new JPanel();
        southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.Y_AXIS));
        southPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        southPanel.setBackground(new DefaultBackgroundColor());

        scrollPanel = new JPanel();
        scrollPanel.setAlignmentX(LEFT_ALIGNMENT);
        scrollPanel.setBackground(new DefaultBackgroundColor());
        scrollPanel.setLayout(new BoxLayout(scrollPanel, BoxLayout.Y_AXIS));
        scroller = new JScrollPane(scrollPanel);

        contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setAlignmentX(Component.CENTER_ALIGNMENT);
        contentPane.setLayout(new BorderLayout());
        contentPane.setBackground(new DefaultBackgroundColor());
        setContentPane(contentPane);
        createReport();

    }

    private void initComponents() {
        titleLabel = new BoldLabel("Generisanje izveštaja o čitanosti");
    }

    private void createReport() {
        BoldLabel title = new BoldLabel("Izveštaj o čitanosti");
        //JLabel subtitle = new JLabel("Najčitaniji naslovi u biblioteci");
        panels = new ArrayList<>();
        northPanel.add(title, "wrap");
        scrollPanel.add(Box.createVerticalStrut(5));
        //scrollPanel.add(subtitle,"wrap");
        scrollPanel.add(Box.createVerticalStrut(20));

        createPanels();
        addPanels();
    }

    private void addPanels() {
        for (JPanel p : panels) {
            p.setBackground(new DefaultBackgroundColor());
            scrollPanel.add(p, "wrap");
            scrollPanel.add(Box.createVerticalStrut(5));
            JSeparator s = new JSeparator();
            s.setOrientation(SwingConstants.HORIZONTAL);
            scrollPanel.add(s);
            scrollPanel.add(Box.createVerticalStrut(5));
        }
    }

    private void createPanels() {

        ArrayList<Book> books = adminServices.getMostRead();
        for (Book b : books) {
            createPanel(b);
        }
    }

    private void createPanel(Book book) {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBackground(new DefaultBackgroundColor());

        JLabel bookLbl = new JLabel("Naslov: " + book.getTitle());
        List<Author> authors = book.getAuthors();
        JPanel authorsPanel = new JPanel();
        authorsPanel.setBackground(new DefaultBackgroundColor());
        authorsPanel.setLayout(new BoxLayout(authorsPanel, BoxLayout.Y_AXIS));
        JLabel authorsLabel = new JLabel("Autori:");
        authorsPanel.add(authorsLabel);
        for (Author a : authors) {
            JLabel al = new JLabel("           " + a.getName() + " " + a.getSurname() + ", " + a.getAuthorshipType().label);
            authorsPanel.add(al);
        }
        JLabel genreLbl = new JLabel("Žanr: " + book.getGenre().toString());
        JLabel publisherLbl = new JLabel("Izdavač: " + book.getPublisher());
        JLabel numLbl = new JLabel("Broj iznajmljivanja: " + String.valueOf(adminServices.getBorrowCount(book)));

        JPanel labelPanel = new JPanel();
        labelPanel.setBackground(new DefaultBackgroundColor());
        labelPanel.setLayout(new BoxLayout(labelPanel, 1));
        labelPanel.setAlignmentX(LEFT_ALIGNMENT);
        labelPanel.add(bookLbl);
        labelPanel.add(authorsPanel);
        labelPanel.add(genreLbl);
        labelPanel.add(publisherLbl);
        labelPanel.add(numLbl);

        JPanel btnPanel = new JPanel();
        btnPanel.setBackground(new DefaultBackgroundColor());
        JButton ratingBtn = new RegularButton("Pogledaj ocene");
        btnPanel.add(ratingBtn);
        final ReviewsFrame[] rf = {null};

        ratingBtn.addActionListener(e -> {
            if (rf[0] == null) rf[0] = new ReviewsFrame(book);
            rf[0].setVisible(true);
        });
        panel.add(labelPanel);
        panel.add(btnPanel);
        panels.add(panel);
    }

    private void addComponents() {
        contentPane.add(northPanel, BorderLayout.NORTH);
        contentPane.add(scroller, BorderLayout.CENTER);
    }
}
