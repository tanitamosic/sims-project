package view.librarian;

import view.components.RegularButton;
import view.components.colors.DefaultBackgroundColor;
import view.tables.MemberSearchModel;
import net.miginfocom.swing.MigLayout;
import controllers.AdminController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;

public class ViewFilteredMembersFrame extends JFrame {

    private JPanel contentPane;
    private JPanel southPanel;
    private JButton btnOK;
    private JPanel centralPanel;
    private AdminController adminController;

    public ViewFilteredMembersFrame(AdminController as) {
        this.adminController = as;
        initFrame();
        initPanes();
        addComponents();
        addButtonListeners();
    }

    private void addButtonListeners() {

        btnOK.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                JOptionPane.showMessageDialog(null, "Vaše promene su sačuvane", "", JOptionPane.INFORMATION_MESSAGE);
            }
        });

    }

    private void addComponents() {
        btnOK = new RegularButton("Potvrdi");
        southPanel.add(btnOK, "cell 13 0,alignx right,aligny top");

        JTable memberSearchTable = new JTable(new MemberSearchModel(adminController));
        JScrollPane scrollPane = new JScrollPane(memberSearchTable);
        add(scrollPane, BorderLayout.CENTER);
    }

    private void initPanes() {
        southPanel = new JPanel();
        contentPane.add(southPanel, BorderLayout.SOUTH);
        southPanel.setLayout(new MigLayout("", "10%[30%]3%[30%]2sp", "15%[30%][30%]3sp"));
        southPanel.setBackground(new DefaultBackgroundColor());

        centralPanel = new JPanel();
        contentPane.add(centralPanel, BorderLayout.CENTER);
        centralPanel.setLayout(new MigLayout("", "[]", "[]"));
        centralPanel.setBackground(new DefaultBackgroundColor());
    }

    private void initFrame() {
        setTitle("Korisnici");
        setExtendedState(MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(new Dimension(size.width / 2, size.height / 2));
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
    }
}

