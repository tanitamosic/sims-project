package view.librarian;


import com.toedter.calendar.JDateChooser;
import exceptions.FieldMissingException;
import exceptions.MismatchingConfirmedPasswordException;
import exceptions.WeakPasswordException;
import model.enums.MemberType;
import net.miginfocom.swing.MigLayout;
import controllers.AdminController;
import view.components.BoldLabel;
import view.components.RegularButton;
import view.components.RegularLabel;
import view.components.colors.DefaultBackgroundColor;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Date;

public class AddNewMemberFrame extends JFrame {

    private JPanel contentPane;
    private JTextField textFieldName;
    private JTextField textFieldSurname;
    private JTextField textFieldJMBG;
    private JPanel northPane;
    private JPanel southPane;
    private JPanel centralPane;
    private JLabel lblTitle;
    private JButton btnOK;
    private JLabel lblName;
    private JLabel lblSurname;
    private JLabel lblDate;
    private JLabel lblJMBG;
    private JLabel lblType;
    private JComboBox<String> comboBoxType;
    private JLabel lblUsername;
    private JLabel lblPassword;
    private JLabel lblPassword2;
    private JTextField textFieldUsername;
    private JPasswordField textFieldPassword;
    private JPasswordField textFieldPassword2;
    private JDateChooser birthDate;

    private AdminController adminController;

    public AddNewMemberFrame(AdminController as) {
        this.adminController = as;
        initFrame();
        initPanes();
        initComponents();
        addComponents();
        addButtonListeners();
    }

    private void initFrame() {
        setTitle("Registracija novog člana");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(size.width / 2, size.height / 2);
        setLocationRelativeTo(null);
        setResizable(false);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
    }

    private void initPanes() {
        northPane = new JPanel();
        contentPane.add(northPane, BorderLayout.NORTH);
        northPane.setLayout(new MigLayout("", "1%[grow]1%", "15%[]7%[]"));
        northPane.setBackground(new DefaultBackgroundColor());

        southPane = new JPanel();
        contentPane.add(southPane, BorderLayout.SOUTH);
        southPane.setLayout(new MigLayout("", "10%[30%]3%[30%]5%", "15%[10%][10%]3sp"));
        southPane.setBackground(new DefaultBackgroundColor());

        centralPane = new JPanel();
        contentPane.add(centralPane, BorderLayout.CENTER);
        centralPane.setLayout(new MigLayout("", "[30%]2%[30%,grow]2%[][]20%", "[8%]4%[8%]4%[8%]4%[8%]4%[8%]4%[8%][][][]"));
        centralPane.setBackground(new DefaultBackgroundColor());
    }

    private void initComponents() {
        lblTitle = new BoldLabel("Unesite podatke o novom korisniku");
        btnOK = new RegularButton("Završi sa unosom");
        lblName = new RegularLabel("Ime");
        textFieldName = new JTextField();
        textFieldName.setColumns(10);
        lblSurname = new RegularLabel("Prezime");
        lblDate = new RegularLabel("Datum rođenja");
        textFieldSurname = new JTextField();
        textFieldSurname.setColumns(10);
        birthDate = new JDateChooser();
        lblJMBG = new RegularLabel("JMBG");
        textFieldJMBG = new JTextField();
        textFieldJMBG.setColumns(10);
        lblType = new RegularLabel("Tip korisnika");
        comboBoxType = new JComboBox<>();
        comboBoxType.setModel(new DefaultComboBoxModel<>(MemberType.getNames()));
        comboBoxType.setSelectedIndex(3);
        comboBoxType.setToolTipText("");
        lblUsername = new RegularLabel("Korisničko ime");
        textFieldUsername = new JTextField();
        textFieldPassword = new JPasswordField();
        textFieldPassword2 = new JPasswordField();
        lblPassword = new RegularLabel("Lozinka");
        lblPassword2 = new RegularLabel("Potvrda lozinke");
    }

    private void addComponents() {
        northPane.add(lblTitle, "cell 0 1,alignx center,aligny top");
        contentPane.add(southPane, BorderLayout.SOUTH);
        southPane.add(btnOK, "cell 13 0,alignx right,aligny top");
        contentPane.add(centralPane, BorderLayout.CENTER);
        centralPane.add(lblName, "cell 0 1,alignx trailing");
        centralPane.add(textFieldName, "cell 1 1,grow");
        centralPane.add(lblSurname, "cell 0 2,alignx trailing,aligny center");
        centralPane.add(textFieldSurname, "cell 1 2,grow");
        centralPane.add(lblDate, "cell 0 3,alignx trailing,aligny center");
        centralPane.add(birthDate, "cell 1 3,grow");
        centralPane.add(lblJMBG, "cell 0 4,alignx trailing");
        centralPane.add(textFieldJMBG, "cell 1 4,grow");
        centralPane.add(lblType, "cell 0 5,alignx trailing");
        centralPane.add(comboBoxType, "cell 1 5,grow");

        centralPane.add(lblUsername, "cell 0 6,alignx trailing");
        centralPane.add(textFieldUsername, "cell 1 6,growx");
        textFieldUsername.setColumns(10);
        centralPane.add(lblPassword, "cell 0 7,alignx trailing,aligny baseline");

        centralPane.add(textFieldPassword, "cell 1 7,growx");
        textFieldPassword.setColumns(10);

        centralPane.add(lblPassword2, "cell 0 8,alignx trailing");
        centralPane.add(textFieldPassword2, "cell 1 8,growx");
        textFieldPassword2.setColumns(10);
    }

    private void addButtonListeners() {
        btnOK.addActionListener(e -> {
            String name = String.valueOf(textFieldName.getText());
            String surname = String.valueOf(textFieldSurname.getText());
            String JMBG = String.valueOf(textFieldJMBG.getText());
            int userType = (comboBoxType.getSelectedIndex());
            String username = String.valueOf(textFieldUsername.getText());
            String password = String.valueOf(textFieldPassword.getText());
            String password2 = String.valueOf(textFieldPassword2.getText());
            Date date = birthDate.getDate();

            try {
                adminController.addMember(name, surname, date, JMBG, userType, username, password, password2);
                this.setVisible(false);
                new AddedMemberFrameMessage().setVisible(true);
            } catch (FieldMissingException e1) {
                JOptionPane.showMessageDialog(null, "Nisu unesena sva polja", "Greška", JOptionPane.ERROR_MESSAGE);
            } catch (WeakPasswordException e2) {
                emptyPasswordFields();
                JOptionPane.showMessageDialog(null, "Unesena lozinka nije dovoljno snažna", "Greška", JOptionPane.ERROR_MESSAGE);
            } catch (MismatchingConfirmedPasswordException e3) {
                emptyPasswordFields();
                JOptionPane.showMessageDialog(null, "Lozinka nije ispravno potvrđena", "Greška", JOptionPane.ERROR_MESSAGE);
            } catch (Exception e4) {
                emptyPasswordFields();
                JOptionPane.showMessageDialog(null, "Dogodila se greška", "Greška", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    private void emptyPasswordFields() {
        textFieldPassword.setText("");
        textFieldPassword2.setText("");
    }
}