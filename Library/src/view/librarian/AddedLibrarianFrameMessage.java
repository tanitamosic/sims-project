package view.librarian;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class AddedLibrarianFrameMessage extends JFrame {

    private JPanel contentPane;
    private JLabel lblSucc;
    private JButton btnOK;

    public AddedLibrarianFrameMessage() {
        initFrame();
        addComponents();
        addListener();
    }

    private void addListener() {
        btnOK.addActionListener(e-> setVisible(false));
    }

    private void addComponents() {
        lblSucc = new JLabel("Bibliotekar dodat");
        contentPane.add(lblSucc, "cell 1 0,alignx center");

        btnOK = new JButton("OK");
        contentPane.add(btnOK, "cell 1 1,alignx center");


    }

    private void initFrame() {
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(size.width / 4, size.height / 4);
        setLocationRelativeTo(null);
        setResizable(false);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new MigLayout("", "25%[]10%[]", "20%[]30%[]"));
    }
}
