package view.librarian;


import controllers.*;
import model.users.UserAccount;
import services.LibrarianServices;
import view.ChangePasswordDialog;
import view.LoginFrame;
import view.UserGUI;
import view.components.*;
import model.users.Librarian;
import net.miginfocom.swing.MigLayout;
import view.components.colors.MenuBackgroundColor;

import javax.swing.*;
import java.awt.*;

public class LibrarianGUI extends UserGUI {

    private ImageButton btnLogOut;
    private MenuButton btnAddNewMember;
    private MenuButton btnSearchBooks;
    private MenuButton btnViewMembers;
    private MenuButton btnPriceList;
    private MenuButton btnReservations;
    private MenuButton btnReport;
    private MenuButton btnAddLibrarian;
    private MenuButton btnBorrowing;
    private MenuButton btnComments;
    private MenuButton btnChangePassword;

    private JLabel lblName;
    private JPanel northPane;
    private JPanel centerPane;
    private LoginFrame lg;

    private AdminController adminController;
    private CataloguerController cataloguerController;
    private FrontDeskController frontDeskController;
    private UserController userController;
    private UserAccount userAccount;
    private LibraryController libraryController;
    private Librarian librarian;
    private LibrarianServices ls;

    public LibrarianGUI(LibrarianServices ls) {
        this.ls = ls;
        this.adminController = ls.adminController;
        this.cataloguerController = ls.cataloguerController;
        this.frontDeskController = ls.frontDeskController;
        this.userController = ls.userController;
        this.libraryController = ls.libraryController;
    }


    public void open(Librarian librarian, LoginFrame lg) {
        this.lg = lg;
        this.librarian = librarian;
        this.userAccount = librarian.getUser_acc();
        initFrame();
        initPanes();
        initComponents();
        addComponents();
        addButtonListeners();
        setVisible(true);
    }

    private void initPanes() {
        northPane = new JPanel(new MigLayout("", "1%[grow][]1%", "15%[][]"));
        getContentPane().add(northPane, BorderLayout.NORTH);
        northPane.setBackground(new MenuBackgroundColor());

        centerPane = new JPanel(new MigLayout("", "18%[30%]3%[30%]", "8%[7%]5%[7%]5%[7%]5%[7%]5%[7%]5%[7%]"));
        getContentPane().add(centerPane, BorderLayout.CENTER);
        centerPane.setBackground(new MenuBackgroundColor());
    }

    private void initComponents() {
        initLabels();
        initButtons();
    }

    private void initLabels() {
        lblName = new BoldLabel("Bibliotekar: " + librarian.getName() + " " + librarian.getSurname());
    }

    private void initButtons() {
        btnLogOut = new ImageButton(LOGOUT_ICON_PATH, 35, 35);
        btnLogOut.setBorder(new RoundedBorder(5));
        btnAddNewMember = new MenuButton("Dodaj novog \u010Dlana");
        btnSearchBooks = new MenuButton("Pretra\u017Ei knjige");
        btnViewMembers = new MenuButton("Pregled \u010Dlanova");
        btnPriceList = new MenuButton("Promena cenovnika i pravila");
        btnReservations = new MenuButton("Rezervacije na \u010Dekanju");
        btnReport = new MenuButton("Izve\u0161taj");
        btnComments = new MenuButton("Pregledaj komentare");
        btnChangePassword = new MenuButton("Promeni lozinku");
        btnAddLibrarian = new MenuButton("Dodaj bibliotekara");
        btnBorrowing = new MenuButton("Nova pozajmica");
    }

    private void addComponents() {
        centerPane.add(btnAddNewMember, "cell 0 1,grow");
        centerPane.add(btnSearchBooks, "cell 1 1,grow");
        centerPane.add(btnViewMembers, "cell 0 2,grow");
        centerPane.add(btnPriceList, "cell 1 2,grow");
        centerPane.add(btnReservations, "cell 0 3,grow");
        centerPane.add(btnComments, "cell 1 3,grow");
        centerPane.add(btnReport, "cell 0 4,grow");
        centerPane.add(btnAddLibrarian, "cell 1 4,grow");
        centerPane.add(btnBorrowing, "cell 0 5,grow");
        centerPane.add(btnChangePassword, "cell 1 5, grow, aligny center");

        northPane.add(lblName, "cell 0 0");
        northPane.add(btnLogOut, "cell 11 0,alignx right,aligny top");
    }

    private void addButtonListeners() {

        btnSearchBooks.addActionListener(e -> new LibrarianBookSearchFrame(libraryController, adminController).setVisible(true));
        btnAddNewMember.addActionListener(e -> new AddNewMemberFrame(adminController).setVisible(true));
        btnAddLibrarian.addActionListener(e -> new AddNewLibrarianFrame(adminController).setVisible(true));
        btnViewMembers.addActionListener(e -> new ViewMembersFrame(adminController).setVisible(true));
        btnPriceList.addActionListener(e -> new ChangePriceListFrame(adminController).setVisible(true));
        btnReservations.addActionListener(e -> new PendingReservationsFrame(frontDeskController).setVisible(true));
        btnReport.addActionListener(e -> new ReportFrame(adminController).setVisible(true));
        btnChangePassword.addActionListener(e -> new ChangePasswordDialog(userController, userAccount).setVisible(true));
        btnComments.addActionListener(e -> new ModerateCommentsFrame(adminController).setVisible(true));
        btnBorrowing.addActionListener(e -> new BorrowingFrame(adminController, libraryController).setVisible(true));
        btnLogOut.addActionListener(e -> logout());
    }

    private void logout() {
        lg.open();
        removeAll();
        dispose();
    }
}


