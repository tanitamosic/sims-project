package view.librarian;

public class PricelistItem {

    String memberType;
    double percents;


    public String getMembreType() {
        return memberType;
    }


    public void setMembreType(String membreType) {
        this.memberType = membreType;
    }


    public double getPercents() {
        return percents;
    }


    public void setPercents(int percents) {
        this.percents = percents;
    }


    public PricelistItem(String memberType, double d) {
        this.memberType = memberType;
        this.percents = d;
    }


    public Object toCell(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return this.memberType;
            case 1:
                return this.percents;
            default:
                return " ";
        }
    }
}