package view.librarian;

import view.SearchCriteriaPane;
import view.components.MultiLineTableCellRenderer;
import view.tables.LibrarianBookSearchTable;
import controllers.AdminController;
import controllers.LibraryController;
import view.components.RegularLabel;
import view.components.colors.HeaderBackgroundColor;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class LibrarianBookSearchFrame extends JFrame {

    private JPanel northPane;
    private LibraryController libraryController;
    private AdminController adminController;


    public LibrarianBookSearchFrame(LibraryController libraryController, AdminController as) {
        this.libraryController = libraryController;
        this.adminController = as;
        initFrame();
        initPanes();
    }

    private void initFrame() {
        setTitle("Pretraga knjiga");
        setExtendedState(MAXIMIZED_BOTH);
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(new Dimension(size.width / 2, size.height / 2));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
    }

    private void initPanes() {
        northPane = new SearchCriteriaPane(libraryController);
        add(northPane, BorderLayout.NORTH);
        northPane.setBackground(new HeaderBackgroundColor());

        JTable librarianBookSearchTable = new LibrarianBookSearchTable(libraryController, adminController);
        //JTable librarianBookSearchTable = new JTable(new LibrarianBookSearchModel(libraryServices));

        DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
        renderer.setHorizontalAlignment(JLabel.CENTER);
        MultiLineTableCellRenderer multiLineRenderer = new MultiLineTableCellRenderer();
        for (int i = 0; i < librarianBookSearchTable.getColumnCount(); i++)
        {
            librarianBookSearchTable.getColumnModel().getColumn(i).setCellRenderer(renderer);
        }
        librarianBookSearchTable.getColumnModel().getColumn(1).setCellRenderer(multiLineRenderer);

        JScrollPane scrollPane = new JScrollPane(librarianBookSearchTable);
        add(scrollPane, BorderLayout.CENTER);
    }
}
