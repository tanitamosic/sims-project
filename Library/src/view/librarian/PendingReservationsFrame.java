package view.librarian;

import controllers.FrontDeskController;
import model.library.Author;
import model.library.Book;
import model.library.Reservation;
import model.users.Member;
import view.components.RegularButton;
import view.components.RegularLabel;
import view.components.colors.DefaultBackgroundColor;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.util.ArrayList;

public class PendingReservationsFrame extends JFrame {
    private JLabel titleLbl;
    private ArrayList<JPanel> panels;
    private JPanel scrollPanel;
    private FrontDeskController controller;
    private JPanel northPane;

    public PendingReservationsFrame(FrontDeskController controller){
        this.controller = controller;

        initFrame();
        initPanes();
        initComponents();
        addComponents();
    }

    private void initFrame() {
        setTitle("Pregled rezervacija na čekanju");
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(size.width / 2, size.height/2+50);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
    }

    private void initPanes() {
        northPane = new JPanel(new FlowLayout());
        add(northPane, BorderLayout.NORTH);
        northPane.setBackground(Color.WHITE);
        scrollPanel = new JPanel();
        scrollPanel.setAlignmentX(CENTER_ALIGNMENT);
        scrollPanel.setLayout(new BoxLayout(scrollPanel, BoxLayout.Y_AXIS));
        JScrollPane scroller = new JScrollPane(scrollPanel);
        add(scroller, BorderLayout.CENTER);
    }

    private void initComponents() {
        titleLbl = new RegularLabel("Rezervacije na čekanju");
        panels = new ArrayList<>();
        ArrayList<Reservation> reservations = controller.getPendingReservations();
        for (Reservation r : reservations) {
            makePanel(r);
        }
    }

    private void makePanel(Reservation r) {
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBackground(new DefaultBackgroundColor());

        Member member = r.getMember();
        JLabel memberLbl = new RegularLabel("Član: " + member.getName() + " " + member.getSurname());
        Book book = r.getBook();
        JLabel bookLbl = new RegularLabel("Naslov: " + book.getTitle());
        Author author = book.getAuthors().get(0);
        JLabel authorLbl = new RegularLabel("Autor: " + author.getName()+" "+ author.getSurname());
        JLabel loaned = new RegularLabel("Broj nevraćenih knjiga: "+ controller.getNumOfLoanedBooks(member));
        JLabel freeCopies = new RegularLabel("Broj slobodnih primeraka: " + controller.getNumOfFreeCopies(book));

        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new BoxLayout(labelPanel,1));
        labelPanel.setBackground(new DefaultBackgroundColor());
        labelPanel.setAlignmentX(LEFT_ALIGNMENT);
        labelPanel.add(memberLbl);
        labelPanel.add(loaned);
        labelPanel.add(Box.createVerticalStrut(10));
        labelPanel.add(bookLbl);
        labelPanel.add(authorLbl);
        labelPanel.add(freeCopies);
        
        JButton approveBtn = new RegularButton("Odobri");
        JButton declineBtn = new RegularButton("Odbij");

        JPanel btnPanel = new JPanel();
        btnPanel.add(approveBtn);
        btnPanel.add(declineBtn);
        btnPanel.setBackground(new DefaultBackgroundColor());

        approveBtn.addActionListener(e -> {
            controller.approveReservation(r);
            btnPanel.setVisible(false);
        });

        declineBtn.addActionListener(e -> {
            controller.declineReservation(r);
            btnPanel.setVisible(false);
        });

        panel.add(labelPanel);
        panel.add(btnPanel);
        panels.add(panel);
    }

    private void addComponents() {
        northPane.add(titleLbl);
        for (JPanel p : panels){
            scrollPanel.add(p);
        }
    }

}
