package view.librarian;

import application.Constants;
import net.miginfocom.swing.MigLayout;
import controllers.AdminController;
import view.components.BoldLabel;
import view.components.RegularButton;
import view.components.RegularLabel;
import view.components.colors.DefaultBackgroundColor;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChangePriceListFrame extends JFrame {
    private JTable changePriceListTable;
    private JPanel contentPane;
    private JPanel northPanel;
    private JPanel southPanel;
    private JPanel centralPanel;
    private JPanel fieldsPanel;
    private JTextField basePriceField;
    private JTextField discountStudentField;
    private JTextField discountPensionField;
    private JTextField dayLimitRegularField;
    private JTextField dayLimitStudentField;
    private JTextField dayLimitPensionField;
    private JTextField dayLimitHonorableField;
    private JTextField bookLimitStudentField;
    private JTextField bookLimitPensionField;
    private JTextField bookLimitRegularField;
    private JTextField bookLimitHonorableField;
    private JLabel titleLbl;
    private JButton confirmBtn;
    private JButton cancelBtn;

    private AdminController adminController;

    public ChangePriceListFrame(AdminController as) {
        adminController = as;
        initFrame();
        initPanes();
        initComponents();
        addComponents();

    }

    private void initFrame() {
        setTitle("Izmjena cjenovnika i pravila");
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(size.width / 5 * 2, size.height / 3 * 2);
        setLocationRelativeTo(null);
        setResizable(false);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new BorderLayout(0, 0));
    }

    private void initPanes() {
        northPanel = new JPanel();
        northPanel.setLayout(new FlowLayout());
        northPanel.setBackground(new DefaultBackgroundColor());

        southPanel = new JPanel();
        southPanel.setLayout(new MigLayout("", "push[][]", "[]"));
        southPanel.setAlignmentX(RIGHT_ALIGNMENT);
        southPanel.setBackground(new DefaultBackgroundColor());

        centralPanel = new JPanel();
        centralPanel.setLayout(new MigLayout("", "7%[][]", "[grow]"));
        centralPanel.setBackground(new DefaultBackgroundColor());

        fieldsPanel = new JPanel();
        fieldsPanel.setLayout(new BoxLayout(fieldsPanel, BoxLayout.Y_AXIS));
        fieldsPanel.setBackground(new DefaultBackgroundColor());
    }

    private void initComponents() {
        titleLbl = new BoldLabel("Izmena cenovnika i pravila");

        JLabel bpLbl = new BoldLabel("Osnovna cena: ");

        basePriceField = new JTextField(String.valueOf(adminController.getConstants().getBasePrice()), 5);
        JLabel dsLbl = new RegularLabel("Popust za studente (%): ");
        discountStudentField = new JTextField(String.valueOf(adminController.getConstants().getDiscountStudent()), 5);
        JLabel dpLbl = new RegularLabel("Popust za penzionere (%): ");
        discountPensionField = new JTextField(String.valueOf(adminController.getConstants().getDiscountPension()), 5);
        JLabel dlrLbl = new RegularLabel("Broj dana za zadržavanje knjiga - zaposleni: ");
        dayLimitRegularField = new JTextField(String.valueOf(adminController.getConstants().getDayLimitRegular()), 5);
        JLabel dlsLbl = new RegularLabel("Broj dana za zadržavanje knjiga - studenti: ");
        dayLimitStudentField = new JTextField(String.valueOf(adminController.getConstants().getDayLimitStudent()), 5);
        JLabel dlpLbl = new RegularLabel("Broj dana za zadržavanje knjiga - penzioneri: ");
        dayLimitPensionField = new JTextField(String.valueOf(adminController.getConstants().getDayLimitPension()), 5);
        JLabel dlhLbl = new RegularLabel("Broj dana za zadržavanje knjiga - počasni članovi: ");
        dayLimitHonorableField = new JTextField(String.valueOf(adminController.getConstants().getDayLimitHonorable()), 5);
        JLabel blrLbl = new RegularLabel("Broj knjiga koje se mogu pozajmiti - zaposleni: ");
        bookLimitRegularField = new JTextField(String.valueOf(adminController.getConstants().getBookLimitRegular()), 5);
        JLabel blsLbl = new RegularLabel("Broj knjiga koje se mogu pozajmiti - studenti: ");
        bookLimitStudentField = new JTextField(String.valueOf(adminController.getConstants().getBookLimitStudent()), 5);
        JLabel blpLbl = new RegularLabel("Broj knjiga koje se mogu pozajmiti - penzioneri: ");
        bookLimitPensionField = new JTextField(String.valueOf(adminController.getConstants().getBookLimitPension()), 5);
        JLabel blhLbl = new RegularLabel("Broj knjiga koje se mogu pozajmiti - počasni članovi: ");
        bookLimitHonorableField = new JTextField(String.valueOf(adminController.getConstants().getBookLimitHonorable()), 5);

        centralPanel.add(bpLbl);
        centralPanel.add(basePriceField, "wrap");
        centralPanel.add(dsLbl);
        centralPanel.add(discountStudentField, "wrap");
        centralPanel.add(dpLbl);
        centralPanel.add(discountPensionField, "wrap");
        centralPanel.add(dlrLbl);
        centralPanel.add(dayLimitRegularField, "wrap");
        centralPanel.add(dlsLbl);
        centralPanel.add(dayLimitStudentField, "wrap");
        centralPanel.add(dlpLbl);
        centralPanel.add(dayLimitPensionField, "wrap");
        centralPanel.add(dlhLbl);
        centralPanel.add(dayLimitHonorableField, "wrap");
        centralPanel.add(blrLbl);
        centralPanel.add(bookLimitRegularField, "wrap");
        centralPanel.add(blsLbl);
        centralPanel.add(bookLimitStudentField, "wrap");
        centralPanel.add(blpLbl);
        centralPanel.add(bookLimitPensionField, "wrap");
        centralPanel.add(blhLbl);
        centralPanel.add(bookLimitHonorableField, "wrap");

        confirmBtn = new RegularButton("Potvrdi");
        cancelBtn = new RegularButton("Otkaži");
    }

    private void addComponents() {
        contentPane.add(northPanel, BorderLayout.NORTH);
        contentPane.add(southPanel, BorderLayout.SOUTH);
        contentPane.add(centralPanel, BorderLayout.CENTER);

        northPanel.add(titleLbl);

        southPanel.add(confirmBtn);
        southPanel.add(cancelBtn);
        addListeners();
    }

    private void addListeners() {
        confirmBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Constants c = adminController.getConstants();
                    c.setBasePrice(Integer.parseInt(basePriceField.getText()));
                    c.setDiscountStudent(Integer.parseInt(discountStudentField.getText()));
                    c.setDiscountPension(Integer.parseInt(discountPensionField.getText()));
                    c.setDayLimitRegular(Integer.parseInt(dayLimitRegularField.getText()));
                    c.setDayLimitStudent(Integer.parseInt(dayLimitStudentField.getText()));
                    c.setDayLimitPension(Integer.parseInt(dayLimitPensionField.getText()));
                    c.setDayLimitHonorable(Integer.parseInt(dayLimitHonorableField.getText()));
                    c.setBookLimitRegular(Integer.parseInt(bookLimitRegularField.getText()));
                    c.setBookLimitStudent(Integer.parseInt(bookLimitStudentField.getText()));
                    c.setBookLimitPension(Integer.parseInt(bookLimitPensionField.getText()));
                    c.setBookLimitHonorable(Integer.parseInt(bookLimitHonorableField.getText()));
                    c.saveConstants();
                } catch (Exception er) {
                    er.printStackTrace();
                }
            }
        });

        cancelBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Constants c = adminController.getConstants();
                basePriceField.setText(String.valueOf(c.getBasePrice()));
                discountStudentField.setText(String.valueOf(c.getDiscountStudent()));
                discountPensionField.setText(String.valueOf(c.getDiscountPension()));
                dayLimitRegularField.setText(String.valueOf(c.getDayLimitRegular()));
                dayLimitStudentField.setText(String.valueOf(c.getDayLimitStudent()));
                dayLimitPensionField.setText(String.valueOf(c.getDayLimitPension()));
                dayLimitHonorableField.setText(String.valueOf(c.getDayLimitHonorable()));
                bookLimitRegularField.setText(String.valueOf(c.getBookLimitRegular()));
                bookLimitStudentField.setText(String.valueOf(c.getBookLimitStudent()));
                bookLimitPensionField.setText(String.valueOf(c.getBookLimitPension()));
                bookLimitHonorableField.setText(String.valueOf(c.getBookLimitHonorable()));
            }
        });
    }
}
