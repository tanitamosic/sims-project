package view;

import view.components.BoldLabel;
import view.components.RegularLabel;
import view.components.StarRater;
import model.library.Book;
import model.library.Rating;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class ReviewsFrame extends JFrame {

    private JPanel northPane;
    private JScrollPane scrollPane;
    private JLabel lblTitle;
    private JLabel lblAuthor;
    private JLabel lblAvgScore;
    private JLabel lblAvgScoreText;
    private List<Rating> ratings;
    private Book book;

    public ReviewsFrame(Book book) {
        this.book = book;
        this.ratings = book.getRatings();

        initFrame();
        initPanes();
        initComponents();
        addComponents();
    }

    private void initFrame() {
        setTitle("Ocene knjige");
        setExtendedState(MAXIMIZED_BOTH);
        setLocationRelativeTo(null);
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(new Dimension(size.width / 2, size.height / 2));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout());
    }

    private void initPanes() {
        northPane = new JPanel();
        add(northPane, BorderLayout.NORTH);
        northPane.setLayout(new MigLayout("", "[grow]", "12%[]5%[]10%[][2sp]"));
    }

    private void initComponents() {
        lblTitle = new BoldLabel(book.getTitle());
        lblAuthor = new BoldLabel(book.getAuthorsNames()[0]);
        lblAvgScoreText = new RegularLabel("Prosečna ocena:");
        String avgScore = book.getAverageRatingScore() != 0 ? String.valueOf(book.getAverageRatingScore()) : "-";
        lblAvgScore = new BoldLabel(avgScore);
        JPanel centerPane = new JPanel();
        scrollPane = new JScrollPane(centerPane);
        centerPane.setLayout(new MigLayout("", "[grow]"));
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        for (Rating rating : ratings) {
            if (!rating.isApproved()) {
                continue;
            }
            StarRater starRater = new StarRater(5, rating.getNumOfStars());
            starRater.setEnabled(false);
            centerPane.add(starRater, "wrap");
            JTextArea taComment = new JTextArea(rating.getComment());
            taComment.setFont(new Font("Verdana", Font.PLAIN, 13));
            taComment.setColumns(size.width / 13);
            centerPane.add(taComment, "alignx left, wrap");
            JSeparator separator = new JSeparator();
            separator.setPreferredSize(new Dimension(size.width, 1));
            separator.setBackground(Color.BLACK);
            centerPane.add(separator, "wrap");
        }
    }

    private void addComponents() {
        northPane.add(lblTitle, "cell 0 0, alignx center");
        northPane.add(lblAuthor, "cell 0 1, alignx center");
        northPane.add(lblAvgScoreText, "cell 0 2, alignx center");
        northPane.add(lblAvgScore, "cell 0 2, alignx center");
        add(scrollPane, BorderLayout.CENTER);
    }

}


