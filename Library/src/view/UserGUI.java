package view;

import javax.swing.*;
import java.awt.*;

public abstract class UserGUI extends JFrame {

    protected static final String ICON_PATH = "./img/book_icon.png";
    protected static final String LOGOUT_ICON_PATH = "./img/logout_icon.png";

    public UserGUI() {
        initFrame();
    }

    protected void initFrame() {
        setTitle("Meni");
        ImageIcon img = new ImageIcon(ICON_PATH);
        setIconImage(img.getImage());
        setExtendedState(MAXIMIZED_BOTH);
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setMinimumSize(new Dimension(size.width / 2, size.height / 3 * 2));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
    }
}
