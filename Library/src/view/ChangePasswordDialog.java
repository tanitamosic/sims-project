package view;

import exceptions.IncorrectPasswordException;
import exceptions.MismatchingConfirmedPasswordException;
import exceptions.WeakPasswordException;
import view.components.RegularButton;
import view.components.RegularLabel;
import model.users.UserAccount;
import net.miginfocom.swing.MigLayout;
import controllers.UserController;

import javax.swing.*;
import java.awt.*;

public class ChangePasswordDialog extends JDialog {

    private JLabel lblOldPassword;
    private JLabel lblNewPassword;
    private JLabel lblNewPasswordConfirm;
    private JPasswordField pfOldPassword;
    private JPasswordField pfNewPassword;
    private JPasswordField pfNewPasswordConfirm;
    private JButton btnConfirm;
    private UserController userController;
    private UserAccount userAccount;
    private JPanel centerPane;

    public ChangePasswordDialog(UserController userController, UserAccount userAccount) {
        this.userController = userController;
        this.userAccount = userAccount;

        initDialog();
        initComponents();
        addComponents();
        addListeners();
    }

    private void initDialog() {
        setTitle("Promena lozinke");
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(size.width / 2, size.height / 2);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        centerPane = new JPanel(new MigLayout("", "4%[]2%[]", "25%[]7%[]7%[]15%[10%]"));
        setLayout(new BorderLayout());
        add(centerPane, BorderLayout.CENTER);
        centerPane.setBackground(new Color(250, 240, 255));
    }

    private void initComponents() {
        lblOldPassword = new RegularLabel("Stara lozinka");
        lblNewPassword = new RegularLabel("Nova lozinka");
        lblNewPasswordConfirm = new RegularLabel("Potvrda nove lozinke");
        pfOldPassword = new JPasswordField(45);
        pfNewPassword = new JPasswordField(45);
        pfNewPasswordConfirm = new JPasswordField(45);
        btnConfirm = new RegularButton("Potvrdi");
    }

    private void addComponents() {
        centerPane.add(lblOldPassword, "cell 0 0");
        centerPane.add(pfOldPassword, "cell 1 0");
        centerPane.add(lblNewPassword, "cell 0 1");
        centerPane.add(pfNewPassword, "cell 1 1");
        centerPane.add(lblNewPasswordConfirm, "cell 0 2");
        centerPane.add(pfNewPasswordConfirm, "cell 1 2");
        centerPane.add(btnConfirm, "cell 1 3, alignx right, growy");
    }

    private void addListeners() {
        btnConfirm.addActionListener(e -> {
            String oldPassword = String.valueOf(pfOldPassword.getPassword());
            String newPassword = String.valueOf(pfNewPassword.getPassword());
            String newPasswordConfirmed = String.valueOf(pfNewPasswordConfirm.getPassword());
            try {
                userController.changePassword(oldPassword, newPassword, newPasswordConfirmed, userAccount);
                JOptionPane.showMessageDialog(null, "Lozinka je uspešno promenjena!");
                dispose();
            } catch (IncorrectPasswordException e1) {
                emptyPasswordFields();
                JOptionPane.showMessageDialog(null, "Unesena lozinka nije ispravna", "Greška", JOptionPane.ERROR_MESSAGE);
            } catch (WeakPasswordException e2) {
                emptyPasswordFields();
                JOptionPane.showMessageDialog(null, "Unesena lozinka nije dovoljno snažna", "Greška", JOptionPane.ERROR_MESSAGE);
            } catch (MismatchingConfirmedPasswordException e3) {
                emptyPasswordFields();
                JOptionPane.showMessageDialog(null, "Lozinka nije ispravno potvrđena", "Greška", JOptionPane.ERROR_MESSAGE);
            } catch (Exception e4) {
                emptyPasswordFields();
                JOptionPane.showMessageDialog(null, "Dogodila se greška", "Greška", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    private void emptyPasswordFields() {
        pfOldPassword.setText("");
        pfNewPassword.setText("");
        pfNewPasswordConfirm.setText("");
    }
}
