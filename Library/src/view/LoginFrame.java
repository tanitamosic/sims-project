package view;

import controllers.UserController;
import model.users.Librarian;
import model.users.Member;
import model.users.User;
import net.miginfocom.swing.MigLayout;
import services.LibrarianServices;
import services.MemberServices;
import view.components.BoldLabel;
import view.components.colors.LoginBackgroundColor;
import view.components.RegularButton;
import view.components.RegularLabel;
import view.librarian.LibrarianGUI;
import view.member.MemberMenu;

import javax.swing.*;
import java.awt.*;

public class LoginFrame extends JFrame {

    private JLabel lblUsername;
    private JLabel lblPassword;
    private JTextField tfUsername;
    private JPasswordField pfPassword;
    private JLabel lblMessage;
    private JButton btnOK;
    private UserController userController;
    private LibrarianServices ls;
    private MemberServices ms;
    private JPanel centerPane;
    private LoginFrame frame;

    public LoginFrame(LibrarianServices ls, MemberServices ms, UserController us) {
        frame = this;
        this.ls = ls;
        this.ms = ms;
        userController = us;
        initFrame();
        initComponents();
        addComponents();
        addListeners();
    }

    private void addListeners() {
        btnOK.addActionListener(e -> {
            String username = tfUsername.getText();
            String password = String.valueOf(pfPassword.getPassword());
            if (userController.tryLogin(username, password)) {
                close();
                User user = userController.login(username);
                if (user.getClass() == Member.class) {
                    Member member = (Member) user;
                    MemberMenu newMem = new MemberMenu(ms);
                    newMem.open(member,frame);
                } else {
                    Librarian librarian = (Librarian) user;
                    LibrarianGUI newLib = new LibrarianGUI(ls);
                    newLib.open(librarian, frame);
                }
            } else {
                loginFailed();
                pfPassword.setText("");
            }
        });
    }

    private void loginFailed() {
        JOptionPane.showMessageDialog(null, "Pogrešno korisničko ime ili lozinka", "Greška", JOptionPane.ERROR_MESSAGE);
    }

    private void initFrame() {
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(size.width / 2, size.height / 2);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Login");
        centerPane = new JPanel(new MigLayout("", "18%[]3%[]", "15%[]20%[]5%[]10%[]"));
        setLayout(new BorderLayout());
        add(centerPane, BorderLayout.CENTER);
        centerPane.setBackground(new LoginBackgroundColor());

    }

    private void initComponents() {
        lblUsername = new RegularLabel("Korisničko ime");
        lblPassword = new RegularLabel("Lozinka");
        tfUsername = new JTextField(30);
        pfPassword = new JPasswordField(30);
        lblMessage = new BoldLabel("Dobrodošli");
        btnOK = new RegularButton("Potvrdi");
        getRootPane().setDefaultButton(btnOK);
    }

    private void addComponents() {
        centerPane.add(lblMessage, "cell 0 0, span 2, align center");
        centerPane.add(lblUsername, "cell 0 1");
        centerPane.add(tfUsername, "cell 1 1");
        centerPane.add(lblPassword, "cell 0 2");
        centerPane.add(pfPassword, "cell 1 2");
        centerPane.add(btnOK, "cell 1 3, align right");
    }

    public void open(){
        tfUsername.setText("");
        pfPassword.setText("");
        setVisible(true);
        tfUsername.requestFocus();
    }
    
    private void close() {
        tfUsername.setText("");
        pfPassword.setText("");
        setVisible(false);
        tfUsername.requestFocus();
    }
}
