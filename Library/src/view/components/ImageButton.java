package view.components;

import javax.swing.*;
import java.awt.*;

public class ImageButton extends JButton {

    public ImageButton(String path, int width, int height) {
        ImageIcon icon = new ImageIcon(path);
        icon = new ImageIcon(icon.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
        setBackground(new Color(255, 250, 250));
        setIcon(icon);
    }
}
