package view.components;

import javax.swing.*;
import java.awt.*;

public class MenuButton extends JButton {

    public MenuButton(String text) {
        super(text);
        setFont(new Font("Verdana", Font.BOLD, 16));
        setBackground(new Color(247, 192, 30));
    }
}
