package view.components;

import javax.swing.*;
import java.awt.*;

public class RegularLabel extends JLabel {

    public RegularLabel(String text) {
        super(text);
        setFont(new Font("Verdana", Font.PLAIN, 14));
    }
}
