package view.components.colors;

import java.awt.*;

public class DefaultBackgroundColor extends Color {

    public DefaultBackgroundColor() {
        super(250, 240, 255);
    }
}
