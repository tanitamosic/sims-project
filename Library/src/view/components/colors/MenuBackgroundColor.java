package view.components.colors;

import java.awt.*;

public class MenuBackgroundColor extends Color {

    public MenuBackgroundColor() {
        super(254, 255, 232);
    }
}
