package view.components.colors;

import java.awt.*;

public class LoginBackgroundColor extends Color {

    public LoginBackgroundColor() {
        super(246, 227, 255);
    }
}
