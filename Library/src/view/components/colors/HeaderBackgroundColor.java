package view.components.colors;

import java.awt.*;

public class HeaderBackgroundColor extends Color {

    public HeaderBackgroundColor() {
        super(255, 215, 145);
    }
}
