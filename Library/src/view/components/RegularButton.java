package view.components;

import javax.swing.*;
import java.awt.*;

public class RegularButton extends JButton {

    public RegularButton(String text) {
        super(text);
        setFont(new Font("Verdana", Font.BOLD, 14));
        setBackground(new Color(255, 250, 250));
    }
}
