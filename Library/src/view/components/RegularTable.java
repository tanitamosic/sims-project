package view.components;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class RegularTable extends JTable {

    public RegularTable(AbstractTableModel model) {
        setRowSelectionAllowed(true);
        setColumnSelectionAllowed(true);
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setModel(model);
        setFont(new Font("Verdana", Font.PLAIN, 13));
        setRowHeight(40);

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        for (int i = 0; i < getColumnCount(); i++)
            getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
    }
}
