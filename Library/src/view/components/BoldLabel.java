package view.components;

import javax.swing.*;
import java.awt.*;

public class BoldLabel extends JLabel {

    public BoldLabel(String text) {
        super(text);
        setFont(new Font("Verdana", Font.BOLD, 14));
    }
}
