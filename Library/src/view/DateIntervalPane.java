package view;

import com.toedter.calendar.JDateChooser;
import view.components.RegularButton;
import view.components.RegularLabel;
import model.DTO.DateInterval;
import net.miginfocom.swing.MigLayout;
import observer.DateIntervalUpdateEvent;
import observer.Observer;
import observer.Publisher;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DateIntervalPane extends JPanel implements Publisher {

    private JLabel lblStartDate;
    private JLabel lblEndDate;
    private JDateChooser dcStartDate;
    private JDateChooser dcEndDate;
    private JButton btnConfirm;
    private List<Observer> observers;
    private Date startDate;
    private Date endDate;
    private static final String DATE_FORMAT = "dd/MM/yyyy";

    public DateIntervalPane() {
        setLayout(new MigLayout("", "1%[]1%[15%][grow]2sp", "[][]"));
        initComponents();
        addComponents();
        addListeners();
    }

    private void initComponents() {
        lblStartDate = new RegularLabel("Početni datum");
        lblEndDate = new RegularLabel("Krajnji datum");
        dcStartDate = new JDateChooser();
        dcStartDate.setMaxSelectableDate(new Date());
        dcStartDate.setDateFormatString(DATE_FORMAT);
        dcEndDate = new JDateChooser();
        dcEndDate.setMaxSelectableDate(new Date());
        dcEndDate.setDateFormatString(DATE_FORMAT);
        btnConfirm = new RegularButton("Potvrdi");
    }

    private void addComponents() {
        add(lblStartDate, "cell 0 0");
        add(dcStartDate, "cell 1 0, growx");
        add(lblEndDate, "cell 0 1");
        add(dcEndDate, "cell 1 1, growx");
        add(btnConfirm, "cell 2 1, alignx right");
    }

    private void addListeners() {
        btnConfirm.addActionListener(e -> {
            startDate = dcStartDate.getDate();
            endDate = dcEndDate.getDate();
            notifyObservers();
        });
    }

    @Override
    public void addObserver(Observer observer) {
        if (observers == null) observers = new ArrayList<>();
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        if (observers == null) return;
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers)
            observer.updatePerformed(new DateIntervalUpdateEvent(new DateInterval(startDate, endDate)));
    }
}
