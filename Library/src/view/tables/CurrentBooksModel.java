package view.tables;

import exceptions.BorrowingAlreadyProlongedException;
import view.components.RegularButton;
import model.library.BorrowedCopy;
import model.users.Member;
import controllers.CurrentBooksController;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

public class CurrentBooksModel extends AbstractTableModel {

    private CurrentBooksController services;
    private Member member;


    public CurrentBooksModel(CurrentBooksController services, Member member) {
        this.services = services;
        this.member = member;
    }

    @Override
    public int getRowCount() {
        return services.getRowCount(member);
    }

    @Override
    public int getColumnCount() {
        return services.getColumnCount();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        BorrowedCopy borrowedCopy = services.getBorrowedCopy(rowIndex, member);
        switch (columnIndex) {
            case CurrentBooksController.TITLE_INDEX:
                return borrowedCopy.getCopy().getBook().getTitle();
            case CurrentBooksController.AUTHORS_INDEX:
                return borrowedCopy.getCopy().getBook().getAuthorsNames();
            case CurrentBooksController.BORROWING_DATE_INDEX:
                return borrowedCopy.getBorrowing().getBorrowingDate();
            case CurrentBooksController.RETURN_DEADLINE_INDEX:
                return borrowedCopy.getReturnDeadline();
            case CurrentBooksController.PROLONGATION_BUTTON_INDEX:
                return makeProlongationButton(borrowedCopy);
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return services.getColumnName(column);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return services.getColumnClass(columnIndex);
    }

    private JButton makeProlongationButton(BorrowedCopy borrowedCopy) {
        JButton btnProlongation = new RegularButton("Produži");
        btnProlongation.addActionListener(e -> {
           if(services.prolongBorrowedCopy(borrowedCopy, member))
               JOptionPane.showMessageDialog(null,"Pozajmica je uspešno produžena","Produženje pozajmice",JOptionPane.INFORMATION_MESSAGE);
            else
                JOptionPane.showMessageDialog(null, "Pozajmicu je moguće produžiti samo jednom", "Greška", JOptionPane.ERROR_MESSAGE);
        });
        return btnProlongation;
    }
}
