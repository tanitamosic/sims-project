package view.tables;

import view.components.ButtonTableCellRenderer;
import view.components.MultiLineTableCellRenderer;
import view.components.RegularTable;
import model.users.Member;
import observer.Observer;
import observer.UpdateEvent;
import controllers.LibraryController;
import controllers.MemberBookSearchController;
import controllers.MemberController;

public class MemberBookSearchTable extends RegularTable implements Observer {

    public MemberBookSearchTable(MemberController memberController, LibraryController libraryController, Member member) {
        super(new MemberBookSearchModel(new MemberBookSearchController(memberController, libraryController), member));
        libraryController.addObserver(this);
        getColumnModel().getColumn(MemberBookSearchController.RESERVE_BUTTON_INDEX).setCellRenderer(new ButtonTableCellRenderer());
        getColumnModel().getColumn(MemberBookSearchController.REVIEWS_BUTTON_INDEX).setCellRenderer(new ButtonTableCellRenderer());
        getColumnModel().getColumn(MemberBookSearchController.AUTHORS_INDEX).setCellRenderer(new MultiLineTableCellRenderer());
    }

    @Override
    public void updatePerformed(UpdateEvent event) {
        MemberBookSearchModel model = (MemberBookSearchModel) this.getModel();
        model.fireTableDataChanged();
    }
}
