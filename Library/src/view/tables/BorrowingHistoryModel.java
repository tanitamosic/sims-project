package view.tables;

import view.member.BookReviewFrame;
import model.DTO.DateInterval;
import model.library.BorrowedCopy;
import model.users.Member;
import controllers.BorrowingHistoryController;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

public class BorrowingHistoryModel extends AbstractTableModel {

    private BorrowingHistoryController services;
    private Member member;

    public BorrowingHistoryModel(BorrowingHistoryController services, Member member) {
        this.services = services;
        this.member = member;
    }

    public void setDateInterval(DateInterval dateInterval) {
        services.setDateInterval(dateInterval);
    }

    @Override
    public int getRowCount() {
        return services.getRowCount(member);
    }

    @Override
    public int getColumnCount() {
        return services.getColumnCount();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        BorrowedCopy borrowedCopy = services.getBorrowedCopy(rowIndex, member);
        switch (columnIndex) {
            case BorrowingHistoryController.TITLE_INDEX:
                return borrowedCopy.getCopy().getBook().getTitle();
            case BorrowingHistoryController.AUTHORS_INDEX:
                return borrowedCopy.getCopy().getBook().getAuthorsNames();
            case BorrowingHistoryController.BORROWING_DATE_INDEX:
                return borrowedCopy.getBorrowing().getBorrowingDate();
            case BorrowingHistoryController.RETURN_DATE_INDEX:
                return borrowedCopy.getReturnDate();
            case BorrowingHistoryController.DEADLINE_INDEX:
                return borrowedCopy.getReturnDeadline();
            case BorrowingHistoryController.RATING_BUTTON_INDEX:
                return makeReviewButton(borrowedCopy);
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return services.getColumnName(column);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return services.getColumnClass(columnIndex);
    }

    private JButton makeReviewButton(BorrowedCopy borrowedCopy) {
        JButton btnReview = new JButton("Oceni");
        if (services.existsRating(member, borrowedCopy)) btnReview.setEnabled(false);
        if (borrowedCopy.getReturnDate() == null) btnReview.setEnabled(false);
        btnReview.addActionListener(e -> {
            new BookReviewFrame(services.getMemberController(), member, borrowedCopy.getCopy().getBookId()).setVisible(true);
            JButton source = (JButton) e.getSource();
            source.setEnabled(false);
        });
        return btnReview;
    }
}
