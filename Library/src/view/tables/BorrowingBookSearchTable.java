package view.tables;


import view.components.ButtonTableCellRenderer;
import view.components.MultiLineTableCellRenderer;
import view.components.RegularTable;
import observer.Observer;
import observer.UpdateEvent;
import controllers.AdminController;
import controllers.BorrowingBookSearchController;
import controllers.LibraryController;

public class BorrowingBookSearchTable extends RegularTable implements Observer {

    public BorrowingBookSearchTable(AdminController adminController, LibraryController libraryController) {
        super(new BorrowingBookSearchModel(new BorrowingBookSearchController(libraryController, adminController)));
        libraryController.addObserver(this);
        getColumnModel().getColumn(BorrowingBookSearchController.BORROW_BUTTON_INDEX).setCellRenderer(new ButtonTableCellRenderer());
        getColumnModel().getColumn(BorrowingBookSearchController.AUTHORS_INDEX).setCellRenderer(new MultiLineTableCellRenderer());
    }

    @Override
    public void updatePerformed(UpdateEvent event) {
        BorrowingBookSearchModel model = (BorrowingBookSearchModel) this.getModel();
        model.fireTableDataChanged();
    }
}
