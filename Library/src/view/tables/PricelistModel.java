package view.tables;

import view.librarian.PricelistItem;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class PricelistModel extends AbstractTableModel {

    private ArrayList<PricelistItem> content = new ArrayList<PricelistItem>();
    private String[] columns = {"Tip clana","Koeficijent"};

    public PricelistModel() {
        PricelistItem pi1 = new PricelistItem("penzioner", 0.8);
        PricelistItem pi2 = new PricelistItem("student", 0.9);
        content.add(pi1);
        content.add(pi2);
    }

    @Override
    public int getRowCount() {
        return content.size();
    }

    @Override
    public int getColumnCount() {
        return this.columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        PricelistItem row = content.get(rowIndex);
        return row.toCell(columnIndex);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return column != 0;
    }

    @Override
    public String getColumnName(int col) {
        return this.columns[col];
    }

}
