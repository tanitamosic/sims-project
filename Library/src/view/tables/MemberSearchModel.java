package view.tables;

import exceptions.InvalidMemberTypeException;
import model.users.Member;
import controllers.AdminController;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.text.ParseException;
import java.util.List;

public class MemberSearchModel extends AbstractTableModel {

    private AdminController adminController;
    private List<Member> members;
    private String[] columns = {"Ime", "Prezime", "Broj članske karte", "Tip člana", "Datum rođenja"};

    public MemberSearchModel(AdminController as) {
        this.adminController = as;

    }

    @Override
    public int getRowCount() {
        return adminController.getSearchResult().size();
    }

    @Override
    public int getColumnCount() {
        return this.columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Member m = adminController.getSearchResult().get(rowIndex);
        return m.toSearchCell(columnIndex);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return column != 2;
    }

    @Override
    public String getColumnName(int col) {
        return this.columns[col];
    }

    @Override
    public Class<?> getColumnClass(int c) {
        if (adminController.getSearchResult().size() == 0) {
            return Object.class;
        }
        return getValueAt(0, c).getClass();
    }

    @Override
    public void setValueAt(Object value, int row, int column) {
        try {
            adminController.getSearchResult().get(row).setProperty(column, value);
        } catch (ParseException e1) {
            JOptionPane.showMessageDialog(null, "Unijeli ste datum pogresnog formata", "Greška", JOptionPane.ERROR_MESSAGE);
        } catch (InvalidMemberTypeException e2) {
            JOptionPane.showMessageDialog(null, "Unijeli ste nepostojeci tip korisnika", "Greška", JOptionPane.ERROR_MESSAGE);
        }
    }

}
