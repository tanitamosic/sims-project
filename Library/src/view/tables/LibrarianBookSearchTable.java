package view.tables;

import view.components.RegularTable;
import observer.Observer;
import observer.UpdateEvent;
import controllers.AdminController;
import controllers.LibraryController;


public class LibrarianBookSearchTable extends RegularTable implements Observer {

    public LibrarianBookSearchTable(LibraryController libraryController, AdminController adminController) {
        super(new LibrarianBookSearchModel(libraryController, adminController));
        libraryController.addObserver(this);
        //getColumnModel().getColumn(BookSearchModel.RESERVE_BUTTON_INDEX).setCellRenderer(new ButtonTableCellRenderer());
        //getColumnModel().getColumn(BookSearchModel.AUTHORS_INDEX).setCellRenderer(new MultiLineTableCellRenderer());
    }

    @Override
    public void updatePerformed(UpdateEvent event) {
        LibrarianBookSearchModel model = (LibrarianBookSearchModel) this.getModel();
        model.fireTableDataChanged();
    }
}

