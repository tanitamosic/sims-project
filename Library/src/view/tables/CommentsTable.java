package view.tables;

import view.components.ButtonTableCellRenderer;
import view.components.MultiLineTableCellRenderer;
import view.components.RegularTable;
import observer.Observer;
import observer.UpdateEvent;
import controllers.AdminController;
import controllers.CommentsTableController;
import view.tables.CommentsTableModel;

public class CommentsTable extends RegularTable implements Observer {

    public CommentsTable(AdminController adminController) {
        super(new CommentsTableModel(new CommentsTableController(adminController)));
        adminController.addObserver(this);
//        libraryServices.addObserver(this);
        getColumnModel().getColumn(CommentsTableController.APPROVE_INDEX).setCellRenderer(new ButtonTableCellRenderer());
        getColumnModel().getColumn(CommentsTableController.REJECT_INDEX).setCellRenderer(new ButtonTableCellRenderer());
        getColumnModel().getColumn(CommentsTableController.COMMENT_INDEX).setCellRenderer(new MultiLineTableCellRenderer());


    }

    @Override
    public void updatePerformed(UpdateEvent event) {
        CommentsTableModel model = (CommentsTableModel) this.getModel();
        model.fireTableDataChanged();
    }
}
