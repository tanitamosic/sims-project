package view.tables;


import model.library.Book;
import controllers.BorrowingBookSearchController;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

public class BorrowingBookSearchModel extends AbstractTableModel {

    private BorrowingBookSearchController services;

    public BorrowingBookSearchModel(BorrowingBookSearchController services) {
        this.services = services;
    }

    @Override
    public int getRowCount() {
        return services.getRowCount();
    }

    @Override
    public int getColumnCount() {
        return services.getColumnCount();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Book book = services.getBook(rowIndex);
        switch (columnIndex) {
            case BorrowingBookSearchController.TITLE_INDEX:
                return book.getTitle();
            case BorrowingBookSearchController.AUTHORS_INDEX:
                return book.getAuthorsNames();
            case BorrowingBookSearchController.PUBLISHER_INDEX:
                return book.getPublisher();
            case BorrowingBookSearchController.YEAR_INDEX:
                return book.getYear();
            case BorrowingBookSearchController.GENRE_INDEX:
                return book.getGenre();
            case BorrowingBookSearchController.POSITION_INDEX:
                return book.getShelfF().getPosition();
            case BorrowingBookSearchController.NUM_OF_FREE_INDEX:
                return services.getNumOfFreeCopies(book);
            case BorrowingBookSearchController.BORROW_BUTTON_INDEX:
                JButton borrowButton = makeBorrowButton(book);
                if (!services.hasFreeCopies(book)) borrowButton.setEnabled(false);
                return borrowButton;
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return services.getColumnName(column);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return services.getColumnClass(columnIndex);
    }

    private JButton makeBorrowButton(Book book) {
        JButton borrowButton = new JButton("Dodaj u korpu");
        borrowButton.addActionListener(e -> {
            services.addBorrowedCandidate(book);
        });
        return borrowButton;
    }
}



