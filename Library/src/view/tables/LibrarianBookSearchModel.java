package view.tables;

import model.library.Book;
import controllers.AdminController;
import controllers.LibraryController;

import javax.swing.table.AbstractTableModel;

public class LibrarianBookSearchModel extends AbstractTableModel {

    private LibraryController libraryController;
    private AdminController adminController;

    // additional column for reservation button
    private String[] columnNames = {"Naslov", "Autori", "Izdavač", "Izdanje", "Zanr / Sekcija", "Broj slobodnih primjeraka"};
    public static final int TITLE_INDEX = 0;
    public static final int AUTHORS_INDEX = 1;
    public static final int PUBLISHER_INDEX = 2;
    public static final int YEAR_INDEX = 3;
    public static final int GENRE = 4;
    public static final int NUM_OF_FREE_COPIES = 5;

    public LibrarianBookSearchModel(LibraryController libraryController, AdminController as) {
        this.libraryController = libraryController;
        this.adminController = as;
    }

    @Override
    public int getRowCount() {
        return libraryController.getSearchCandidates().size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Book book = libraryController.getSearchCandidates().get(rowIndex);
        switch (columnIndex) {
            case TITLE_INDEX:
                return book.getTitle();
            case AUTHORS_INDEX:
                return book.getAuthorsNames();
            case PUBLISHER_INDEX:
                return book.getPublisher();
            case YEAR_INDEX:
                return book.getYear();
            case GENRE:
                return book.getGenre();
            case NUM_OF_FREE_COPIES:
                return adminController.getBookRepo().getNumOfFreeCopies(book);
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (libraryController.getSearchCandidates().size() == 0)
            return Object.class;
        return getValueAt(0, columnIndex).getClass();
    }


}