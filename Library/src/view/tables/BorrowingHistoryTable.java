package view.tables;

import view.components.ButtonTableCellRenderer;
import view.components.MultiLineTableCellRenderer;
import view.components.RegularTable;
import model.DTO.DateInterval;
import model.users.Member;
import controllers.BorrowingHistoryController;
import controllers.MemberController;

public class BorrowingHistoryTable extends RegularTable {

    public BorrowingHistoryTable(MemberController memberController, Member member) {
        super(new BorrowingHistoryModel(new BorrowingHistoryController(memberController), member));
        getColumnModel().getColumn(BorrowingHistoryController.RATING_BUTTON_INDEX).setCellRenderer(new ButtonTableCellRenderer());
        getColumnModel().getColumn(BorrowingHistoryController.AUTHORS_INDEX).setCellRenderer(new MultiLineTableCellRenderer());
    }

    public void updateData(DateInterval dateInterval) {
        BorrowingHistoryModel model = (BorrowingHistoryModel) this.getModel();
        model.setDateInterval(dateInterval);
        model.fireTableDataChanged();
    }
}
