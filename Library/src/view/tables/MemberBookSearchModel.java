package view.tables;

import exceptions.NoAvailableBookCopiesException;
import view.ReviewsFrame;
import model.library.Book;
import model.users.Member;
import controllers.MemberBookSearchController;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

public class MemberBookSearchModel extends AbstractTableModel {

    private Member member;
    private MemberBookSearchController services;

    public MemberBookSearchModel(MemberBookSearchController services, Member member) {
        this.services = services;
        this.member = member;
    }

    @Override
    public int getRowCount() {
        return services.getRowCount();
    }

    @Override
    public int getColumnCount() {
        return services.getColumnCount();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Book book = services.getBook(rowIndex);
        switch (columnIndex) {
            case MemberBookSearchController.TITLE_INDEX:
                return book.getTitle();
            case MemberBookSearchController.AUTHORS_INDEX:
                return book.getAuthorsNames();
            case MemberBookSearchController.PUBLISHER_INDEX:
                return book.getPublisher();
            case MemberBookSearchController.YEAR_INDEX:
                return book.getYear();
            case MemberBookSearchController.RESERVE_BUTTON_INDEX:
                return makeReservationButton(book);
            case MemberBookSearchController.REVIEWS_BUTTON_INDEX:
                return makeViewReviewsButton(book);
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return services.getColumnName(column);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return services.getColumnClass(columnIndex);
    }

    private JButton makeReservationButton(Book book) {
        JButton btnReservation = new JButton("Rezerviši");
        btnReservation.addActionListener(e -> {
            try {
                services.reserveBook(book, member);
                JOptionPane.showMessageDialog(null, "Rezervacija je prihvaćena na čekanje");
            }
            catch (NoAvailableBookCopiesException ex) {
                JOptionPane.showMessageDialog(null, "Nema raspoloživih primeraka", "Greška", JOptionPane.ERROR_MESSAGE);
            }
        });
        return btnReservation;
    }

    private JButton makeViewReviewsButton(Book book) {
        JButton btnViewReviews = new JButton("Ocene");
        btnViewReviews.addActionListener(e -> new ReviewsFrame(book).setVisible(true));
        return btnViewReviews;
    }
}
