package view.tables;

import view.components.ButtonTableCellRenderer;
import view.components.MultiLineTableCellRenderer;
import view.components.RegularTable;
import model.users.Member;
import controllers.CurrentBooksController;
import controllers.MemberController;

public class CurrentBooksTable extends RegularTable {

    public CurrentBooksTable(MemberController memberController, Member member) {
        super(new CurrentBooksModel(new CurrentBooksController(memberController), member));

        getColumnModel().getColumn(CurrentBooksController.PROLONGATION_BUTTON_INDEX).setCellRenderer(new ButtonTableCellRenderer());
        getColumnModel().getColumn(CurrentBooksController.AUTHORS_INDEX).setCellRenderer(new MultiLineTableCellRenderer());
    }
}
