package view.tables;

import model.library.Rating;
import controllers.CommentsTableController;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

public class CommentsTableModel extends AbstractTableModel {

    private CommentsTableController services;

    public CommentsTableModel(CommentsTableController services) {
        this.services = services;
    }

    @Override
    public int getRowCount() {
        return services.getRowCount();
    }

    @Override
    public int getColumnCount() {
        return services.getColumnCount();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Rating rating = services.getRating(rowIndex);
        switch (columnIndex) {
            case CommentsTableController.COMMENT_INDEX:
                return rating.getComment().split("\n");
            case CommentsTableController.APPROVE_INDEX:
                return makeApproveButton(rating);
            case CommentsTableController.REJECT_INDEX:
                return makeRejectButton(rating);
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
        return services.getColumnName(column);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return services.getColumnClass(columnIndex);
    }

    private JButton makeApproveButton(Rating rating) {
        JButton approveButton = new JButton("Odobri");
        approveButton.addActionListener(e -> {
            services.approveComment(rating);
            // sad bi trebalo ponistiti ove dugmice nekako
            System.out.println("Komentar je odobren");
            approveButton.setEnabled(false);
        });
        return approveButton;
    }

    private JButton makeRejectButton(Rating rating) {
        JButton rejectButton = new JButton("Odbij");
        rejectButton.addActionListener(e -> {
            services.rejectRating(rating);
            System.out.println("Komentar je odbijen");

        });
        return rejectButton;
    }
}


