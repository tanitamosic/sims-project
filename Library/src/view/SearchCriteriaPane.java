package view;

import view.components.RegularButton;
import view.components.RegularLabel;
import model.DTO.SearchCriteria;
import model.enums.Genre;
import net.miginfocom.swing.MigLayout;
import controllers.LibraryController;

import javax.swing.*;

public class SearchCriteriaPane extends JPanel {

    private JLabel lblTitle;
    private JLabel lblAuthor;
    private JLabel lblTags;
    private JLabel lblGenre;
    private JTextField tfTitle;
    private JTextField tfAuthor;
    private JTextField tfTags;
    private JComboBox<String> cbGenre;
    private JButton btnSearch;
    private LibraryController libraryController;

    public SearchCriteriaPane(LibraryController libraryController) {
        this.libraryController = libraryController;

        setLayout(new MigLayout("", "1%[]1%[][grow]2%", "[][][][]"));
        initComponents();
        addComponents();
        addListeners();
    }

    private void initComponents() {
        lblTitle = new RegularLabel("Naslov");
        lblAuthor = new RegularLabel("Autor");
        lblTags = new RegularLabel("Ključne reči");
        lblGenre = new RegularLabel("Žanr");
        tfTitle = new JTextField(70);
        tfAuthor = new JTextField(70);
        tfTags = new JTextField(70);
        cbGenre = new JComboBox<>();
        cbGenre.setModel(new DefaultComboBoxModel<>(Genre.getNames()));
        cbGenre.addItem("-");
        cbGenre.setSelectedItem("-");
        btnSearch = new RegularButton("Pretraga");
    }

    private void addComponents() {
        add(lblTitle, "cell 0 0");
        add(tfTitle, "cell 1 0");
        add(lblAuthor, "cell 0 1");
        add(tfAuthor, "cell 1 1");
        add(lblTags, "cell 0 2");
        add(tfTags, "cell 1 2");
        add(lblGenre, "cell 0 3");
        add(cbGenre, "cell 1 3");
        add(btnSearch, "cell 2 3, growy, alignx right");
    }

    private void addListeners() {
        btnSearch.addActionListener(e -> {
            String title = tfTitle.getText();
            String author = tfAuthor.getText();
            String tags = tfTags.getText();
            String genre = (String) cbGenre.getSelectedItem();
            libraryController.setSearchCriteria(new SearchCriteria(title, author, tags, genre));
            libraryController.search();
        });
    }
}
