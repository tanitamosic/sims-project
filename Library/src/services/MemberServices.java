package services;

import controllers.LibraryController;
import controllers.MemberController;
import controllers.UserController;
import model.users.Member;

public class MemberServices {
    public  UserController userController;
    public MemberController memberController;
    public LibraryController libraryController;

    public MemberServices(UserController userController, MemberController memberController, LibraryController libraryController) {
        this.userController = userController;
        this.memberController = memberController;
        this.libraryController = libraryController;
    }
}
