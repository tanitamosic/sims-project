package services;

import controllers.*;
import model.users.Librarian;

public class LibrarianServices {
    public AdminController adminController;
    public CataloguerController cataloguerController;
    public FrontDeskController frontDeskController;
    public UserController userController;
    public LibraryController libraryController;

    public LibrarianServices(UserController userController, AdminController adminController,
                             CataloguerController cataloguerController, FrontDeskController frontDeskController, LibraryController ls){
        this.adminController = adminController;
        this.userController = userController;
        this.cataloguerController = cataloguerController;
        this.frontDeskController = frontDeskController;
        this.libraryController = ls;
    }
}
